#require 'excel_wrapper/ew_workbook/ew_worksheet'

class ExcelWrapper::EwWorkbook

  class DuplicateSheets < StandardError
    attr_reader :message

    def initialize(message)
    super
      @message = message
    end
  end

	attr_reader :label, :sheets

	# Initialize
  #
  # @param args [Hash] the argument hash
  # @return [Null] no return
  def initialize(args)
  	@label = ""
    @sheets = []
    @label = args[:label] if args[:label]
    if args[:sheets]
    	args[:sheets].each do |sheet|
    		@sheets << ExcelWrapper::EwWorkbook::EwWorksheet.new(sheet)
    	end
    end
  end

  # Find Sheet
  #
  # @param label [String] the name of the sheet desired
  # @raise [DuplicateSheets] if duplicate sheet names are detected
  # @return [Excel:Worksheet] the worksheet reference if found, nil otherwise
  def find_worksheet(label)
    items = @sheets.select{ |x| x.label == label}
    return items[0] if items.count == 1
    return nil if items.count == 0
    raise DuplicateSheets, "Duplicate sheets detected in find, label: #{label}."
  end

  # Add Sheet
  #
  # @param label [String] the name of the sheet desired
  # @raise [DuplicateSheets] if duplicate sheet names are detected
  # @return [Excel:Worksheet] the worksheet reference if found, nil otherwise
  def add_worksheet(args)
    if find_worksheet(args[:label]).nil?
      sheet = ExcelWrapper::EwWorkbook::EwWorksheet.new(args)
      @sheets << sheet
      return sheet
    else
      raise DuplicateSheets, "Attempt to create duplicate sheets detected in add, label: #{label}."
    end
  end

  # To Definition Hash
  #
  # @return [Hash] the object as a hash
  def to_definition_hash
  	ws = []
    @sheets.each { |x| ws << x.to_definition_hash}
    return { label: @label, sheets: ws }
  end

end