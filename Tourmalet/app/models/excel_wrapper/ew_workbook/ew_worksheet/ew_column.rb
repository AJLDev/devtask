class ExcelWrapper::EwWorkbook::EwWorksheet::EwColumn

  attr_reader :label, :default, :ordinal

  # Initialize
  #
  # @param args [Hash] the argument hash
  # @return [Null] no return
  def initialize(args)
    @label = ""
    @default = ""
    @ordinal = 1
    @label = args[:label] if args[:label]
    @default = args[:default] if args[:default]
    @ordinal = args[:ordinal] if args[:ordinal]
  end

  # To Definition Hash
  #
  # @return [Hash] the object as a hash
  def to_definition_hash
    return { label: @label, default: @default, ordinal: @ordinal }
  end

end