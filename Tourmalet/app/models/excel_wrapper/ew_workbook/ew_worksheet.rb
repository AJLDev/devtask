require 'csv'

class ExcelWrapper::EwWorkbook::EwWorksheet

	attr_reader :label, :columns, :rows

	# Initialize
  #
  # @param args [Hash] the argument hash
  # @return [Null] no return
  def initialize(args)
  	@label = ""
    @columns = []
    @rows = []
    @label = args[:label] if args[:label]
    if args[:columns]
    	args[:columns].each do |column|
    		@columns << ExcelWrapper::EwWorkbook::EwWorksheet::EwColumn.new(column)
    	end
    end
  end
  
  # Add Row. Will add heading row if not there. Merges the
  # args into the default values overwriting them if detected.
  #
  # @param args [Hash] the argument hash
  # @return [Null] no return
  def add_row(args)
    if @rows.count == 0
      row = []
      @columns.each { |x| row << x.label}
      @rows << row
    end
    row = []
    @columns.each do |column|
      data = args.detect { |arg| arg[:label] == column.label }
      if data.nil?
        row << column.default
      else
        row << data[:value]
      end
    end
    @rows << row
  end

  # To Definition Hash
  #
  # @return [Hash] the object as a hash
  def to_definition_hash
  	cols = []
    @columns.each { |x| cols << x.to_definition_hash}
    return { label: @label, columns: cols }
  end

  # To CSV
  #
  # @return [Array] the worksheet in a CSV format (array of arrays)
  def to_csv
    csv_data = CSV.generate do |csv|
      @rows.each do |row|
        csv << row
      end
    end
    return csv_data
  end

end