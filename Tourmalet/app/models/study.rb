class Study < ActiveRecord::Base

	C_CLASS_NAME = "Study"

	has_many :study_versions, :dependent => :destroy

	validates :identifier, presence: true, uniqueness: true
  validates_presence_of :label

  def to_hash
  	return { id: self.id, identifier: self.identifier, label: self.label }
  end

end
