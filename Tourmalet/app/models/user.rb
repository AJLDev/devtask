class User < ActiveRecord::Base
  
  # Include the user settings
  include UserSettings
 
	# Constants
  C_CLASS_NAME = "User"

  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
    :recoverable, :rememberable, :trackable, # :validatable, 
    :timeoutable,
    :password_expirable, :password_archivable , :secure_validatable

  # Simple role / access level management
  enum access_level: [:read, :edit, :system_admin]

  after_create :set_extra
  after_save :user_update

  # Set any extra items we need when a user is created
  def set_extra
    # Set the reader default role.
    self.access_level = User.access_levels[:read]
  end

  # Do any processing after user is changed
  def user_update
    # Audit if password changed  
    if encrypted_password_changed?
      AuditTrail.user_event(self, "User changed password.")
    end
  end

  # Has Read access
  #
  # @return [Boolean] true if user has read access, false otherwise
  def has_read_access? 
  	self.read? || self.edit? || self.system_admin?
	end

  # Has Edit access
  #
  # @return [Boolean] true if user has edit access, false otherwise
  def has_edit_access? 
  	self.edit? || self.system_admin?
	end

  # Has System Admin access
  #
  # @return [Boolean] true if user has system admin access, false otherwise
  def has_system_admin_access? 
  	self.system_admin?
	end

	# Role List
  #
  # @return [String] the roles as a string
  def role_list
		return "Read" if self.read?
		return "Read, Edit" if self.edit?
		return "Read, Edit, System Admin" if self.system_admin?
		return "err!"
	end

end
