class Mdr

  require 'rest-client'
  include ActiveModel::Naming
  include ActiveModel::Conversion
  include ActiveModel::Validations

  C_FORM = "http://www.assero.co.uk/BusinessForm#Form";
  C_GROUP ="http://www.assero.co.uk/BusinessForm#NormalGroup";
  C_COMMON_GROUP = "http://www.assero.co.uk/BusinessForm#CommonGroup";
  C_PLACEHOLDER = "http://www.assero.co.uk/BusinessForm#Placeholder";
  C_BC_ITEM = "http://www.assero.co.uk/BusinessForm#BcProperty";
  C_QUESTION = "http://www.assero.co.uk/BusinessForm#Question";
  C_TC = "http://www.assero.co.uk/BusinessOperational#TcReference";

  attr_accessor :data

  C_CLASS_NAME = "Mdr"

  def self.discover
    object = api_call("discover", {}, "discover info")
    return object
  end

  def self.forms_list
    object = api_call("list", {:label => "Form"}, "forms")
    return object
  end

  def self.forms_all
    object = api_call("", {:label => "Form"}, "forms")
    return object
  end

  def self.form(id, ns)
    # Note that the path is set to the id
    object = api_call(id, {:namespace => ns}, "form")
    return object
  end

  def self.domains
    object = api_call("list", {:label => "SDTM Sponsor Domain"}, "domains")
    return object
  end

  def self.domain
    # Note that the path is set to the id
    object = api_call(id, {:namespace => ns}, "domain")
    return object
  end

  def self.thesaurus_concept(id, ns)
    # Note that the path is set to the id
    object = api_call(id, {:namespace => ns}, "thesaurus concept")
    return object
  end

  def self.bc_property(id, ns)
    # Note that the path is set to the id
    object = api_call(id, {:namespace => ns}, "biomedical concept property")
    return object
  end

  def self.form_to_html(id, ns, pdf)
    form_json = api_call(id, {:namespace => ns}, "form")
    form_node(form_json.data)
    html = Crf.create(form_json.data, nil, {:annotate => false, :full => false})
    return html
  end

  def self.form_to_html_annotations(id, ns, pdf)
    form_json = api_call(id, {:namespace => ns}, "form")
    annotations = api_call("form_annotations", {:id => id, :namespace => ns}, "form annotations")
    form_node(form_json.data)
    html = Crf.create(form_json.data, annotations.data, {:annotate => true, :full => false})
    return html
  end

  def self.form_to_hash(id, ns)
    form_hash = api_call(id, {:namespace => ns}, "form")
    if form_hash.errors.empty?
      form_node(form_hash.data)
      return form_hash.data
    else
      ConsoleLogger.info(C_CLASS_NAME, __method__.to_s, "Errors retrieving form #{ns}##{id}. #{form_hash.errors.full_messages.to_sentence}")
      return {}
    end
  end

  private

  def self.api_call(path, params, item_text)
    object = self.new
    object.errors.clear
    begin
      url = "#{url_root}#{path}"
      if !Rails.env.test?
        response = RestClient.get url, {:params => params, :accept => :json}
        write_text_file(response, "api", api_path_to_filename(response.request.url)) if Rails.env.development?
        if response.code == 200
          object.data = JSON.parse(response, :symbolize_names => true)
        else
          object.errors.add(:base, "Error retrieving #{item_text} from the MDR.")
        end
      else
        request = RestClient::Request.new(:method => :get, :url => url, :headers => {:accept => :json, :params => params})
        response = read_text_file("api", api_path_to_filename(request.url))
        object.data = JSON.parse(response, :symbolize_names => true)
      end
    rescue => e
      object.errors.add(:base, "Error retrieving #{item_text} from the MDR. [Exception: " + e.to_s + "]")
    end
    return object
  end

  def self.form_node(node)
    unless node.nil?
      if !node[:children].blank?
        node[:children].each do |child|
          if child[:type] == Mdr::C_QUESTION
            amendments = Amendment.where(cid: child[:id], namespace: child[:namespace])
            amendments.each do |amendment|
              if amendment.property == "QuestionText"
                child[:question_text] = amendment.study_value
              elsif amendment.property == "Enabled"
                child[:enabled] = amendment.study_value
              end
            end
          elsif child[:type] == Mdr::C_TC
            tc = Mdr.thesaurus_concept(child[:subject_ref][:id], child[:subject_ref][:namespace])
            child[:subject_data] = tc.data
          elsif child[:type] == Mdr::C_BC_ITEM
            bc_property = Mdr.bc_property(child[:property_ref][:subject_ref][:id], child[:property_ref][:subject_ref][:namespace])
            child[:property_ref][:subject_data] = bc_property.data
          end
          form_node(child)
        end
      end
    end
  end

  # Methods to support testing
  if !Rails.env.production?

    def self.read_text_file(sub_dir, filename)
      text = ""
      full_path = set_path(sub_dir, filename, :read)
      File.open(full_path, "r") do |f|
        text = f.read
      end
      return text
    end

    def self.write_text_file(item, sub_dir, filename)
      full_path = set_path(sub_dir, filename, :write)
      File.open(full_path, "w+") do |f|
        f.write(item)
      end
    end

    def self.read_yaml_file(sub_dir, filename)
      full_path = set_path(sub_dir, filename, :read)
      return YAML.load_file(full_path)
    end

    def self.write_yaml_file(item, sub_dir, filename)
      full_path = set_path(sub_dir, filename, :write)
      File.open(full_path, "w+") do |f|
        f.write(item.to_yaml)
      end
    end

    def self.api_path_to_filename(url)
      root = url_root
      result = url.gsub(root, '')
      result = result.gsub('?label=', '')
      #result = result.gsub('?namespace=http://www.assero.co.uk/', '')
      result = result.gsub('?namespace=http%3A%2F%2Fwww.assero.co.uk%2F', '-')
      result = result.gsub('/', '-')
      result = result.gsub('%2F', '-')
      #ConsoleLogger.log(C_CLASS_NAME, "api_path_to_filename", "URL=#{url}, Result=#{result}")
      return "#{result}.txt"
    end

    def self.set_path(sub_dir, filename, mode)
      return Rails.root.join partial_path(sub_dir, filename, mode)
    end

    def self.partial_path(sub_dir, filename, mode)
      return "spec/fixtures/#{sub_dir}/#{filename}" if mode == :read
      return "public/#{sub_dir}/#{filename}" # Default for write
    end

  end

  def self.url_root
    #ConsoleLogger.log(C_CLASS_NAME, "url_root", "Start")
    url = "#{ENV['glandon_api_protocol']}://#{ENV['glandon_api_username']}:#{ENV['glandon_api_password']}@#{ENV['glandon_api_ip']}"
    url += ":#{ENV['glandon_api_port']}" if !ENV['glandon_api_port'].blank?
    url += "/api/"
    #ConsoleLogger.log(C_CLASS_NAME, "url_root", "URL=#{url}")
    return url
  end

end
