class Visit < ActiveRecord::Base

	belongs_to :study_version
	has_many :elements, :dependent => :destroy
	
	def self.ordinals(study_version_id)
		visits = Visit.where(study_version_id: study_version_id).order('ordinal asc')
		ordinal = 1
		visits.each do |item|
			item.ordinal = ordinal
			ordinal += 1
			item.save
		end
	end

end
