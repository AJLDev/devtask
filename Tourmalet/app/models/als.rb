require 'excel_wrapper'

class Als

	attr_reader :errors

  @@sheet_names =
  {
    main: "CRFDraft",
    forms: "Forms",
    fields: "Fields",
    folders: "Folders",
    code_lists: "DataDictionaries",
    code_list_items: "DataDictionaryEntries",
    #unit_lists: "UnitDictionaires",
    #unit_list_items: "UnitDictionaryEntries",
    matrices: "Matrices"     
  }

  # Initialize
  #
  # @return [Null] no return
  def initialize
  	@errors = ActiveModel::Errors.new(self)
  	file_location = APP_CONFIG['als_definiton_file']
  	@excel = ExcelWrapper.new({file_location: file_location})
  rescue Errno::ENOENT
    self.errors.add(:base, "Workbook was not created. The definition file did not exist")
    @excel = nil
  rescue Psych::SyntaxError 
    self.errors.add(:base, "Workbook was not created. The definition file contained syntax errors")
    @excel = nil
	end

	# Create study ALS
  #
  # @param study_version [Hash] The study hash
  # @return [Null] no return
  def study(study_version)
    study_hash = study_version.to_full_hash
    map = {}
    parse_tc_node(study_hash, map)
    process_tc_map(map)
		project_sheet(study_hash)
    forms_sheet(study_hash)
    fields_sheet(study_hash, map)
    folders_sheet(study_version)
    code_lists_sheet(map)
    code_list_items_sheet(map)
    matrices_sheet()
    all_remaining_sheets()
    matrix_sheet(study_version)
	end

  # To Definition Hash
  #
  # @return [Hash] the object as a hash
  def to_definition_hash
  #byebug
  	@excel.to_definition_hash
  end

  # To CSV. Entire workbook in series of CSV output
  #
  # @return [String] the workbook as set of sheets in CSV format.
  def to_csv()
    result = ""
    @excel.workbook.sheets.each do |sheet|
      result += "#{sheet.label.toupper}\n\n"
      result += sheet.to_csv
      result += "\n\n" 
    end
    return result
  end

  def to_excel(filename)
    full_path = @excel.save(filename)
    return full_path
  end

	def project_sheet(study_hash)
  	sheet = @excel.workbook.find_worksheet(@@sheet_names[:main])
		sheet.add_row([{label: "DraftName", value: study_hash[:semantic_version] }, {label: "ProjectName", value: study_hash[:name]}])
	end

  def forms_sheet(study_hash)
    sheet = @excel.workbook.find_worksheet(@@sheet_names[:forms])
    study_hash[:children].each_with_index do |child, index|
      fields = 
      [
        {label: "OID", value: clean_oid(child[:scoped_identifier][:identifier])}, 
        {label: "Ordinal", value: index + 1}, 
        {label: "DraftFormName", value: child[:label]}
      ]
      sheet.add_row(fields)
    end
  end

  def fields_sheet(study_hash, map)
    sheet = @excel.workbook.find_worksheet(@@sheet_names[:fields])
    study_hash[:children].each_with_index do |form, index|
      question_node(form, clean_oid(form[:scoped_identifier][:identifier]), sheet, 1, map)
    end
  end
  
  def code_lists_sheet(map)
    sheet = @excel.workbook.find_worksheet(@@sheet_names[:code_lists])
    map.each do |cl, entries|
      entry = entries[0]
      if entry[:status] == :as_is_single || entry[:status] == :use_first
        cl_oid = "#{cl}"
        sheet.add_row([{label: "DataDictionaryName", value: cl_oid}])
      elsif entry[:status] == :as_is_multiple
        entries.each do |entry|
          cl_oid = "#{cl}_#{entry[:index]}"
          sheet.add_row([{label: "DataDictionaryName", value: cl_oid}])
        end
      end
    end
  end
  
  def code_list_items_sheet(map)
    sheet = @excel.workbook.find_worksheet(@@sheet_names[:code_list_items])
    map.each do |cl, entries|
      entry = entries[0]
      if entry[:status] == :as_is_single || entry[:status] == :use_first
        cl_oid = "#{cl}"
        ordinal = 1
        entry[:items].each do |item|
          fields = 
          [
            {label: "DataDictionaryName", value: cl_oid }, 
            {label: "CodedData", value: item[:notation] }, 
            {label: "Ordinal", value: ordinal },
            {label: "UserDataString", value: item[:preferred_term] }
          ]
          sheet.add_row(fields)
          ordinal += 1
        end
      elsif entry[:status] == :as_is_multiple
        entries.each do |entry|
          ordinal = 1
          cl_oid = "#{cl}_#{entry[:index]}"
          entry[:items].each do |item|
            fields = 
            [
              {label: "DataDictionaryName", value: cl_oid }, 
              {label: "CodedData", value: item[:notation] }, 
              {label: "Ordinal", value: ordinal },
              {label: "UserDataString", value: item[:preferred_term] }
            ]
            sheet.add_row(fields)
            ordinal += 1
          end
        end
      end
    end
  end
  
  def folders_sheet(study_version)
    sheet = @excel.workbook.find_worksheet(@@sheet_names[:folders])
    visits = Visit.where(study_version_id: study_version.id).order('ordinal asc')
    visits.each do |visit|
      fields = 
      [ 
        { label: "OID", value: "#{visit.short_name}" },
        { label: "Ordinal", value: "#{visit.ordinal}" },
        { label: "FolderName", value: "#{visit.long_name}" },
      ]
      sheet.add_row(fields)
    end
  end

  def matrices_sheet()
    sheet = @excel.workbook.find_worksheet(@@sheet_names[:matrices])
    fields = 
    [
      {label: "MatrixName", value: "Primary" }, 
      {label: "OID", value: "PRIMARY" }, 
    ]
    sheet.add_row(fields)
  end

  def matrix_sheet(study_version)
    columns = []
    visits = Visit.where(study_version_id: study_version.id).order('ordinal asc')
    forms = Form.where(study_version_id: study_version.id).order('ordinal asc')
    columns << { label: "Matrix:PRIMARY", default: '', ordinal: 1}
    visits.each do |visit|
      columns << { label: visit.short_name, default: '', ordinal: visit.ordinal + 1}
    end
    sheet = @excel.workbook.add_worksheet( { label: "Matrix1#PRIMARY", columns: columns} )
    forms.each do |form|
      fields = [ { label: "Matrix:PRIMARY", value: "#{form.identifier}" } ]
      visits.each do |visit|
        element = Element.find_element(study_version.id, visit.id, form.id)
        mark = element.nil? ? "" : "X"
        fields << { label: "#{visit.short_name}", value: mark }
      end
      sheet.add_row(fields)
    end
  end

  def all_remaining_sheets
    @excel.workbook.sheets.each do |sheet|
      if !@@sheet_names.has_value?(sheet.label)
        sheet.add_row([]) # Add heading row and blank row
      end
    end
  end

  # Parse Question Nodes. Add in for each form. Recursive method.
  #
  # @param node [Hash] the node 
  # @param form_oid [String] the form oid to which the questions belong
  # @param sheet [Object] the sheet object to which the questions are to be added
  # @param ordinal [Integer] the question ordinal within the form
  # @param map [Hash] the terminology map
  # @return [Integer] the updated ordinal
  def question_node(node, form_oid, sheet, ordinal, map)
    if !node[:children].blank?
      node[:children].each do |child|
        if child[:type] == Mdr::C_QUESTION
          fields = 
          [
            {label: "FormOID", value: form_oid}, 
            #{label: "FieldOID", value: child[:id]}, 
            {label: "FieldOID", value: "#{form_oid}_#{ordinal}"}, 
            {label: "Ordinal", value: ordinal}, 
            {label: "PreText", value: child[:question_text]},
            {label: "HelpText", value: child[:completion]},
            {label: "DataDictionaryName", value: set_cl_oid(child, map)}
          ]
          fields += set_datatype_and_format(child)
          sheet.add_row(fields)
          ordinal += 1
        elsif child[:type] == Mdr::C_BC_ITEM
          fields = 
          [
            {label: "FormOID", value: form_oid}, 
            #{label: "FieldOID", value: child[:property_ref][:subject_data][:id]}, 
            {label: "FieldOID", value: "#{form_oid}_#{ordinal}"}, 
            {label: "Ordinal", value: ordinal}, 
            {label: "PreText", value: child[:property_ref][:subject_data][:question_text]},
            {label: "HelpText", value: child[:completion]},
            {label: "DataDictionaryName", value: set_cl_oid(child, map)}
          ]
          fields += set_datatype_and_format(child)
          sheet.add_row(fields)
          ordinal += 1
        else
          ordinal = question_node(child, form_oid, sheet, ordinal, map)
        end
      end
    end
    return ordinal
  end

  # Parse TC Node. Build map of used terminology. Recursive method.
  #
  # @param node [Hash] the node 
  # @param map [Hash] the tc map
  # @return [Null] null return, map is updated
  def parse_tc_node(node, map)
    if !node[:children].blank?
      node[:children].each do |child|
        if child[:type] == Mdr::C_QUESTION || child[:type] == Mdr::C_BC_ITEM
          if !child[:children].blank?
            cl = extract_cl(child[:children][0][:subject_ref][:id])
            if !map.has_key?(cl)
              map[cl] = []
            end
            set = { index: 0, items: [] }
            child[:children].each do |tc|
              set[:items] <<  { id: tc[:subject_ref][:id], notation: tc[:subject_data][:notation], 
                preferred_term: tc[:subject_data][:preferredTerm] }
            end
            set[:index] = map[cl].length + 1
            map[cl] << set
            child[:cl_map] = { key: cl, index: set[:index] }
          else
            child[:cl_map] = {}
          end 
        else
          parse_tc_node(child, map)
        end
      end
    end
  end

  # Process TC Map. Sets the status for each entry. Determines if single or multiple
  # entries or if mutiple entries all match (i.e. use a single entry).
  #
  # @param map [Hash] the tc map
  # @return [Null] null return, map is updated
  def process_tc_map(map)
    map.each do |key, entries|
      if entries.length == 1
        entries[0][:status] = :as_is_single
      else
        if entries.all? { |x| x[:items] == entries[0][:items] }
          entries.each { |x| x[:status] = :use_first }
        else
          entries.each { |x| x[:status] = :as_is_multiple }
        end 
      end
    end
  end

  # Extract CL Identifier
  #
  # @param node [id] the CLI identifier
  # @return [String] the CL identifier
  def extract_cl(id)
    main_parts = id.split("-")
    parts = main_parts[1].split("_")
    parts.pop
    return parts.join("_")
  end

  # Set Datatype and Format
  #
  # @param node [Hash] the current node
  # @return [Array] array of hash of label value pairs
  def set_datatype_and_format(node)
    case node[:datatype]
      when "string"
        if node[:cl_map].blank?
          return [ { label: "DataFormat" , value: "$#{node[:format]}" }, { label: "ControlType" , value: "Text" } ]
        else
          return [ { label: "DataFormat" , value: "$#{node[:format]}" }, { label: "ControlType" , value: "DropDownList" } ]
        end
      when "integer"
        return [ { label: "DataFormat" , value: "$#{node[:format]}" }, { label: "ControlType" , value: "Text" } ]
      when "boolean"
        return [ { label: "DataFormat" , value: "$1" }, { label: "ControlType" , value: "Text" } ]
      when "dateTime"
        return [ { label: "DataFormat" , value: "dd MMM yyyy" }, { label: "ControlType" , value: "DateTime" } ]
      when "date"
        return [ { label: "DataFormat" , value: "dd MMM yyyy" }, { label: "ControlType" , value: "DateTime" } ]
      when "time"
        return [ { label: "DataFormat" , value: "HH:nn" }, { label: "ControlType" , value: "DateTime" } ]
      when "float"
        return [ { label: "DataFormat" , value: "#{node[:format]}" }, { label: "ControlType" , value: "Text" } ]
      else
        return [ { label: "DataFormat" , value: "$#{node[:format]}" }, { label: "ControlType" , value: "Text" } ]
    end
  end

  # Set Node CL OID
  #
  # @param node [Hash] the current node
  # @param map [Hash] the terminology map
  # @return [String] the OID
  def set_cl_oid(node, map)
    cl_oid = ""
    if !node[:cl_map].blank?
      entry = map[node[:cl_map][:key]][node[:cl_map][:index] - 1]
      if entry[:status] == :as_is_single || entry[:status] == :use_first
        cl_oid = "#{node[:cl_map][:key]}"
      elsif entry[:status] == :as_is_multiple
        cl_oid = "#{node[:cl_map][:key]}_#{node[:cl_map][:index]}"
      end
    end
    return cl_oid
  end

  # Clean OID
  #
  # @param oid [String] the OID
  # @return [String] the OID
  def clean_oid(oid)
    return oid.gsub(/[^0-9A-Za-z_]/,'')
  end

  if Rails.env.test?

    def get_sheet(label)
      return @excel.workbook.find_worksheet(label)
    end

  end

  # The following methods are needed to be minimally implemented for errors to function
  def read_attribute_for_validation(attr)
    send(attr)
  end

  def self.human_attribute_name(attr, options = {})
    attr
  end

  def self.lookup_ancestors
    [self]
  end

end