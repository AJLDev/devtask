class StudyVersion < ActiveRecord::Base

  has_many :forms, :dependent => :destroy
  has_many :visits, :dependent => :destroy
  has_many :elements, :dependent => :destroy
  has_many :amendments, :dependent => :destroy

  validates_presence_of :name
  validates_presence_of :description
  validates_presence_of :protocol_name

  include IsoRegistrationState

  @@datatype_map =
      {
          "S" => "text",
          "I" => "integer",
          "F" => "float",
          "D" => "date",
          "T" => "time",
          "D+T" => "datetime",
          "B" => "boolean",
      }

  # Next Edit Version. If new version needed clone
  #
  # @return [StudyVersion] the object to be edited
  def edit_version
    if self.new_version?
      return self.clone
    else
      return self
    end
  end

  # Update State. Update the state and version if necessary.
  #
  # @return [Null] no return
  def update_state(params)
    self.update(params)
    self.reload
    self.next_state_version
    self.save
  end

  # Clone the Object
  #
  # @return [StudyVersion] the new object
  def clone
    new_study_version = StudyVersion.create(self.attributes.merge({ id: nil }))
    self.amendments.each do |amendment|
      new_study_version.amendments << Amendment.create(amendment.attributes.merge({ id: nil }))
    end
    self.elements.each do |element|
      form = Form.find(element.form_id)
      visit = Visit.find(element.visit_id)
      new_element = Element.create(element.attributes.merge({ id: nil }))
      new_form = Form.create(form.attributes.merge({ id: nil }))
      new_visit = Visit.create(visit.attributes.merge({ id: nil }))
      new_study_version.forms << new_form
      new_study_version.visits << new_visit
      new_study_version.elements << new_element
      new_form.elements << element
      new_visit.elements << element
    end
    new_study_version.next_clone_version
    new_study_version.state = self.state_on_edit
    new_study_version.save
    return new_study_version
  end

  # Generate the Schedule fo Assessments Table
  #
  # @return [String] the HTML SOA
  def soa
    Visit.order(:ordinal)
    Form.order(:ordinal)
    visits = Visit.where(study_version_id: self.id).order('ordinal asc')
    forms = Form.where(study_version_id: self.id).order('ordinal asc')
    html = "<table id=\"soa_table\" class=\"table table-striped table-bordered table-condensed soa-table\">"
    html += "<thead>"
    html += "<tr>"
    html += "<th>SoA</th>"
    visits.each do |visit|
      #html += "<th id=\"#{visit.id}\" style=\"width:5%\" class=\"soa-column text-center\">#{visit.short_name}</th>"
      html += "<th id=\"#{visit.id}\" class=\"soa-column text-center\">#{visit.short_name}</th>"
    end

    html += "</tr>"
    html += "</thead>"
    html += "<tbody>"
    forms.each do |form|
      html += "<tr>"
      html += "<td class=\"soa-row\" id=\"#{form.id}\">#{form.label}</td>"
      visits.each do |visit|
        element = Element.find_element(self.id, visit.id, form.id)
        if !element.nil?
          html += "<td class=\"soa-element text-center\" form_id=\"#{form.id}\" visit_id=\"#{visit.id}\" id=\"#{element.id}\">"
          html += "<span class=\"glyphicon glyphicon-ok text-success\"></span></td>"
        else
          html += "<td class=\"soa-element\" form_id=\"#{form.id}\" visit_id=\"#{visit.id}\" id=\"0\"></td>"
        end
      end
      html += "</tr>"
    end
    html += "</tbody>"
    html += "</table>"
    return html
  end

  # Generate a hash for the study version. Don't include full form details.
  #
  # @return [Hash] the hash
  def to_hash
    result = to_base_hash
    forms = Form.where({study_version_id: self.id}).order('ordinal asc')
    forms.each { |x| result[:children] << x.to_hash }
    return result
  end

  # Generate a full hash for the study version with full form details
  #
  # @return [Hash] the hash
  def to_full_hash
    result = to_base_hash
    forms = Form.where(study_version_id: self.id).order('ordinal asc')
    forms.each {|form| result[:children] << Mdr.form_to_hash(form.form_id, form.form_namespace)}
    return result
  end

  def to_als(filename)
    als = Als.new
    if als.errors.count == 0
      als.study(self)
      full_path = als.to_excel(filename)
      return { full_path: full_path, error_count: 0, error_message: "" }
    else
      return { full_path: "", error_count: 1, error_message: als.errors.full_messages.to_sentence }
    end
  end

  def to_xml
    # Build the document and the header elements
    odm_document = Odm.new("ODM-#{self.id}", "Assero", "Tourmalet", Version::VERSION)
    odm = odm_document.root
    study = odm.add_study("S-#{self.id}")
    global_variables = study.add_global_variables()
    global_variables.add_study_name("#{self.name}")
    global_variables.add_study_description("#{self.description}")
    global_variables.add_protocol_name("#{self.protocol_name}")
    metadata_version = study.add_metadata_version("MDV-#{self.id}", "Metadata for #{self.description}", "Study export.")
    protocol = metadata_version.add_protocol()
    # Get the set of visits, forms and elements.
    visits = Visit.where(study_version_id: self.id).order('ordinal asc')
    forms = Form.where(study_version_id: self.id).order('ordinal asc')
    elements = Element.where(study_version_id: self.id).find_each
    # Build the Protocol element.
    visits.each do |visit|
      protocol.add_study_event_ref("SE-#{visit.ordinal}", "#{visit.ordinal}", "Yes", "")
    end
    # Build the StudyEventDef elements
    visits.each do |visit|
      study_event_def = metadata_version.add_study_event_def("SE-#{visit.ordinal}", "#{visit.long_name}", "No", "Scheduled", "")
      elements = Element.where(study_version_id: self.id, visit_id: visit.id).find_each
      elements.each do |element|
        #ConsoleLogger.log(C_CLASS_NAME, "to_xml", "Element=#{element.to_json}")
        form = Form.find(element.form_id)
        study_event_def.add_form_ref("#{form.form_id}", "#{form.ordinal}", "Yes", "")
      end
    end
    # Now the remainder of the metadata
    forms.each do |form|
      form_json = Mdr.form_to_hash(form.form_id, form.form_namespace)
      #ConsoleLogger.log(C_CLASS_NAME, "to_xml", "JSON=#{form_json}")
      xml_node(form_json, metadata_version, nil)
    end
    return odm_document.to_xml
  end

  # Next Clone Version. Update the version
  #
  # @return [Null] no return.
  def next_clone_version
    sv = SemanticVersion.from_s(self.semantic_version)
    sv.increment_minor
    self.semantic_version = sv.to_s
    self.version += 1
  end

  # Next State Version. Update the version based on state.
  #
  # @return [Null] no return.
  def next_state_version
    sv = SemanticVersion.from_s(self.semantic_version)
    if self.state == :standard.to_s
      sv.increment_major
    else
      sv.increment_minor
    end
    self.semantic_version = sv.to_s
  end

  private

  def xml_node(node, metadata, parent)
    #ConsoleLogger.log(C_CLASS_NAME, "xml_node", "Node=#{node[:id]}, Type=#{node[:type]}")
    if node[:type] == Mdr::C_FORM
      form_def = metadata.add_form_def("#{node[:id]}", "#{node[:scoped_identifier][:identifier]}", "No")
      node[:children].each do |child|
        xml_node(child, metadata, form_def)
      end
    elsif node[:type] == Mdr::C_COMMON_GROUP
      #ConsoleLogger.log(C_CLASS_NAME, "xml_node", "Node=#{node[:id]}, Type=#{node[:type]}")
      parent.add_item_group_ref("#{node[:id]}", "#{node[:ordinal]}", "No", "")
      item_group_def = metadata.add_item_group_def("#{node[:id]}", "#{node[:label]}", "No", "", "", "", "", "", "")
      node[:children].each do |child|
        xml_node(child, metadata, item_group_def)
      end
    elsif node[:type] == Mdr::C_GROUP
      #ConsoleLogger.log(C_CLASS_NAME, "xml_node", "Node=#{node[:id]}, Type=#{node[:type]}")
      sub_group = false
      node[:children].each do |child|
        if child[:type] == Mdr::C_GROUP || child[:type] == Mdr::C_COMMON_GROUP
          sub_group = true
        end
      end
      if !sub_group
        parent.add_item_group_ref("#{node[:id]}", "#{node[:ordinal]}", "No", "")
        item_group_def = metadata.add_item_group_def("#{node[:id]}", "#{node[:label]}", "No", "", "", "", "", "", "")
        node[:children].each do |child|
          xml_node(child, metadata, item_group_def)
        end
      else
        node[:children].each do |child|
          xml_node(child, metadata, parent)
        end
      end
    elsif node[:type] == Mdr::C_PLACEHOLDER
      #ConsoleLogger.log(C_CLASS_NAME, "xml_node", "Node=#{node[:id]}, Type=#{node[:type]}")
      parent.add_item_ref("#{node[:id]}", "#{node[:ordinal]}", "No", "", "", "", "", "")
      item_def = metadata.add_item_def("#{node[:id]}", "#{node[:label]}", "No", "", "", "", "", "", "")
    elsif node[:type] == Mdr::C_QUESTION || node[:type] == Mdr::C_BC_ITEM
      #ConsoleLogger.log(C_CLASS_NAME, "xml_node", "Node=#{node[:id]}, Type=#{node[:type]}")
      parent.add_item_ref("#{node[:id]}", "#{node[:ordinal]}", "No", "", "", "", "", "")
      xml_datatype = to_xml_datatype(node[:datatype])
      xml_length = to_xml_length(node[:datatype], node[:format])
      xml_digits = to_xml_significant_digits(node[:datatype], node[:format])
      item_def = metadata.add_item_def("#{node[:id]}", "#{node[:label]}", "#{xml_datatype}", "#{xml_length}", "#{xml_digits}", "", "", "", "")
      question = item_def.add_question()
      question.add_translated_text("#{node[:question_text]}")
      if !node[:children].empty?
        code_list_ref = item_def.add_code_list_ref("#{node[:id]}-CL")
        code_list = metadata.add_code_list("#{node[:id]}-CL", "Code list for #{node[:label]}", "text", "")
        node[:children].each do |child|
          xml_node(child, metadata, code_list)
        end
      end
    elsif node[:type] == Mdr::C_TC
      #ConsoleLogger.log(C_CLASS_NAME, "xml_node", "Node=#{node[:id]}, Type=#{node[:type]}")
      code_list_item = parent.add_code_list_item("#{node[:subject_data][:notation]}", "", "#{node[:ordinal]}")
      decode = code_list_item.add_decode()
      decode.add_translated_text("#{node[:subject_data][:label]}")
    else
      #ConsoleLogger.log(C_CLASS_NAME, "xml_node", "Node=#{node[:id]}, Type=#{node[:type]}")
    end
  end

  def to_xml_datatype(datatype)
    if @@datatype_map.has_key?(datatype)
      return @@datatype_map[datatype]
    else
      return "text"
    end
  end

  def to_xml_length(datatype, format)
    if datatype == "S"
      return format
    elsif datatype == "I"
      return format
    elsif datatype == "F"
      parts = format.split('.')
      length = (parts[0].to_i) - 1
      return length
    else
      return ""
    end
  end

  def to_xml_significant_digits(datatype, format)
    if datatype == "F"
      parts = format.split('.')
      digits = (parts[1].to_i)
      return digits
    else
      return ""
    end
  end

  # Generate the base hash, no children filled in.
  def to_base_hash
    result =
        {
            :id => self.id,
            :name => self.name,
            :description => self.description,
            :protocol_name => self.protocol_name,
            :semantic_version => self.semantic_version,
            :children => []
        }
    study = Study.find(self.study_id)
    result[:identifier] = study[:identifier]
    return result
  end

end
