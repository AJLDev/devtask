class Form < ActiveRecord::Base

	belongs_to :study_version
	has_many :elements, :dependent => :destroy

	def self.ordinals(study_version_id)
		forms = Form.where(study_version_id: study_version_id).order('ordinal asc')
		ordinal = 1
		forms.each do |item|
			item.ordinal = ordinal
			ordinal += 1
			item.save
		end
	end

	def to_hash
		result = 
		{ 
			id: self.id, 
			form_id: self.form_id, 
			form_namespace: self.form_namespace, 
			identifier: self.identifier, 
			ordinal: self.ordinal, 
			label: self.label
		}
		return result
	end
	
end
