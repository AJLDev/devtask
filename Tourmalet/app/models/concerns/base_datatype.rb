module BaseDatatype

	C_CLASS_NAME = "BaseDatatype"

	C_STRING = "string"
	C_INTEGER = "integer"
	C_POSITIVE_INTEGER = "positiveInteger"
	C_BOOLEAN = "boolean"
	C_DATETIME = "dateTime"
	C_DATE = "date"
	C_TIME = "time"
	C_FLOAT = "float"

end