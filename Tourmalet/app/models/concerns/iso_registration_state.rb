module IsoRegistrationState

  C_CLASS_NAME = "IsoRegistrationState"

  extend ActiveSupport::Concern

  included do
    enum state: {not_set: 0, incomplete: 1, candidate: 2, recorded: 3, qualified: 4, standard: 5, retired: 6, superseded: 7}
    #user_hash = APP_CONFIG['user_settings']
    @@state_info = [
        { 
          :key => :not_set, 
          :label => "Not set", 
          :definition => "State has not beeen set",
          :delete_enabled => false, 
          :edit_enabled => true, 
          :edit_up_version => true, # New items set to this state 
          :state_on_edit => :incomplete,
          :can_be_current => false, 
          :next_state => :incomplete
        },
        { 
          :key => :incomplete, 
          :label => "Incomplete", 
          :definition => "Submitter wishes to make the community that uses this metadata register aware of the existence of an Administered Item in their local domain.",        
          :delete_enabled => true, 
          :edit_enabled  => true,
          :edit_up_version => false,
          :state_on_edit => :incomplete,
          :can_be_current => false, 
          :next_state => :qualified
        },
        { 
          :key => :candidate, 
          :label => "Candidate", 
          :definition => "The Administered Item has been proposed for progression through the registration levels.",
          :delete_enabled => false, 
          :edit_enabled  => true,
          :edit_up_version => false,
          :state_on_edit => :candidate,
          :can_be_current => false, 
          :next_state => :recorded
        },
        { 
          :key => :recorded, 
          :label => "Recorded", 
          :definition => "The Registration Authority has confirmed that: a) all mandatory metadata attributes have been completed.",
          :delete_enabled => false, 
          :edit_enabled  => true,
          :edit_up_version => true,
          :state_on_edit => :recorded,
          :can_be_current => false, 
          :next_state => :qualified
        },
        { 
          :key => :qualified, 
          :label => "Qualified", 
          :definition => "The Registration Authority has confirmed that: a) the mandatory metadata attributes are complete and b) the mandatory metadata attributes conform to applicable quality requirements.",
          :delete_enabled => false, 
          :edit_enabled  => true,
          :edit_up_version => true,
          :state_on_edit => :qualified,
          :can_be_current => false, 
          :next_state => :standard
        },
        { 
          :key => :standard, 
          :label => "Standard", 
          :definition => "The Registration Authority confirms that the Administered Item is: a) of sufficient quality and b) of broad interest for use in the community that uses this metadata register.",
          :delete_enabled => false, 
          :edit_enabled  => true,
          :edit_up_version => true,
          :state_on_edit => :incomplete,
          :can_be_current => true, 
          :next_state => :superseded
        },
        { 
          :key => :retired, 
          :label => "Retired", 
          :definition => "The Registration Authority has approved the Administered Item as: a) no longer recommended for use in the community that uses this metadata register and b) should no longer be used.",
          :delete_enabled => false, 
          :edit_enabled  => false,
          :edit_up_version => false,
          :state_on_edit => :retired,
          :can_be_current => false, 
          :next_state => :retired
        },
        { 
          :key => :superseded, 
          :label => "Superseded", 
          :definition => "The Registration Authority determined that the Administered Item is: a) no longer recommended for use by the community that uses this metadata register, and b) a successor Administered Item is now preferred for use.",
          :delete_enabled => false, 
          :edit_enabled => false, 
          :edit_up_version => false, 
          :state_on_edit => :superseded,
          :can_be_current => false, 
          :next_state => :superseded
        }
    ]

  end


  # Add class methods
  module ClassMethods
  
    # Get the released state
    #
    # @return [string] The released state
    def the_released_state
      return :standard
    end
    
    # Get the intial state
    #
    # @return [string] The released state
    def the_initial_state
      return :incomplete
    end

    # Get the human readable label for a state
    #
    # @return [string] The label
    def state_label(state)
      state_info = self.class_variable_get(:'@@state_info')
      info = state_info[StudyVersion.states[state]]
      return info[:label]
    end

    # Get the definition for a state
    #
    # @return [string] The definition
    def state_definition(state)
      state_info = self.class_variable_get(:'@@state_info')
      info = state_info[StudyVersion.states[state]]
      return info[:definition]
    end

  end

  # Test if registered
  # 
  # @return [Boolean] True if a registration state is present
  def registered?()
    return self.state != :not_set.to_s
  end

  # Is the item at the released state
  #
  # @return [boolean] True if in the released state, false otherwise
  def released_state?
    self.state == :standard.to_s
  end

  # Get the next state
  # 
  # @return [string] The next state
  def next_state()
    info = @@state_info[StudyVersion.states[self.state]]
    return info[:next_state]
  end

  # Get the human readable label for a state
  #
  # @return [string] The label
  def state_label
    info = @@state_info[StudyVersion.states[self.state]]
    return info[:label]
  end

  # Get the definition for a state
  #
  # @return [string] The definition
  def state_definition
    info = @@state_info[StudyVersion.states[self.state]]
    return info[:definition]
  end

  # Can the item be edited
  #
  # @return [string] The next state
  def edit?
    info = @@state_info[StudyVersion.states[self.state]]
    return info[:edit_enabled]
  end

  # Can the item be deleted
  #
  # @return [string] The next state
  def delete?
    info = @@state_info[StudyVersion.states[self.state]]
    return info[:delete_enabled]
  end

  # Returns the new state after the item has been edited
  #
  # @return [string] The next state
  def state_on_edit
    info = @@state_info[StudyVersion.states[self.state]]
    return info[:state_on_edit]
  end

  # Returns true if the version needs to be updated after an edit
  #
  # @return [string] The next state
  def new_version?
    info = @@state_info[StudyVersion.states[self.state]]
    return info[:edit_up_version]
  end

  # Returns true if the state can be changed
  #
  # @return [string] The next state
  def can_be_changed?
    info = @@state_info[StudyVersion.states[self.state]]
    return info[:next_state].to_s != self.state
  end

end