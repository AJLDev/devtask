#require 'writeexcel'
require 'write_xlsx'

#require 'excel_wrapper/ew_workbook'

class ExcelWrapper

	attr_reader :filename, :workbook

  # Initialize
  #
  # @param args [Hash] the argument hash
  # @raise Errno::ENOENT if file does not exisit
  # @raise Psych::SyntaxError  if the file has invalid syntax
  # @return [Null] no return
  def initialize(args)
  	# Load the workbook configuration
  	file_location = ""
  	file_location = args[:file_location] if args[:file_location]
  	@filename = Rails.root.join file_location
		wb_definition = YAML.load_file(@filename)
		# Construct the workbook
		@workbook = ExcelWrapper::EwWorkbook.new(wb_definition)
	end

  # To Definition Hash
  #
  # @return [Hash] the object as a hash
  def to_definition_hash
  	return { filename: @filename.to_s, workbook: @workbook.to_definition_hash }
  end

  # To xlsx. Export to an xlsx file
  #
  # @param full_path [String] full path of the file to be written
  # @return [Null] no return.
  def save(filename)
    full_path = Rails.root.join "public/upload/#{filename}"
    #workbook = WriteExcel.new(full_path)
    workbook = WriteXLSX.new(full_path)
    @workbook.sheets.each do |worksheet|
      ws = workbook.add_worksheet(worksheet.label)
      worksheet.rows.each_with_index do |row, row_index|
        row.each_with_index do |col, col_index|
          ws.write(row_index, col_index, col)
        end
      end
    end
    workbook.close
    return full_path
  end

end