class Amendment < ActiveRecord::Base

  belongs_to :study_version

  def to_hash	
    hash = {}
    hash[:cid] = self.cid
    hash[:namespace] = self.namespace
    hash[:property] = self.property
    hash[:study_value] = self.study_value
    hash[:default_value] = self.default_value
    hash[:datatype] = self.datatype
    return hash
  end

end
