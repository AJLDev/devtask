class Element < ActiveRecord::Base

	belongs_to :study_version
	belongs_to :visit
	belongs_to :form

	def self.find_element(study_version_id, visit_id, form_id)
		elements = Element.where(study_version_id: study_version_id, form_id: form_id, visit_id: visit_id)
		return elements[0] if elements.count > 0
		return nil
	end
	
end
