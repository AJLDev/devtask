class MdrsController < ApplicationController

	before_action :authenticate_user!
  before_action :authorized?

  C_CLASS_NAME = "MdrsController"
	
	#def forms_all
	#	forms = Mdr.forms_all
	#  if forms.errors.empty?
  #		render :json => forms.data, :status => 200
  #	else
  #		render :json => { :errors => forms.errors.full_messages }, :status => 422
  #	end
	#end

	#def forms_list
	#	forms = Mdr.forms_list
	#	if forms.errors.empty?
  #		render :json => forms.data, :status => 200
  #	else
  #		render :json => { :errors => forms.errors.full_messages }, :status => 422
  #	end
	#end

	def forms
		forms = Mdr.forms_all
	  if forms.errors.empty?
	  	results = []
	  	forms.data.uniq! {|x| x[:scoped_identifier][:identifier] }
	  	forms.data.each { |x| results << {id: x[:scoped_identifier][:identifier], identifier: x[:scoped_identifier][:identifier], 
	  		label: x[:label], owner: x[:registration_state][:owner]} }
  		render :json => results, :status => 200
  	else
  		render :json => { :errors => forms.errors.full_messages }, :status => 422
  	end
	end

	# @todo Should really be in a separate controller Mdrs/Forms as Show action
	def form
    form = Mdr.form(params[:id], params[:namespace])
    if form.errors.empty?
    	render :json => form.data, :status => 200
  	else
    	render :json => { :errors => form.errors.full_messages }, :status => 422
  	end
	end

	def thesaurus_concept
    tc = Mdr.thesaurus_concept(params[:id], params[:namespace])
    if tc.errors.empty?
    	render :json => tc.data, :status => 200
  	else
    	render :json => { :errors => tc.errors.full_messages }, :status => 422
  	end
	end

	def bc_property
	    property = Mdr.bc_property(params[:id], params[:namespace])
	    if property.errors.empty?
	    	render :json => property.data, :status => 200
	  	else
	    	render :json => { :errors => property.errors.full_messages }, :status => 422
	  	end
	end

	def form_show
    render html: Mdr.form_to_html(params[:id], params[:namespace], false).html_safe
  end

	def form_annotations
    render html: Mdr.form_to_html_annotations(params[:id], params[:namespace], false).html_safe
  end

private

  def authorized?
    authorize Mdr
  end

  def the_params
    params.require(:mdr).permit(:release)
  end  	

end
