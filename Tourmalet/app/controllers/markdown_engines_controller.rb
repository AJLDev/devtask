class MarkdownEnginesController < ApplicationController

  before_action :authenticate_user!
  before_action :authorized?

  def create
    render :json => { :result => MarkdownEngine::render(the_params[:markdown])}, :status => 200
  end

private

  def authorized?
    authorize MarkdownEngine
  end

  def the_params
    params.require(:markdown_engine).permit(:markdown)
  end  

end