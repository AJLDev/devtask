class ElementsController < ApplicationController

  before_action :authenticate_user!
  before_action :authorized?

  C_CLASSNAME = "ElementsController"

  def create
    element = Element.create(the_params)
    if element.errors.empty?
   		render :json => { :data => {}, :mark => true  }, :status => 200
    else
  		render :json => { :errors => element.errors.full_messages}, :status => 422
    end
  end

  def destroy
    element = Element.find(params[:id])
    element.destroy
    render :json => { :data => {}, :mark => false }, :status => 200
  end

  def clear
    Element.where(the_params).destroy_all
    render :json => {}, :status => 200
  end

private

	def authorized?
    authorize Element
  end

  def the_params
  	params.require(:element).permit(:study_version_id, :form_id, :visit_id)
	end  	

end