class UsersController < ApplicationController

  before_action :authenticate_user!
  before_action :authorized?
  before_action :set_user, only: [:show, :edit, :update_name, :update_access_level]

  C_CLASS_NAME = "UsersController"
  
  def new
  end

  def set_read
    user = User.find(params[:id])
    current_roles = user.role_list
    if last_system_admin?(user)
      flash[:error] = "You cannot remove system admin access from your user account. You are the last system admin user!"
    else
      user.read!
      AuditTrail.update_event(current_user, "User #{user.email} roles updated from [#{current_roles}] to [#{user.role_list}].")
    end
    redirect_to users_path
  end

  def set_edit
    user = User.find(params[:id])
    current_roles = user.role_list
    if last_system_admin?(user)
      flash[:error] = "You cannot remove system admin access from your user account. You are the last system admin user!"
    else
      user.edit!
      AuditTrail.update_event(current_user, "User #{user.email} roles updated from [#{current_roles}] to [#{user.role_list}].")
    end
    redirect_to users_path
  end
  
  def set_system_admin
    user = User.find(params[:id])
    current_roles = user.role_list
    user.system_admin!
    AuditTrail.update_event(current_user, "User #{user.email} roles updated from [#{current_roles}] to [#{user.role_list}].")
    redirect_to users_path
  end

  def create
    new_user = User.create(user_params)
    if new_user.errors.blank?
      AuditTrail.create_event(current_user, "User #{new_user.email} created.")
      flash[:success] = "User was successfully created."
    else
      flash[:error] = "User was not created. #{new_user.errors.full_messages.to_sentence}."
    end
    redirect_to users_path
  end  

  def index
    @users = User.all.order("name ASC")
  end

  def show
  end

  def edit
  end

  def update_name
    if @user.update(user_params)
      flash[:success] = "User display name sucessfully updated."
    else
      flash[:error] = "Failed to update user display name."
    end
    redirect_to user_settings_path
  end

  def destroy
    # Note, no check on deleting the last admin user as cannot delete yourself and
    # you need to be admin to delete.
    user = User.find(params[:id])
    delete_email = user.email
    if current_user.id != user.id 
      user.destroy
      AuditTrail.delete_event(current_user, "User #{delete_email} deleted.")
      flash[:success] = "User #{delete_email} was successfully deleted."  
    else
      flash[:error] = "You cannot delete your own user!"
    end
    redirect_to users_path
  end

private

  def authorized?
    authorize User
  end

  def last_system_admin?(user)
    return user.id == current_user.id && current_user.has_system_admin_access? && system_admin_user_count == 1
  end

  def system_admin_user_count
    users = User.where(access_level: User.access_levels[:system_admin])
    return users.count
  end

  def set_user
    @user = User.find(params[:id])
  end

  def user_params
    params.require(:user).permit(:email, :password, :password_confirmation, :access_level, :name)
  end

end