class VisitsController < ApplicationController

	before_action :authenticate_user!
  before_action :authorized?

  C_CLASSNAME = "VisitsController"

  def create
		visit = Visit.create(the_params)
		if visit.errors.empty?
      Visit.ordinals(visit.study_version_id)
    	render :json => { :data => visit.to_json }, :status => 200
  	else
    	render :json => { :errors => form.errors.full_messages }, :status => 422
  	end
	end

  def update
    visit = Visit.update(params[:id], the_params)
    if visit.errors.empty?
      Visit.ordinals(visit.study_version_id)
      render :json => { :data => visit.to_json }, :status => 200
    else
      render :json => { :errors => form.errors.full_messages }, :status => 422
    end
  end

  def show
    visit = Visit.find(params[:id])
    render :json => { :data => visit }, :status => 200
  end

  def destroy
    visit = Visit.find(params[:id])
    visit.destroy
    Visit.ordinals(visit.study_version_id)
    render :json => {}, :status => 200
  end

  def move
    visit = Visit.find(params[:id])
    temp = visit.ordinal
    to_visit = Visit.find(the_params[:to_id])
    visit.update({:ordinal => to_visit.ordinal})
    to_visit.update({:ordinal => temp})
    render :json => {}, :status => 200
  end

private
  	
  def authorized?
    authorize Visit
  end

  def the_params
  	params.require(:visit).permit(:study_version_id, :ordinal, :long_name, :short_name, :timing, :to_id)
	end  	
  
end
