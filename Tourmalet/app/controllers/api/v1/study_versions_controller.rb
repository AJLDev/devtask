class Api::V1::StudyVersionsController < ActionController::Base
  
  http_basic_authenticate_with name: ENV["api_username"], password: ENV["api_password"]

	C_CLASS_NAME = "Api::V1::StudyVersionsController"

  def show
    study_version = StudyVersion.find_by_id(params[:id])
    if !study_version.nil?
    	render json: study_version.to_hash, status: 200
    else
    	render json: {errors: study_version_not_found_error}, status: 404
    end
  end

private 

  def study_version_not_found_error
		study_version = StudyVersion.new
    study_version.errors.add(:base, "Failed to find study version with identifier #{params[:id]}")
    return study_version.errors.full_messages
  end
    
end