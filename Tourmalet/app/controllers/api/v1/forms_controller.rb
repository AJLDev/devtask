class Api::V1::FormsController < ActionController::Base
  
  http_basic_authenticate_with name: ENV["api_username"], password: ENV["api_password"]

	C_CLASS_NAME = "Api::V1::FormsController"

  def show
    form = Form.find_by_id(params[:id])
    if !form.nil?
    	mdr_form = Mdr.form_to_hash(form.form_id, form.form_namespace)
    	render json: mdr_form, status: 200
    else
    	render json: {errors: form_not_found_error}, status: 404
    end
  end

private 

  def form_not_found_error
		form = Form.new
    form.errors.add(:base, "Failed to find form with identifier #{params[:id]}")
    return form.errors.full_messages
  end
    
end