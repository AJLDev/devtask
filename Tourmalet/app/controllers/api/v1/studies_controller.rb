class Api::V1::StudiesController < ActionController::Base
  
  http_basic_authenticate_with name: ENV["api_username"], password: ENV["api_password"]

	C_CLASS_NAME = "Api:V1:StudyController"

  def index
    results = []
    studies = Study.all
    studies.each {|x| results << x.to_hash}
    render json: results, status: 200
  end

  def history
    study = Study.find_by_id(params[:id])
    if !study.nil?
    	results = []
	    study_versions = StudyVersion.where(study_id: params[:id]).order('version asc')
	    study_versions.each {|x| results << x.to_hash}
    	render json: results, status: 200
    else
    	render json: {errors: study_not_found_error}, status: 404
    end
  end

private 

  def study_not_found_error
		study = Study.new
    study.errors.add(:base, "Failed to find study with identifier #{params[:id]}")
    return study.errors.full_messages
  end
    	
end