class StudyVersionsController < ApplicationController

  before_action :authenticate_user!
  before_action :authorized?
  before_action :set_study_version, only: [:show, :edit, :update, :destroy]

  def edit
    obtain_token
    if @token.nil?
      flash[:error] = "The study is locked for editing by another user."
      redirect_to request.referer
    else
      current_id = @study_version.id
      @study_version = @study_version.edit_version
      if current_id != @study_version.id
        AuditTrail.update_item_event(current_user, @study.identifier, @study_version.semantic_version, "Study version created")
      end
      AuditTrail.update_item_event(current_user, @study.identifier, @study_version.semantic_version, "Study version updated")
    end
  end

  def status
    obtain_token
    if @token.nil?
      flash[:error] = "The study is locked for editing by another user."
      redirect_to request.referer
    end
  end

  def update_status
    find_token
    if @token.nil?
      flash[:error] = "The changes were not saved as the edit lock has timed out."
    else
      @study_version.update_state(the_params)
      AuditTrail.update_item_event(current_user, @study.identifier, @study_version.semantic_version, "Study version updated")
      @token.release
    end
    redirect_to history_study_path(@study)
  end

  def soa
    @study_version = StudyVersion.find(params[:id])
    render text: @study_version.soa.html_safe
  end

  def clear
    find_token
    if @token.nil?
      render :json => {:errors => ["The changes were not saved as the edit lock has timed out."]}, :status => 422
    else
      result = Visit.where(study_version_id: params[:id]).destroy_all
      result = Form.where(study_version_id: params[:id]).destroy_all
      render :json => {}, :status => 200
    end
  end

  def update
    find_token
    if @token.nil?
      render :json => {:errors => ["The changes were not saved as the edit lock has timed out."]}, :status => 422
    else
      @study_version.update(the_params)
      if @study_version.errors.empty?
        render json: { data: @study_version.to_hash }, :status => 200
      else
        render :json => { :errors => @study_version.errors.full_messages}, :status => 422
      end
    end
  end

  def show
    @study_version = StudyVersion.find(params[:id])
    @study = Study.find(@study_version.study_id)
    respond_to do |format|
      format.html
      format.json do
        render json: { data: @study_version.to_hash }, :status => 200
      end
    end
  end

  def destroy
    obtain_token
    if @token.nil?
      flash[:error] = "The study is locked for editing by another user."
    else
      @study_version.destroy
      AuditTrail.delete_item_event(current_user, @study.identifier, @study_version.semantic_version, "Study version deleted")
      @token.release
    end
    redirect_to history_study_path(@study)
  end

  def export
    study_version = StudyVersion.find(params[:id])
    study = Study.find(study_version.study_id)
    lock = Token.obtain("#{study.identifier}_#{the_params[:export_type].upcase}", "", current_user)
    if !lock.nil?
      @lock_id = lock.id
      case the_params[:export_type]
        when "json"
          @path = export_json_study_version_path(study_version)
        when "odm"
          @path = export_odm_study_version_path(study_version)
        when "als"
          @path = export_als_study_version_path(study_version)
        when "crf"
          @path = export_crf_study_version_path(study_version, table_data: params[:table_data])
        when "acrf"
          @path = export_acrf_study_version_path(study_version, table_data: params[:table_data])
        else
          @path = export_json_study_version_path(study_version) # Default to JSON for the moment
      end
      respond_to do |format|
        format.html {render :layout => 'export'}
      end
    else
      @lock_id = 0
      respond_to do |format|
        flash[:error] = "Someone else is curently exporting this item. Try again in a minute."
        format.html {render :layout => 'export'}
      end
    end
  end

  def export_json
    study_version = StudyVersion.find(params[:id])
    study = Study.find(study_version.study_id)
    lock = Token.obtain("#{study.identifier}_JSON", "", current_user)
    study = Study.find(study_version.study_id)
    hash = study_version.to_full_hash
    send_data hash.to_json, filename: "#{study_version.name}.json", :type => 'application/json; header=present', disposition: "attachment"
    lock.release
  end

  def export_odm
    study_version = StudyVersion.find(params[:id])
    study = Study.find(study_version.study_id)
    lock = Token.obtain("#{study.identifier}_ODM", "", current_user)
    xml = study_version.to_xml
    send_data xml, filename: "#{study_version.name}.xml", :type => 'application/xhtml+xml; header=present', disposition: "attachment"
    lock.release
  end

  def export_als
    study_version = StudyVersion.find(params[:id])
    study = Study.find(study_version.study_id)
    lock = Token.obtain("#{study.identifier}_ALS", "", current_user)
    filename = "#{study_version.name}.xls"
    result = study_version.to_als(filename)
    lock.release
    if result[:error_count] == 0
      send_file result[:full_path], :type => "application/vnd.ms-excel", :filename => filename, :stream => false
    else
      flash[:error] = result[:error_message]
      respond_to do |format|
        format.html { render :layout => 'export' }
      end
    end
  end

  def export_crf
    html = ""
    html += "<br>"
    html += params[:table_data].gsub('<span class="glyphicon glyphicon-ok text-success"></span>', '&#10004;') if params[:table_data].present?
    html += "</td></tr></tbody></table>"
    # html += PdfReport.soa_table_export
    html += PdfReport.page_break
    study_version = StudyVersion.find(params[:id])
    study = Study.find(study_version.study_id)
    lock = Token.obtain("#{study.identifier}_CRF", "", current_user)
    forms = Form.where(study_version_id: study_version.id).order('ordinal asc')
    forms.each do |form|
      html += PdfReport.page_break
      html += Mdr.form_to_html(form.form_id, form.form_namespace, true).html_safe
    end
    # pdf = PdfReport.create("Case Report Form", "#{study.label}", current_user, html)
    header_html = view_context.render 'shared/report.html.erb'
    pdf = PdfReport.create("Case Report Form", "#{study.label}", "#{study_version.description}",  "#{study_version.protocol_name}", current_user, html,  header_html)
    send_data pdf, filename: "#{study.identifier}_CRF.pdf", type: 'application/pdf', disposition: 'inline' #'inline'
    lock.release
  end

  def export_acrf
    html = ""
    html += "<br>"
    html += params[:table_data].gsub('<span class="glyphicon glyphicon-ok text-success"></span>', '&#10004;') if params[:table_data].present?
    html += "</td></tr></tbody></table>"
    # html += PdfReport.soa_table_export
    html += PdfReport.page_break
    study_version = StudyVersion.find(params[:id])
    study = Study.find(study_version.study_id)
    lock = Token.obtain("#{study.identifier}_ACRF", "", current_user)
    forms = Form.where(study_version_id: study_version.id).order('ordinal asc')
    forms.each do |form|
      html += PdfReport.page_break
      html += Mdr.form_to_html_annotations(form.form_id, form.form_namespace, true).html_safe
    end
    #pdf = PdfReport.create("Annotated CRF", "#{study.label}", current_user, html)
    header_html = view_context.render 'shared/report.html.erb'
    pdf = PdfReport.create("Case Report Form", "#{study.label}", "#{study_version.description}",  "#{study_version.protocol_name}", current_user, html, header_html)
    send_data pdf, filename: "#{study.identifier}_aCRF.pdf", type: 'application/pdf', disposition: 'inline'
    lock.release
  end

  private

  def obtain_token
    @study_version = StudyVersion.find(params[:id])
    @study = Study.find(@study_version.study_id)
    @token = Token.obtain(@study.identifier, @study_version.semantic_version, current_user)
    return @token
  end

  def find_token
    @study_version = StudyVersion.find(params[:id])
    @study = Study.find(@study_version.study_id)
    @token = Token.find_token(@study.identifier, current_user)
    return @token
  end

  def authorized?
    authorize StudyVersion
  end

  def the_params
    params.require(:study_version).permit(:study_id, :name, :description, :protocol_name, :state, :export_type)
  end

  # Use callbacks to share common setup or constraints between actions.
  def set_study_version
    @study_version = StudyVersion.find(params[:id])
  end

end
