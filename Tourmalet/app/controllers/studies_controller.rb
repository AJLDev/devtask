class StudiesController < ApplicationController

  before_action :authenticate_user!
  before_action :authorized?

  TBS = "To be set"

  def index
  	@studies = Study.all
	end

	def history
    @study = Study.find(params[:id])
    token = Token.find_token(@study.identifier, current_user)
    token.release if !token.nil?
    @study_versions = StudyVersion.where(study_id: params[:id]).order('version asc')
    if @study_versions.count == 0 
      study_version = StudyVersion.create(study_id: params[:id], state: StudyVersion.states[:incomplete], 
        semantic_version: "0.1.0", version: 1, name: @study.label, description: TBS, protocol_name: TBS)
      AuditTrail.update_item_event(current_user, @study.identifier, study_version.semantic_version, "Study version created.")
      @study_versions = StudyVersion.where(study_id: params[:id])
    end
  end

  def create
		study = Study.create(the_params)
    if study.errors.empty?
      AuditTrail.update_item_event(current_user, study.identifier, "", "Study created.")
      redirect_to studies_path
    else
      flash[:error] = study.errors.full_messages.to_sentence
      redirect_to new_study_path
    end
	end

  def destroy
    study = Study.find(params[:id])
    study.destroy
    AuditTrail.update_item_event(current_user, study.identifier, "", "Study deleted.")
    redirect_to studies_path
  end

private

	def authorized?
    authorize Study
  end

  def the_params
  	params.require(:study).permit(:identifier, :label)
	end  	

end
