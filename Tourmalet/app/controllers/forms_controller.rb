class FormsController < ApplicationController

  before_action :authenticate_user!
  before_action :authorized?

  C_CLASSNAME = "FormsController"

	def create
		form = Form.create(the_params)
    if form.errors.empty?
      Form.ordinals(form.study_version_id)
      render :json => { :data => form.to_json }, :status => 200
  	else
  		render :json => { :errors => form.errors.full_messages }, :status => 422
  	end
	end

  def destroy
    form = Form.find(params[:id])
    form.destroy
    Form.ordinals(form.study_version_id)
    render :json => {}, :status => 200
  end

  def move
    form = Form.find(params[:id])
    temp = form.ordinal
    to_form = Form.find(the_params[:to_id])
    form.update({:ordinal => to_form.ordinal})
    to_form.update({:ordinal => temp})
    render :json => {}, :status => 200
  end

private

  def authorized?
    authorize Form
  end

  def the_params
    params.require(:form).permit(:study_version_id, :ordinal, :form_id, :form_namespace, :identifier, :label, :to_id)
  end 
    	
end
