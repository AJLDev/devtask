class Mdrs::FormsController < ApplicationController

	before_action :authenticate_user!
  before_action :authorized?

  C_CLASS_NAME = "Mdrs::FormsController"
	
	def history
		forms = Mdr.forms_all
	  if forms.errors.empty?
	  	results = forms.data.select {|x| x[:scoped_identifier][:identifier] == params[:id] } # A little naughty, should include owner but for the moment will do
  		render :json => results, :status => 200
  	else
  		render :json => { :errors => forms.errors.full_messages }, :status => 422
  	end
	end

	def latest
		forms = Mdr.forms_all
	  if forms.errors.empty?
	  	results = forms.data.select {|x| x[:scoped_identifier][:identifier] == params[:id] } # A little naughty, should include owner but for the moment will do
	  	set = results.sort_by {|x| x[:scoped_identifier][:version]}.reverse!
  		item = set.find {|x| x[:registration_state][:registration_status] == "Standard"}
  		if !item.nil?
  			render :json => item, :status => 200
  		else
  			forms.errors.add(:base, "No released items were found.")
  			render :json => {errors: forms.errors.full_messages}, status: 404
  		end
  	else
  		render :json => { :errors => forms.errors.full_messages }, :status => 422
  	end
	end

private

  def authorized?
    authorize Mdr::Forms
  end

end
