class AmendmentsController < ApplicationController

  before_action :authenticate_user!
  before_action :authorized?

  C_CLASSNAME = "AmendmentsController"

	def find
    amendments = Amendment.where(the_params)
    results = {}
    results[:data] = []
    amendments.each do |amendment|
      results[:data] << amendment.to_json
    end
    render :json => results, :status => 200
  end

  def create
		amendment = Amendment.create(the_params)
    if amendment.errors.empty?
    	render :json => { :id => amendment.id }, :status => 200
  	else
    	render :json => { :errors => amendment.errors.full_messages }, :status => 422
  	end
	end

  def update
    amendment = Amendment.find(params[:id])
    result = amendment.update(the_params)
    if result
      render :json => {}, :status => 200
    else
      render :json => {}, :status => 422
    end
  end

  def destroy
    amendment = Amendment.find(params[:id])
    amendment.destroy
    render :json => {}, :status => 200
  end

private
  	
  def authorized?
    authorize Amendment
  end

  def the_params
  	params.require(:amendment).permit(:study_version_id, :cid, :namespace, :property, :study_value, :default_value, :datatype)
	end  	

end
