class StudyPolicy < ApplicationPolicy

  def history?
    @user.has_read_access?
  end

end