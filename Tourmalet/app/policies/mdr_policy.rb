class MdrPolicy < ApplicationPolicy
  
  # Note in all the policies we use 'user' and not '@user' as in all of 
  # the remaining policies. Seems to be due to headless nature of this 
  # controller (no model)
  
  #def forms_all?
  #  user.has_read_access?
  #end

  #def forms_list?
  #  user.has_read_access?
  #end

  def forms?
    @user.has_read_access?
  end

  def form?
    @user.has_read_access?
  end

  def form_show?
    @user.has_read_access?
  end

  def form_annotations?
    @user.has_read_access?
  end

  def thesaurus_concept?
    @user.has_read_access?
  end

  def bc_property?
    @user.has_read_access?
  end

end