class AmendmentPolicy < ApplicationPolicy

	def find?
		@user.has_read_access?
	end

end