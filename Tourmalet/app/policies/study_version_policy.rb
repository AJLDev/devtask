class StudyVersionPolicy < ApplicationPolicy

	def status?
    @user.has_edit_access?
  end

	def update_status?
    @user.has_edit_access?
  end

	def soa?
    @user.has_edit_access?
  end

  def clear?
    @user.has_edit_access?
  end

  def export?
    @user.has_edit_access?
  end

  def export_als?
    @user.has_edit_access?
  end

  def export_odm?
    @user.has_edit_access?
  end

  def export_json?
    @user.has_edit_access?
  end

  def export_crf?
    @user.has_edit_access?
  end

  def export_acrf?
    @user.has_edit_access?
  end

end