class TokenPolicy < ApplicationPolicy

  def index?
    @user.has_system_admin_access?
  end

  def release?
    @user.has_read_access?
  end

  def status?
    @user.has_read_access?
  end

  def extend_token?
    @user.has_read_access?
  end

end