class UserPolicy < ApplicationPolicy

  def set_read?
    @user.has_system_admin_access?
  end

  def set_edit?
    @user.has_system_admin_access?
  end

  def set_system_admin?
    @user.has_system_admin_access?
  end

  def index?
    @user.has_system_admin_access?
  end

  def show?
    @user.has_system_admin_access?
  end

  def new?
    @user.has_system_admin_access?
  end

  def update_name?
    @user.has_read_access?
  end

  def create?
    @user.has_system_admin_access?
  end

  def edit?
    @user.has_system_admin_access?
  end

  def destroy?
    @user.has_system_admin_access?
  end

end