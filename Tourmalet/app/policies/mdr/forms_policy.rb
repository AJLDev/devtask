class Mdr::FormsPolicy < ApplicationPolicy
  
  def history?
    @user.has_read_access?
  end

  def latest?
    @user.has_read_access?
  end

end