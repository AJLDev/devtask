class ElementPolicy < ApplicationPolicy

	def clear?
		@user.has_edit_access?
	end
	
end