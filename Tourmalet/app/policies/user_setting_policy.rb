class UserSettingPolicy < ApplicationPolicy

	def update?
		@user.has_read_access?
	end

end