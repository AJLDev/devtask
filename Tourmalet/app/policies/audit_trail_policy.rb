class AuditTrailPolicy < ApplicationPolicy

	def index?
    @user.has_system_admin_access?
  end

	def search?
    @user.has_system_admin_access?
  end

	def export_csv?
    @user.has_system_admin_access?
  end

end