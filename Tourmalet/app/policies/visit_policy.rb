class VisitPolicy < ApplicationPolicy

	def move?
		@user.has_edit_access?
	end

end