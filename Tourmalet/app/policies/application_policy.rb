class ApplicationPolicy
  
  C_CLASS_NAME = "ApplicationPolicy"
  
  attr_reader :user, :record

  def initialize(user, record)
    @user = user
    @record = record
  end

  def index?
    @user.has_read_access? 
  end

  def show?
    @user.has_read_access? 
  end

  def create?
    @user.has_edit_access?
  end

  def new?
    @user.has_edit_access?
  end

  def update?
    @user.has_edit_access?
  end

  def edit?
    @user.has_edit_access?
  end

  def destroy?
    @user.has_edit_access?
  end

  def scope
    Pundit.policy_scope!(user, record.class)
  end

  class Scope
    attr_reader :user, :scope

    def initialize(user, scope)
      @user = user
      @scope = scope
    end

    def resolve
      scope
    end
  end

end
