class RemoveColumnsFromStudies < ActiveRecord::Migration
  def change
    remove_column :studies, :name, :string
    remove_column :studies, :description, :string
    remove_column :studies, :protocol_name, :string
  end
end
