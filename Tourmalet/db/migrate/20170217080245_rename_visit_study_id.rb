class RenameVisitStudyId < ActiveRecord::Migration
  def change
  	rename_column :visits, :study_id, :study_version_id
  end
end
