class RenameAmendmentsStudyId < ActiveRecord::Migration
  def change
  	rename_column :amendments, :study_id, :study_version_id
  end
end
