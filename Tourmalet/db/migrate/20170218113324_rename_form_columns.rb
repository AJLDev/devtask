class RenameFormColumns < ActiveRecord::Migration
  def change
  	rename_column :forms, :cid, :form_id
  	rename_column :forms, :namespace, :form_namespace
  end
end
