class CreateVisits < ActiveRecord::Migration
  def change
    create_table :visits do |t|
    	t.belongs_to :study, index: true
    	t.string :short_name
    	t.string :long_name
    	t.string :timing
      t.integer :ordinal

      t.timestamps null: false
    end
  end
end
