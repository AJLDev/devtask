class AddLabelToForms < ActiveRecord::Migration
  def change
    add_column :forms, :label, :string
  end
end
