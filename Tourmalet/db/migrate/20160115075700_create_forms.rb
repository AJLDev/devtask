class CreateForms < ActiveRecord::Migration
	def change
    	create_table :forms do |t|
      		t.belongs_to :study, index: true
    		t.string :cid
      		t.string :namespace
			t.string :identifier
			t.integer :ordinal

      		t.timestamps null: false
    	end
  	end
end
