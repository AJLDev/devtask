class CreateTokens < ActiveRecord::Migration
  def change
    create_table :tokens do |t|
      t.timestamp :locked_at
      t.integer :refresh_count
      t.integer :study_version_id
      t.string :item
      t.string :item_info
      t.integer :user_id

      t.timestamps null: false
    end
  end
end
