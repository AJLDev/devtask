class AddIndexToStudies < ActiveRecord::Migration
  def change
    add_index :studies, :identifier, unique: true
  end
end
