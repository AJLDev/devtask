class RenameFormStudyId < ActiveRecord::Migration
  def change
  	rename_column :forms, :study_id, :study_version_id
  end
end
