class RenameElementStudyId < ActiveRecord::Migration
  def change
  	rename_column :elements, :study_id, :study_version_id
  end
end
