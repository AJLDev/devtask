class CreateElements < ActiveRecord::Migration
  def change
    create_table :elements do |t|
		t.belongs_to :study
		t.belongs_to :form
		t.belongs_to :visit
      
      	t.timestamps null: false
    end
  end
end
