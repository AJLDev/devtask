class CreateAmendments < ActiveRecord::Migration
  def change
    create_table :amendments do |t|
      t.belongs_to :study, index: true
      t.string :cid
      t.string :namespace
      t.string :property
      t.string :study_value
      t.string :default_value
      t.string :datatype

      t.timestamps null: false
    end
  end
end
