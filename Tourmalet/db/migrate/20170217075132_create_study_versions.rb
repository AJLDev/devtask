class CreateStudyVersions < ActiveRecord::Migration
  def change
    create_table :study_versions do |t|
      t.integer :study_id
      t.string :name
      t.string :description
      t.string :protocol_name
      t.integer :state
      t.integer :version
      t.string :semantic_version

      t.timestamps null: false
    end
  end
end
