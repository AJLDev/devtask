# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20170226113452) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "amendments", force: :cascade do |t|
    t.integer  "study_version_id"
    t.string   "cid"
    t.string   "namespace"
    t.string   "property"
    t.string   "study_value"
    t.string   "default_value"
    t.string   "datatype"
    t.datetime "created_at",       null: false
    t.datetime "updated_at",       null: false
  end

  add_index "amendments", ["study_version_id"], name: "index_amendments_on_study_version_id", using: :btree

  create_table "audit_trails", force: :cascade do |t|
    t.datetime "date_time"
    t.string   "user"
    t.string   "identifier"
    t.string   "version"
    t.integer  "event"
    t.string   "description"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  create_table "elements", force: :cascade do |t|
    t.integer  "study_version_id"
    t.integer  "form_id"
    t.integer  "visit_id"
    t.datetime "created_at",       null: false
    t.datetime "updated_at",       null: false
  end

  create_table "forms", force: :cascade do |t|
    t.integer  "study_version_id"
    t.string   "form_id"
    t.string   "form_namespace"
    t.string   "identifier"
    t.integer  "ordinal"
    t.datetime "created_at",       null: false
    t.datetime "updated_at",       null: false
    t.string   "label"
  end

  add_index "forms", ["study_version_id"], name: "index_forms_on_study_version_id", using: :btree

  create_table "old_passwords", force: :cascade do |t|
    t.string   "encrypted_password",       null: false
    t.string   "password_salt"
    t.string   "password_archivable_type", null: false
    t.integer  "password_archivable_id",   null: false
    t.datetime "created_at"
  end

  add_index "old_passwords", ["password_archivable_type", "password_archivable_id"], name: "index_password_archivable", using: :btree

  create_table "studies", force: :cascade do |t|
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string   "label"
    t.string   "identifier"
  end

  add_index "studies", ["identifier"], name: "index_studies_on_identifier", unique: true, using: :btree

  create_table "study_versions", force: :cascade do |t|
    t.integer  "study_id"
    t.string   "name"
    t.string   "description"
    t.string   "protocol_name"
    t.integer  "state"
    t.integer  "version"
    t.string   "semantic_version"
    t.datetime "created_at",       null: false
    t.datetime "updated_at",       null: false
  end

  create_table "tokens", force: :cascade do |t|
    t.datetime "locked_at"
    t.integer  "refresh_count"
    t.integer  "study_version_id"
    t.string   "item"
    t.string   "item_info"
    t.integer  "user_id"
    t.datetime "created_at",       null: false
    t.datetime "updated_at",       null: false
  end

  create_table "user_settings", force: :cascade do |t|
    t.string   "name"
    t.string   "value"
    t.integer  "user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "user_settings", ["user_id"], name: "index_user_settings_on_user_id", using: :btree

  create_table "users", force: :cascade do |t|
    t.string   "email",                  default: "", null: false
    t.string   "encrypted_password",     default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.inet     "current_sign_in_ip"
    t.inet     "last_sign_in_ip"
    t.datetime "created_at",                          null: false
    t.datetime "updated_at",                          null: false
    t.integer  "access_level",           default: 0
    t.string   "name"
    t.datetime "password_changed_at"
  end

  add_index "users", ["email"], name: "index_users_on_email", unique: true, using: :btree
  add_index "users", ["password_changed_at"], name: "index_users_on_password_changed_at", using: :btree
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree

  create_table "visits", force: :cascade do |t|
    t.integer  "study_version_id"
    t.string   "short_name"
    t.string   "long_name"
    t.string   "timing"
    t.integer  "ordinal"
    t.datetime "created_at",       null: false
    t.datetime "updated_at",       null: false
  end

  add_index "visits", ["study_version_id"], name: "index_visits_on_study_version_id", using: :btree

  add_foreign_key "user_settings", "users"
end
