require 'rails_helper'

describe Study do
  
  it "returns a hash" do
    study = Study.create(identifier: "XXX", label: "label for")
    expected = {identifier: "XXX", label: "label for"}
    expected[:id] = study.id # Id cannot be predicted
    expect(study.to_hash).to eq(expected)
  end

end
