require 'rails_helper'

describe Amendment do
  
  before :each do
  	Amendment.destroy_all
  end

  after :each do
  	Amendment.destroy_all
  end
  
  it "serializes the amendmemt as a hash" do
  	amendment = Amendment.create(cid: "XXX", namespace:  "http://ww.example.com", property: "question", 
      study_value: "ADEF", default_value: "A", datatype: "string")
		result = amendment.to_hash
		expected = 
		{ 
			cid: "XXX", 
			namespace: "http://ww.example.com", 
			property: "question", 
			study_value: "ADEF",
      default_value: "A",
      datatype: "string"
		}
    expect(result).to eq(expected)
  end

end
