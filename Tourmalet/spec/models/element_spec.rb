require 'rails_helper'

describe Element do
  
  it "allows an element to be found" do
  	element1 = Element.create(study_version_id: 1, visit_id: 10, form_id: 11)
    element2 = Element.create(study_version_id: 1, visit_id: 11, form_id: 11)
    element3 = Element.create(study_version_id: 2, visit_id: 1, form_id: 11)
    element4 = Element.create(study_version_id: 2, visit_id: 1, form_id: 12)
    element5 = Element.create(study_version_id: 3, visit_id: 3, form_id: 7)
    result = Element.find_element(1, 10, 11)
    expect(result.id).to eq(element1.id)
  end

	it "returns nil for an element that does not exist" do
  	element1 = Element.create(study_version_id: 1, visit_id: 10, form_id: 11)
    element2 = Element.create(study_version_id: 1, visit_id: 11, form_id: 11)
    element3 = Element.create(study_version_id: 2, visit_id: 1, form_id: 11)
    element4 = Element.create(study_version_id: 2, visit_id: 1, form_id: 12)
    element5 = Element.create(study_version_id: 3, visit_id: 3, form_id: 7)
    result = Element.find_element(1, 10, 13)
    expect(result).to eq(nil)
  end

end
