require 'rails_helper'

RSpec.describe Token, type: :model do

  before :all do
    Token.destroy_all
    @user = User.create :email => "token_user@assero.co.uk", :password => "chAngeme2$" 
    @user2 = User.create :email => "token_user2@assero.co.uk", :password => "chanGeme67*" 
  end

  after :all do
    user = User.where(:email => "token_user@assero.co.uk").first
    user.destroy
    user = User.where(:email => "token_user2@assero.co.uk").first
    user.destroy
  end

	it "allows a token to be obtained" do
  	item = "IDENTIFIER"
  	token = Token.obtain(item, "info", @user)
    expect(token.item).to eq("IDENTIFIER")
    expect(token.item_info).to eq("info")
    expect(token.user_id).to eq(@user.id)
    expect(token.locked_at).to be_within(1.second).of Time.now
  end

  it "allows the same user to obtain a token when already allocated" do
  	Token.set_timeout(5)
  	item = "IDENTIFIER"
    token1 = Token.obtain(item, "info 1", @user)
    sleep 3 # Valid sleep in this case, to let token 1 elapse a bit so as to test reset
    token2 = Token.obtain(item, "info 2", @user)
    expect(token1.item).to eq(token2.item) # Same item locked
    expect(token2.locked_at).to be_within(1.second).of Time.now # time reset to maximise lock time
  end

  it "prevents another user obtaining a token when already allocated" do
  	item = "IDENTIFIER"
    token1 = Token.obtain(item, "info", @user)
    token2 = Token.obtain(item, "info", @user2)
    expect(token2).to eq(nil)
  end

  it "allows a token to be released" do
  	item = "IDENTIFIER"
    token = Token.obtain(item, "info", @user)
    token.release
    expect{Token.find(token.id)}.to raise_error(ActiveRecord::RecordNotFound)
  end

  it "allows a token to be refreshed" do
  	item = "IDENTIFIER"
    token = Token.obtain(item, "info", @user)
    sleep 3
    expect(token.refresh).to eq(1)
    expect(token.locked_at).to be_within(1.second).of Time.now
    sleep 5
    expect(token.refresh).to eq(2)
    expect(token.locked_at).to be_within(1.second).of Time.now
  end

  it "finds token" do
  	item = "IDENTIFIER"
    token = Token.obtain(item, "info", @user)
    expect(Token.find_token(item, @user).to_json).to eq(token.to_json)
  end

  it "determines if user does not own lock, released" do
    Token.set_timeout(5)
  	item = "IDENTIFIER"
    token = Token.obtain(item, "info", @user)
    sleep 6
    expect(Token.find_token(item, @user)).to eq(nil)
  end

  it "determines if user does not own lock, never locked" do
  	item = "IDENTIFIER"
    expect(Token.find_token(item, @user)).to eq(nil)
  end

  it "allows tokens to be expired" do
    Token.set_timeout(5)
  	item1 = "IDENTIFIER1"
    item2 = "IDENTIFIER2"
    item3 = "IDENTIFIER3"
    item4 = "IDENTIFIER4"
    token1 = Token.obtain(item1, "info 1", @user)
    token2 = Token.obtain(item2, "info 2", @user)
    sleep 3
    token3 = Token.obtain(item3, "info 3", @user)
    token4 = Token.obtain(item4, "info 4", @user)
    sleep 3
    expect(Token.find_token(item1, @user)).to eq(nil)
    expect(Token.find_token(item2, @user)).to eq(nil)
    expect(Token.find_token(item3, @user).to_json).to eq(token3.to_json)
    expect(Token.find_token(item4, @user).to_json).to eq(token4.to_json)
  end

  it "allows the timeout to be modified" do
    Token.set_timeout(10)
    expect(Token.get_timeout).to eq(10)
    item1 = "ITEM1"
    token1 = Token.obtain(item1, "info", @user)
    sleep 3
    expect(Token.find_token(item1, @user).to_json).to eq(token1.to_json)
    sleep 3
    expect(Token.find_token(item1, @user).to_json).to eq(token1.to_json)
    sleep 6
    expect(Token.find_token(item1, @user)).to eq(nil)
    Token.set_timeout(5)
  end

  it "tests for an expired timeout" do
    Token.set_timeout(5)
    item = "ITEM"
    token = Token.obtain(item, "info", @user)
    expect(token.timed_out?).to eq(false)
    sleep 6
    expect(token.timed_out?).to eq(true)
  end

  it "tests for the remaining time" do
    Token.set_timeout(5)
    item = "ITEM"
    token = Token.obtain(item, "info", @user)
    expect(token.remaining).to eq(5)
    sleep 1
    expect(token.remaining).to eq(4)
    sleep 1
    expect(token.remaining).to eq(3)
  end

  it "allows the timeout to be extended" do
    Token.set_timeout(5)
    item = "ITEM"
    token = Token.obtain(item, "info", @user)
    sleep 3
    token.extend_token
    sleep 4
    expect(token.timed_out?).to eq(false)
  end

end
