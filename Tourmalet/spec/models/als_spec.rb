require 'rails_helper'
require 'excel_wrapper/ew_workbook'

describe Als do
  
  include FileHelpers
  include StudyVersionHelpers

  def sub_dir
    return "models"
  end

  def move_als_definition
    test_delete_file(sub_dir, "als.yml")
    test_copy_file(sub_dir, "als_good.yml", "als.yml")
    als_def = read_yaml_file(sub_dir, "als.yml")
    return als_def
  end

  it "allows the object to be initialized" do
  	als_def = move_als_definition
  	partial_path = test_file_partial_path(sub_dir, "als.yml")
    result = Als.new()
    path = Rails.root.join partial_path
    expected = { filename: path.to_s, workbook: als_def}
    expect(result.to_definition_hash).to eq(expected)
  end

	it "handles missing file" do
  	test_delete_file(sub_dir, "als.yml")
  	result = Als.new
  	expect(result.errors.full_messages.to_sentence).to eq("Workbook was not created. The definition file did not exist")
  end

	it "handles syntax error in file" do
  	test_delete_file(sub_dir, "als.yml")
  	test_copy_file(sub_dir, "als_syntax_error.yml", "als.yml")
  	result = Als.new
  	expect(result.errors.full_messages.to_sentence).to eq("Workbook was not created. The definition file contained syntax errors")
  end

  it "creates an ALS file, see scenario tests"

  it "builds a project sheet" do
    als_def = move_als_definition
    als = Als.new()
    map = {}
    study_hash = read_yaml_file(sub_dir, "study_version_2.yaml")
    als.parse_tc_node(study_hash, map)
    als.process_tc_map(map)
    als.project_sheet(study_hash)
    sheet = als.get_sheet("CRFDraft")
  #write_text_file(sheet.to_csv, sub_dir, "als_project_sheet.txt")
    expected = read_text_file(sub_dir, "als_project_sheet.txt")
    expect(sheet.to_csv).to eq(expected)
  end

  it "builds a forms sheet" do
    als_def = move_als_definition
    als = Als.new()
    map = {}
    study_hash = read_yaml_file(sub_dir, "study_version_2.yaml")
    als.parse_tc_node(study_hash, map)
    als.process_tc_map(map)
    als.forms_sheet(study_hash)
    sheet = als.get_sheet("Forms")
  #write_text_file(sheet.to_csv, sub_dir, "als_forms_sheet.txt")
    expected = read_text_file(sub_dir, "als_forms_sheet.txt")
    expect(sheet.to_csv).to eq(expected)
  end

  it "builds a fields sheet" do
    als_def = move_als_definition
    als = Als.new()
    map = {}
    study_hash = read_yaml_file(sub_dir, "study_version_2.yaml")
    als.parse_tc_node(study_hash, map)
    als.process_tc_map(map)
    als.fields_sheet(study_hash, map)
    sheet = als.get_sheet("Fields")
  #write_text_file(sheet.to_csv, sub_dir, "als_fields_sheet.txt")
    expected = read_text_file(sub_dir, "als_fields_sheet.txt")
    expect(sheet.to_csv).to eq(expected)
  end

  it "builds a folders sheet"

  it "builds a data dictionaries sheet" do
    als_def = move_als_definition
    als = Als.new()
    map = {}
    study_hash = read_yaml_file(sub_dir, "study_version_2.yaml")
    als.parse_tc_node(study_hash, map)
    als.process_tc_map(map)
    als.code_lists_sheet(map)
    sheet = als.get_sheet("DataDictionaries")
  #write_text_file(sheet.to_csv, sub_dir, "als_data_dictionaries_sheet.txt")
    expected = read_text_file(sub_dir, "als_data_dictionaries_sheet.txt")
    expect(sheet.to_csv).to eq(expected)
  end

  it "builds a data dictionary entries sheet" do
    als_def = move_als_definition
    als = Als.new()
    map = {}
    study_hash = read_yaml_file(sub_dir, "study_version_2.yaml")
    als.parse_tc_node(study_hash, map)
    als.process_tc_map(map)
    als.code_list_items_sheet(map)
    sheet = als.get_sheet("DataDictionaryEntries")
  #write_text_file(sheet.to_csv, sub_dir, "als_data_dictionary_entry_sheet.txt")
    expected = read_text_file(sub_dir, "als_data_dictionary_entry_sheet.txt")
    expect(sheet.to_csv).to eq(expected)
  end

  it "builds a matrices sheet" do
    als_def = move_als_definition
    als = Als.new()
    als.matrices_sheet
    sheet = als.get_sheet("Matrices")
  #write_text_file(sheet.to_csv, sub_dir, "als_matrices_sheet.txt")
    expected = read_text_file(sub_dir, "als_matrices_sheet.txt")
    expect(sheet.to_csv).to eq(expected)
  end

  it "processes the form questions" do
    als_def = move_als_definition
    als = Als.new()
    map = {}
    study_hash = read_yaml_file(sub_dir, "study_version_2.yaml")
    als.parse_tc_node(study_hash, map)
    als.process_tc_map(map)
    sheet = ExcelWrapper::EwWorkbook::EwWorksheet.new(als_def[:sheets][1])
    ordinal = als.question_node(study_hash[:children][0], "F-ACME_AENEW", sheet, 1, map)
  #write_text_file(sheet.to_csv, sub_dir, "als_question.txt")
    expected = read_text_file(sub_dir, "als_question.txt")
    expect(sheet.to_csv).to eq(expected)
  end

  it "builds a terminology map, stage I" do
    test_delete_file(sub_dir, "als.yml")
    test_copy_file(sub_dir, "als_good.yml", "als.yml")
    als = Als.new()
    study_hash = read_yaml_file(sub_dir, "study_version_2.yaml")
    map = {}
    als.parse_tc_node(study_hash, map)
  #write_yaml_file(map, sub_dir, "als_map_stage_I.yaml")
    expected = read_yaml_file(sub_dir, "als_map_stage_I.yaml")
    expect(map).to eq(expected)
  end

  it "builds a terminology map, stage II" do
    test_delete_file(sub_dir, "als.yml")
    test_copy_file(sub_dir, "als_good.yml", "als.yml")
    als = Als.new()
    study_hash = read_yaml_file(sub_dir, "study_version_2.yaml")
    map = {}
    als.parse_tc_node(study_hash, map)
    als.process_tc_map(map)
  #write_yaml_file(map, sub_dir, "als_map_stage_II.yaml")
    expected = read_yaml_file(sub_dir, "als_map_stage_II.yaml")
    expect(map).to eq(expected)
  end

  it "extracts the Code List" do
    als = Als.new()
    result = als.extract_cl("CLI-C66742_C49488")
    expect(result).to eq("C66742")
    result = als.extract_cl("TH-ACME_QSTERM_EQ5D3L_MOBILITY_NONE")
    expect(result).to eq("ACME_QSTERM_EQ5D3L_MOBILITY")
  end


  it "sets the datatype and format" do
    als = Als.new()
    node = {}
    node[:datatype] = "string"
    node[:cl_map] = {}
    result = als.set_datatype_and_format(node)
    expect(result).to eq([{:label=>"DataFormat", :value=>"$"}, {:label=>"ControlType", :value=>"Text"}])
    node[:datatype] = "string"
    node[:cl_map] = { content: {}}
    result = als.set_datatype_and_format(node)
    expect(result).to eq([{:label=>"DataFormat", :value=>"$"}, {:label=>"ControlType", :value=>"DropDownList"}])
    node[:datatype] = "integer"
    node[:format] = 3
    result = als.set_datatype_and_format(node)
    expect(result).to eq([{:label=>"DataFormat", :value=>"$3"}, {:label=>"ControlType", :value=>"Text"}])
    node[:datatype] = "boolean"
    result = als.set_datatype_and_format(node)
    expect(result).to eq([{:label=>"DataFormat", :value=>"$1"}, {:label=>"ControlType", :value=>"Text"}])
    node[:datatype] = "dateTime"
    result = als.set_datatype_and_format(node)
    expect(result).to eq([{:label=>"DataFormat", :value=>"dd MMM yyyy"}, {:label=>"ControlType", :value=>"DateTime"}])
    node[:datatype] = "date"
    result = als.set_datatype_and_format(node)
    expect(result).to eq([{:label=>"DataFormat", :value=>"dd MMM yyyy"}, {:label=>"ControlType", :value=>"DateTime"}])
    node[:datatype] = "time"
    result = als.set_datatype_and_format(node)
    expect(result).to eq([{:label=>"DataFormat", :value=>"HH:nn"}, {:label=>"ControlType", :value=>"DateTime"}])
    node[:datatype] = "float"
    node[:format] = 7.2
    result = als.set_datatype_and_format(node)
    expect(result).to eq([{:label=>"DataFormat", :value=>"7.2"}, {:label=>"ControlType", :value=>"Text"}])
  end

  it "determines the CL OID, single" do
    map = {}
    node = {}
    als = Als.new()
    node[:cl_map] = { key: "XXX", index: 1 }
    map["XXX"] = 
    [
      {
        :index => 1,
        :items =>
        [
          {
            :id=>"CLI-C71148_C62122",
            :notation=>"SITTING",
            :preferred_term=>"Sitting"
          },
          {
            :id=>"CLI-C71148_C62166",
            :notation=>"STANDING",
            :preferred_term=>"Standing"
          },
          {
            :id=>"CLI-C71148_C62167",
            :notation=>"SUPINE",
            :preferred_term=>"Supine Position"
          }
        ],
        :status=> :as_is_single
      }
    ]
    oid = als.set_cl_oid(node, map)
    expect(oid).to eq("XXX")
  end

  it "determines the CL OID, multiple" do
    map = {}
    node = {}
    als = Als.new()
    node[:cl_map] = { key: "C66770", index: 2 }
    map["C66770"] = 
    [
      {
        :index=> 1,
        :items=> 
        [
          {
            :id=>"CLI-C66770_C42559",
            :notation=>"C",
            :preferred_term=>"Degree Celsius"
          }
        ],
        :status => :as_is_multiple
      },
      {
        :index =>2,
        :items =>
        [
          {
            :id =>"CLI-C66770_C48531",
            :notation =>"LB",
            :preferred_term =>"Pound"
          },
          {
            :id=>"CLI-C66770_C28252",
            :notation=>"kg",
            :preferred_term=>"Kilogram"
          },
          {
            :id=>"CLI-C66770_C48155",
            :notation=>"g",
            :preferred_term=>"Gram"
          },
          {
            :id=>"CLI-C71620_C48519",
            :notation=>"oz",
            :preferred_term=>"Ounce"
          }
        ],
        :status => :as_is_multiple
      },
      {
        :index=>3,
        :items=>
        [
          {
            :id=>"CLI-C66770_C49673",
            :notation=>"beats/min",
            :preferred_term=>"Beats per Minute"
          }
        ],
        :status => :as_is_multiple
      }
    ]
    oid = als.set_cl_oid(node, map)
    expect(oid).to eq("C66770_2")
  end

  it "determines the CL OID, multiple as single" do
    map = {}
    node = {}
    als = Als.new()
    node[:cl_map] = { key: "XXX", index: 1 }
    map["XXX"] =
    [
      {
        :index=>1,
        :items=>[
          {
            :id=>"CLI-C66742_C49488",
            :notation=>"Y",
            :preferred_term=>"Yes"
          },
          {
            :id=>"CLI-C66742_C49487",
            :notation=>"N",
            :preferred_term=>"No"
          }
        ],
        :status=> :use_first
      },
      {
        :index=>2,
        :items=>[
          {
            :id=>"CLI-C66742_C49488",
            :notation=>"Y",
            :preferred_term=>"Yes"
          },
          {
            :id=>"CLI-C66742_C49487",
            :notation=>"N",
            :preferred_term=>"No"
          }
        ],
        :status=> :use_first
      },
      {
        :index=>3,
        :items=>[
          {
            :id=>"CLI-C66742_C49488",
            :notation=>"Y",
            :preferred_term=>"Yes"
          },
          {
            :id=>"CLI-C66742_C49487",
            :notation=>"N",
            :preferred_term=>"No"
          }
        ],
        :status=> :use_first
      }
    ]
    oid = als.set_cl_oid(node, map)
    expect(oid).to eq("XXX")
  end

end