require 'rails_helper'

describe IsoRegistrationState do
  
  before :all do

  end

  it "provides the release state" do
    result = StudyVersion.the_released_state
    expect(result).to eq(:standard)
  end

  it "provides the initial state" do
    result = StudyVersion.the_initial_state
    expect(result).to eq(:incomplete)
  end

  it "provides the state label" do
    result = StudyVersion.state_label(:qualified)
    expect(result).to eq("Qualified")
  end

  it "provides the state definition" do
    result = StudyVersion.state_definition(:recorded)
    expect(result).to eq("The Registration Authority has confirmed that: a) all mandatory metadata attributes have been completed.")
  end

  it "detects not registered state" do
    result = StudyVersion.new
    result.state = StudyVersion.states[:not_set]
    expect(result.registered?).to eq(false)
  end

  it "detects registered state" do
    result = StudyVersion.new
    result.state = :qualified
    expect(result.registered?).to eq(true)
  end

  it "allows the item to be checked for the released state, I" do
    result = StudyVersion.new
    result.state = :standard
    expect(result.released_state?).to eq(true)
  end
  
  it "allows the item to be checked for the released state, II" do
    result = StudyVersion.new
    expect(result.released_state?).to eq(false)
  end
  
  it "allows the next state to be retrieved" do
    result = StudyVersion.new
    result.state = :qualified
    expect(result.next_state).to eq(:standard)
  end
  
  it "allows the next state to be retrieved, II" do
    result = StudyVersion.new
    result.state = :incomplete
    expect(result.next_state).to eq(:qualified)
  end
    
  

  it "allows the state label to be retrieved" do
    result = StudyVersion.new
    result.state = :incomplete
    expect(result.state_label).to eq("Incomplete")
  end
  
  it "allows the state definition to be retrieved" do
    expected = "The Registration Authority has confirmed that: a) the mandatory metadata attributes are complete and b) " +
      "the mandatory metadata attributes conform to applicable quality requirements."
    result = StudyVersion.new
    result.state = :qualified
    expect(result.state_definition).to eq(expected)
  end
  
  it "allows the edit state to be determined" do
    result = StudyVersion.new
    result.state = :incomplete
    expect(result.edit?).to eq(true)
    result.state = :superseded
    expect(result.edit?).to eq(false)
  end
  
  it "allows the delete state to be determined" do
    result = StudyVersion.new
    result.state = :incomplete
    expect(result.delete?).to eq(true)
    result.state = :standard
    expect(result.delete?).to eq(false)
  end
  
  it "allows the state after edit to be determined" do
    result = StudyVersion.new
    result.state = :incomplete
    expect(result.state_on_edit).to eq(:incomplete)
    result.state = :standard
    expect(result.state_on_edit).to eq(:incomplete)
    result.state = :qualified
    expect(result.state_on_edit).to eq(:qualified)
  end
  
  it "allows whether a new version is required after an edit to be determined" do
    result = StudyVersion.new
    result.state = :incomplete
    expect(result.new_version?).to eq(false)
    result.state = :standard
    expect(result.new_version?).to eq(true)
  end
  
  it "determines if the item's state can be changed" do
    result = StudyVersion.new
    result.state = :incomplete
    expect(result.can_be_changed?).to eq(true)
    result.state = :standard
    expect(result.can_be_changed?).to eq(true)
    result.state = :superseded
    expect(result.can_be_changed?).to eq(false)
  end

end
  