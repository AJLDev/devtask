require 'rails_helper'

describe Visit do
  
  before :each do
  	Visit.destroy_all
  end

  after :each do
  	Visit.destroy_all
  end
  
  it "orders the visits based on the ordinal" do
  	visit1 = Visit.create(study_version_id: 1, ordinal: 3)
    visit2 = Visit.create(study_version_id: 1, ordinal: 5)
    visit3 = Visit.create(study_version_id: 1, ordinal: 7)
    visit4 = Visit.create(study_version_id: 2, ordinal: 4)
    Visit.ordinals(1)
    visits = Visit.where(study_version_id: 1).order('ordinal asc')
    expect(visits[0].id).to eq(visit1.id)
    expect(visits[0].ordinal).to eq(1)
    expect(visits[1].id).to eq(visit2.id)
    expect(visits[1].ordinal).to eq(2)
    expect(visits[2].id).to eq(visit3.id)
    expect(visits[2].ordinal).to eq(3)
  end

end
