require 'rails_helper'

describe Form do
  
  before :each do
  	Form.destroy_all
  end

  after :each do
  	Form.destroy_all
  end
  
  it "orders the foms based on the ordinal" do
  	form1 = Form.create(study_version_id: 1, ordinal: 3)
    form2 = Form.create(study_version_id: 1, ordinal: 5)
    form3 = Form.create(study_version_id: 1, ordinal: 7)
    form4 = Form.create(study_version_id: 2, ordinal: 4)
    Form.ordinals(1)
    forms = Form.where(study_version_id: 1).order('ordinal asc')
    expect(forms[0].id).to eq(form1.id)
    expect(forms[0].ordinal).to eq(1)
    expect(forms[1].id).to eq(form2.id)
    expect(forms[1].ordinal).to eq(2)
    expect(forms[2].id).to eq(form3.id)
    expect(forms[2].ordinal).to eq(3)
  end

  it "serializes the form as a hash" do
  	form1 = Form.create(form_id: "XXX", form_namespace:  "http://ww.example.com", identifier: "ADEF", ordinal: 1, label: "This is a label")
		result = form1.to_hash
		expected = 
		{ 
			id: form1.id, 
			ordinal: 1, 
			form_id: "XXX", 
			form_namespace: "http://ww.example.com", 
			identifier: "ADEF", 
			label: "This is a label"
		}
    expect(result).to eq(expected)
  end

end
