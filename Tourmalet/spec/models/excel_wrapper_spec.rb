require 'rails_helper'

describe ExcelWrapper do
  
  include FileHelpers
  
  def sub_dir
    return "models"
  end

  it "allows the object to be initialized" do
  	wb_hash = read_yaml_file(sub_dir, "excel_1.yaml")
  	partial_path = test_file_partial_path(sub_dir, "excel_1.yaml")
    result = ExcelWrapper.new({file_location: partial_path})
    path = Rails.root.join partial_path
    expected = { filename: path.to_s, workbook: wb_hash}
    expect(result.to_definition_hash).to eq(expected)
  end

  it "handles missing file" do
    partial_path = test_file_partial_path(sub_dir, "excel_1X.yaml")
    expect{object = ExcelWrapper.new({file_location: partial_path})}.to raise_error(Errno::ENOENT)
  end

  it "handles sybtax error in file" do
    partial_path = test_file_partial_path(sub_dir, "excel_syntax_error.yaml")
    expect{ExcelWrapper.new({file_location: partial_path})}.to raise_error(Psych::SyntaxError)
  end

end