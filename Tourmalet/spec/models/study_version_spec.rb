require 'rails_helper'

describe Study do

  include FileHelpers

  def sub_dir
    return "models"
  end

  def create_form(study_version, form_id, form_namespace, identifier, ordinal, label)
    form = Form.create(study_version_id: study_version.id, ordinal: ordinal, form_id: form_id,
                       form_namespace: form_namespace, identifier: identifier, label: label)
    return form
  end

  def new_form(study_version, form_id, form_namespace, identifier, ordinal, label)
    form = Form.new(study_version_id: study_version.id, ordinal: ordinal, form_id: form_id,
                    form_namespace: form_namespace, identifier: identifier, label: label)
    return form
  end

  def create_visit(study_version, short_name, long_name, timing, ordinal)
    visit = Visit.create(study_version_id: study_version.id, ordinal: ordinal, short_name: short_name, long_name: long_name, timing: timing)
    return visit
  end

  def new_visit(study_version, short_name, long_name, timing, ordinal)
    visit = Visit.new(study_version_id: study_version.id, ordinal: ordinal, short_name: short_name, long_name: long_name, timing: timing)
    return visit
  end

  def create_element(study_version, form, visit)
    element = Element.create(study_version_id: study_version.id, form_id: form.id, visit_id: visit.id)
    return element
  end

  def new_element(study_version, form, visit)
    element = Element.new(study_version_id: study_version.id, form_id: form.id, visit_id: visit.id)
    return element
  end

  before :each do
    Form.destroy_all
    Visit.destroy_all
    Element.destroy_all
    StudyVersion.destroy_all
  end

  after :each do
    Form.destroy_all
    Visit.destroy_all
    Element.destroy_all
    StudyVersion.destroy_all
  end

  it "edits a version, no clone" do
    study_version = StudyVersion.create(study_id: 1, name: "Name", description: "description", protocol_name: "protocol_name",
                                        state: StudyVersion.states[:incomplete], version: 1, semantic_version: "0.1.0")
    new_version = study_version.edit_version
    expect(new_version.id).to eq(study_version.id)
  end

  it "edits a version, clone" do
    study_version = StudyVersion.create(study_id: 1, name: "Name", description: "description", protocol_name: "protocol_name",
                                        state: StudyVersion.states[:qualified], version: 1, semantic_version: "0.1.0")
    new_version = study_version.edit_version
    expect(new_version.id).to_not eq(study_version.id)
  end

  it "updates the state" do
    study_version = StudyVersion.create(study_id: 1, name: "Name", description: "description", protocol_name: "protocol_name",
                                        state: StudyVersion.states[:qualified], version: 1, semantic_version: "0.1.0")
    study_version.update_state({ state: :standard })
    expect(study_version.state).to eq("standard")
    expect(study_version.semantic_version).to eq("1.0.0")
  end

  it "allows a study version to be cloned, incomplete state" do
    study_version = StudyVersion.new
    form1 = create_form(study_version, "XXX1", "http://www.example.com/form", "F1", 1, "Label 1")
    form2 = create_form(study_version, "XXX2", "http://www.example.com/form", "F2", 2, "Label 2")
    form3 = create_form(study_version, "XXX2", "http://www.example.com/form", "F3", 3, "Label 3")
    visit1 = create_visit(study_version, "V1", "Visit 1", "Day 0", 1)
    visit2 = create_visit(study_version, "V2", "Visit 2", "Day 7", 2)
    element1 = create_element(study_version, form1, visit1)
    element2 = create_element(study_version, form2, visit1)
    study_version.state = StudyVersion.states[:incomplete]
    study_version.semantic_version = "0.1.0"
    study_version.version = 1
    new_version = study_version.clone
    expect(new_version.version).to eq(2)
    expect(new_version.semantic_version).to eq("0.2.0")
    forms = Form.where(study_version_id: new_version.id)
    visits = Visit.where(study_version_id: new_version.id)
    elements = Element.where(study_version_id: new_version.id)
    expect(forms.count).to eq(3)
    expect(visits.count).to eq(2)
    expect(elements.count).to eq(2)
  end

  it "allows a study version to be cloned, qualified state" do
    study_version = StudyVersion.new
    form1 = create_form(study_version, "XXX1", "http://www.example.com/form", "F1", 1, "Label 1")
    form2 = create_form(study_version, "XXX2", "http://www.example.com/form", "F2", 2, "Label 2")
    form3 = create_form(study_version, "XXX2", "http://www.example.com/form", "F3", 3, "Label 3")
    visit1 = create_visit(study_version, "V1", "Visit 1", "Day 0", 1)
    visit2 = create_visit(study_version, "V2", "Visit 2", "Day 7", 2)
    element1 = create_element(study_version, form1, visit1)
    element2 = create_element(study_version, form2, visit1)
    study_version.state = StudyVersion.states[:qualified]
    study_version.semantic_version = "0.1.0"
    study_version.version = 1
    new_version = study_version.clone
    expect(new_version.version).to eq(2)
    expect(new_version.semantic_version).to eq("0.2.0")
    forms = Form.where(study_version_id: new_version.id)
    visits = Visit.where(study_version_id: new_version.id)
    elements = Element.where(study_version_id: new_version.id)
    expect(forms.count).to eq(3)
    expect(visits.count).to eq(2)
    expect(elements.count).to eq(2)
  end

  it "allows a study version to be cloned, standard state" do
    study_version = StudyVersion.new
    form1 = create_form(study_version, "XXX1", "http://www.example.com/form", "F1", 1, "Label 1")
    form2 = create_form(study_version, "XXX2", "http://www.example.com/form", "F2", 2, "Label 2")
    form3 = create_form(study_version, "XXX2", "http://www.example.com/form", "F3", 3, "Label 3")
    visit1 = create_visit(study_version, "V1", "Visit 1", "Day 0", 1)
    visit2 = create_visit(study_version, "V2", "Visit 2", "Day 7", 2)
    element1 = create_element(study_version, form1, visit1)
    element2 = create_element(study_version, form2, visit1)
    study_version.state = StudyVersion.states[:standard]
    study_version.semantic_version = "1.0.0"
    study_version.version = 1
    new_version = study_version.clone
    expect(new_version.version).to eq(2)
    expect(new_version.semantic_version).to eq("1.1.0")
    forms = Form.where(study_version_id: new_version.id)
    visits = Visit.where(study_version_id: new_version.id)
    elements = Element.where(study_version_id: new_version.id)
    expect(forms.count).to eq(3)
    expect(visits.count).to eq(2)
    expect(elements.count).to eq(2)
  end

  it "allows the soa to be generated" do
    study_version = StudyVersion.new
    form1 = create_form(study_version, "XXX1", "http://www.example.com/form", "F1", 1, "Label 1")
    form2 = create_form(study_version, "XXX2", "http://www.example.com/form", "F2", 2, "Label 2")
    form3 = create_form(study_version, "XXX2", "http://www.example.com/form", "F3", 3, "Label 3")
    visit1 = create_visit(study_version, "V1", "Visit 1", "Day 0", 1)
    visit2 = create_visit(study_version, "V2", "Visit 2", "Day 7", 2)
    element1 = create_element(study_version, form1, visit1)
    element2 = create_element(study_version, form2, visit1)
    element3 = create_element(study_version, form3, visit2)
    result = study_version.soa
    #write_text_file(result, sub_dir, "study_version_soa_1.htm")
    expected = read_text_file(sub_dir, "study_version_soa_1.htm")
    expected = expected.gsub("___FORM1___", "#{form1.id}")
    expected = expected.gsub("___FORM2___", "#{form2.id}")
    expected = expected.gsub("___FORM3___", "#{form3.id}")
    expected = expected.gsub("___VISIT1___", "#{visit1.id}")
    expected = expected.gsub("___VISIT2___", "#{visit2.id}")
    expected = expected.gsub("___ELEMENT1___", "#{element1.id}")
    expected = expected.gsub("___ELEMENT2___", "#{element2.id}")
    expected = expected.gsub("___ELEMENT3___", "#{element3.id}")
    expected = Nokogiri::HTML(expected)
    result = Nokogiri::HTML(result)
    expected_table = expected.xpath('//table')
    result_table = result.xpath('//table')
    expect(result_table.search('tr').count).to eq(expected_table.search('tr').count)
    expect(result_table.search('td').count).to eq(expected_table.search('td').count)
    # expect(result).to eq(expected)
  end

  it "allows a hash to be generated" do
    study = Study.create(identifier: "XXX", label: "label for")
    study_version = StudyVersion.new
    study_version.semantic_version = "0.1.0"
    study_version.version = 1
    study_version.state = "standard"
    study_version.description = "Description"
    study_version.name = "Name"
    study_version.protocol_name = "Protcol Name"
    study_version.study_id = study.id
    study_version.save
    form1 = create_form(study_version, "XXX1", "http://www.example.com/form", "F1", 1, "Label 1")
    form2 = create_form(study_version, "XXX2", "http://www.example.com/form", "F2", 2, "Label 2")
    form3 = create_form(study_version, "XXX2", "http://www.example.com/form", "F3", 3, "Label 3")
    result = study_version.to_hash
    #write_yaml_file(result, sub_dir, "study_version_hash_1.yaml")
    expected = read_yaml_file(sub_dir, "study_version_hash_1.yaml")
    expected[:id] = study_version.id # Fix the study id as can be predicted
    expected[:children][0][:id] = form1.id # Fix the form ids as they are dynamic
    expected[:children][1][:id] = form2.id
    expected[:children][2][:id] = form3.id
    expect(result).to eq(expected)
  end

  it "allows a full hash to be generated" do
    study = Study.create(identifier: "XXX", label: "label for")
    study_version = StudyVersion.new
    study_version.semantic_version = "0.1.0"
    study_version.version = 1
    study_version.state = "standard"
    study_version.description = "Description"
    study_version.name = "Name"
    study_version.protocol_name = "Protcol Name"
    study_version.study_id = study.id
    study_version.save
    form1 = create_form(study_version, "XXX1", "http://www.example.com/form", "F1", 1, "Label 1")
    form2 = create_form(study_version, "XXX2", "http://www.example.com/form", "F2", 2, "Label 2")
    form_json_1 = read_yaml_file(sub_dir, "form_json_1.yaml")
    form_json_2 = read_yaml_file(sub_dir, "form_json_2.yaml")
    expect(Mdr).to receive(:form_to_hash).with("XXX1", "http://www.example.com/form").and_return(form_json_1)
    expect(Mdr).to receive(:form_to_hash).with("XXX2", "http://www.example.com/form").and_return(form_json_2)
    result = study_version.to_full_hash
    #write_yaml_file(result, sub_dir, "study_version_hash_2.yaml")
    expected = read_yaml_file(sub_dir, "study_version_hash_2.yaml")
    expected[:id] = study_version.id # Fix the study id as can be predicted
    expect(result).to eq(expected)
  end

  it "next clone version" do
    study_version = StudyVersion.new
    study_version.semantic_version = "0.1.0"
    study_version.version = 1
    study_version.next_clone_version
    expect(study_version.version).to eq(2)
    expect(study_version.semantic_version).to eq("0.2.0")
  end

  it "next state version, qualified" do
    study_version = StudyVersion.new
    study_version.semantic_version = "0.10.0"
    study_version.version = 10
    study_version.state = StudyVersion.states[:qualified]
    study_version.next_state_version
    expect(study_version.version).to eq(10)
    expect(study_version.semantic_version).to eq("0.11.0")
  end

  it "next state version, standard" do
    study_version = StudyVersion.new
    study_version.semantic_version = "0.10.0"
    study_version.version = 10
    study_version.state = StudyVersion.states[:standard]
    study_version.next_state_version
    expect(study_version.version).to eq(10)
    expect(study_version.semantic_version).to eq("1.0.0")
  end

end
