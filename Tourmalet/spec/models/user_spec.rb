require 'rails_helper'

describe User do

	before :each do
    AuditTrail.delete_all
  end

  it "allows a user's roles to be listed" do
    user = User.new
    user.access_level = User.access_levels[:read]
    expect(user.role_list).to eq("Read")
    user.access_level = User.access_levels[:edit]
    expect(user.role_list).to eq("Read, Edit")
    user.access_level = User.access_levels[:system_admin]
    expect(user.role_list).to eq("Read, Edit, System Admin")
  end

  it "sets the reader role when a user is created" do
    user = User.new
    user.set_extra
    expect(user.role_list).to eq("Read")
  end

  it "logs an audit event when a user password is changed" do
    user = User.new
    expect(user).to receive(:encrypted_password_changed?) {true}
    user.user_update
    expect(AuditTrail.count).to eq(1)
  end

  it "does not log an audit event when a user password is not changed" do
    user = User.new
    expect(user).to receive(:encrypted_password_changed?) {false}
    user.user_update
    expect(AuditTrail.count).to eq(0)
  end

  it "determines if a user is a reader" do
    user = User.new
    expect(user.has_read_access?).to eq(true) 
  end
  
  it "determines if a user is an editor" do
    user = User.new
    expect(user.has_edit_access?).to eq(false)
    user.access_level = User.access_levels[:edit]
    expect(user.has_edit_access?).to eq(true)
  end

  it "determines if a user is a system admin" do
    user = User.new
    expect(user.has_system_admin_access?).to eq(false)
    user.access_level = User.access_levels[:system_admin]
    expect(user.has_system_admin_access?).to eq(true)
  end

end
