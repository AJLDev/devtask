require 'rails_helper'

describe ExcelWrapper::EwWorkbook do
  
  include FileHelpers
  
  def sub_dir
    return "models"
  end

  it "allows the object to be initialized, none" do
    result = ExcelWrapper::EwWorkbook.new({})
    expect(result.label).to eq("")
    expect(result.sheets).to eq([])
  end

  it "allows the object to be initialized, label" do
    result = ExcelWrapper::EwWorkbook.new( label: "Heading")
    expect(result.label).to eq("Heading")
    expect(result.sheets).to eq([])
  end

  it "allows the object to be initialized, sheets" do
    result = ExcelWrapper::EwWorkbook.new(sheets: [{label: "Sheet 1", columns: [{label: "Boolean", default: "true", ordinal: 3}]}])
    expect(result.label).to eq("")
    expect(result.to_definition_hash).to eq({label: "", sheets: [{label: "Sheet 1", columns: [{label: "Boolean", default: "true", ordinal: 3}]}]})
  end

  it "allows the object to be initialized, name and sheets" do
  	hash = 
  	{
  		label: "Heading", 
  		sheets: 
  		[
  			{
  				label: "Sheet 1", 
  				columns: 
  				[
  					{ label: "Boolean", default: "true", ordinal: 3 }
  				]
  			}
  		]
  	}
    result = ExcelWrapper::EwWorkbook.new(hash)
    expect(result.label).to eq("Heading")
    expect(result.to_definition_hash).to eq(hash)
  end

  it "allows the object to be serialized as a hash" do
  	hash = 
  	{
  		label: "Books",
  		sheets: 
  		[
  			{ 
  				label: "Test", 
  				columns: 
  				[
  					{label: "Col 1", default: "true", ordinal: 1}, 
  					{label: "Col 2", default: "true", ordinal: 2}
  				]
  			},
  			{ 
  				label: "Sheet 2", 
  				columns: 
  				[
  					{label: "Col 1", default: "false", ordinal: 1}, 
  					{label: "Col 2", default: "false", ordinal: 2},
  					{label: "Col 3", default: "", ordinal: 3} 
  				]
  			}
  		]
  	}
    wb = ExcelWrapper::EwWorkbook.new(hash)
    expected = hash
    expect(wb.to_definition_hash).to eq(expected)
  end

  it "allows a sheet to be found" do
    hash = 
    {
      label: "Books",
      sheets: 
      [
        { 
          label: "Test", 
          columns: 
          [
            {label: "Col 1", default: "true", ordinal: 1}, 
            {label: "Col 2", default: "true", ordinal: 2}
          ]
        },
        { 
          label: "Sheet 2", 
          columns: 
          [
            {label: "Col 1", default: "true", ordinal: 1}, 
            {label: "Col 2", default: "true", ordinal: 2}
          ]
        },
        { 
          label: "Sheet 2", 
          columns: 
          [
            {label: "Col 1", default: "false", ordinal: 1}, 
            {label: "Col 2", default: "false", ordinal: 2},
            {label: "Col 3", default: "", ordinal: 3} 
          ]
        }
      ]
    }
    wb = ExcelWrapper::EwWorkbook.new(hash)
    sheet = wb.find_worksheet("Test")
    expect(sheet.label).to eq("Test")
    sheet = wb.find_worksheet("Test1")
    expect(sheet).to eq(nil)
    expect {wb.find_worksheet("Sheet 2")}.to raise_error(ExcelWrapper::EwWorkbook::DuplicateSheets)
  end

  it "allows a sheet to be added"

end