require 'rails_helper'

describe ExcelWrapper::EwWorkbook::EwWorksheet::EwColumn do
  
  it "allows the object to be initialized, none" do
    result = ExcelWrapper::EwWorkbook::EwWorksheet::EwColumn.new({})
    expect(result.label).to eq("")
    expect(result.default).to eq("")
    expect(result.ordinal).to eq(1)
  end

  it "allows the object to be initialized, major" do
    result = ExcelWrapper::EwWorkbook::EwWorksheet::EwColumn.new( label: "Heading")
    expect(result.label).to eq("Heading")
    expect(result.default).to eq("")
    expect(result.ordinal).to eq(1)
  end

  it "allows the object to be initialized, minor" do
    result = ExcelWrapper::EwWorkbook::EwWorksheet::EwColumn.new( default: "Whatevs")
    expect(result.label).to eq("")
    expect(result.default).to eq("Whatevs")
    expect(result.ordinal).to eq(1)
  end

  it "allows the object to be initialized, patch" do
    result = ExcelWrapper::EwWorkbook::EwWorksheet::EwColumn.new( ordinal: 3)
    expect(result.label).to eq("")
    expect(result.default).to eq("")
    expect(result.ordinal).to eq(3)
  end

  it "allows the column to be serialized as a hash" do
    column = ExcelWrapper::EwWorkbook::EwWorksheet::EwColumn.new( label: "Test", default: "value", ordinal: 3)
    expected = { label: "Test", default: "value", ordinal: 3 }
    expect(column.to_definition_hash).to eq(expected)
  end
    
end