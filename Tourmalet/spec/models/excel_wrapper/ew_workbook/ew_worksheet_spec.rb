require 'rails_helper'

describe ExcelWrapper::EwWorkbook::EwWorksheet do
  
  include FileHelpers

  def sub_dir
    return "models/workbook/worksheet"
  end

  it "allows the object to be initialized, none" do
    result = ExcelWrapper::EwWorkbook::EwWorksheet.new({})
    expect(result.label).to eq("")
    expect(result.columns).to eq([])
    expect(result.rows).to eq([])
  end

  it "allows the object to be initialized, name" do
    result = ExcelWrapper::EwWorkbook::EwWorksheet.new( label: "Heading")
    expect(result.label).to eq("Heading")
    expect(result.columns).to eq([])
    expect(result.rows).to eq([])
  end

  it "allows the object to be initialized, columns" do
    result = ExcelWrapper::EwWorkbook::EwWorksheet.new( columns: [{label: "Boolean", default: "true", ordinal: 3}])
    expect(result.label).to eq("")
    expect(result.columns[0].to_definition_hash).to eq({label: "Boolean", default: "true", ordinal: 3})
    expect(result.rows).to eq([])
  end

  it "allows the object to be initialized, name and columns" do
    result = ExcelWrapper::EwWorkbook::EwWorksheet.new( label: "Heading", columns: [{label: "Boolean", default: "true", ordinal: 3}])
    expect(result.label).to eq("Heading")
    expect(result.columns[0].to_definition_hash).to eq({label: "Boolean", default: "true", ordinal: 3})
    expect(result.rows).to eq([])
  end

  it "allows the object to be serialized as a hash" do
    ws = ExcelWrapper::EwWorkbook::EwWorksheet.new( label: "Test", columns: [{label: "Col 1", default: "true", ordinal: 1}, {label: "Col 2", default: "true", ordinal: 2}])
    expected = {:label=>"Test", :columns=>[{:label=>"Col 1", :default=>"true", :ordinal=>1}, {:label=>"Col 2", :default=>"true", :ordinal=>2}]}
    expect(ws.to_definition_hash).to eq(expected)
  end

  it "allows the first row to be added, auto adds the header row" do
    definition = 
    { 
      label: "Test", 
      columns: 
      [
        {label: "Col 1", default: "AAA", ordinal: 1}, 
        {label: "Col 2", default: "BBB", ordinal: 2},
        {label: "Col 3", default: "CCC", ordinal: 3},
        {label: "Col 4", default: "DDD", ordinal: 4}
      ]
    }
    ws = ExcelWrapper::EwWorkbook::EwWorksheet.new(definition)
    ws.add_row([{label: "Col 1", value: "XXXX"}])
  #write_text_file(ws.to_csv, sub_dir, "add_row_1.txt")
    expected = read_text_file(sub_dir, "add_row_1.txt")
    expect(ws.to_csv).to eq(expected)
  end

  it "allows multiple rows to be added" do
    definition = 
    { 
      label: "Test", 
      columns: 
      [
        {label: "Col 1", default: "AAA", ordinal: 1}, 
        {label: "Col 2", default: "BBB", ordinal: 2},
        {label: "Col 3", default: "CCC", ordinal: 3},
        {label: "Col 4", default: "DDD", ordinal: 4}
      ]
    }
    ws = ExcelWrapper::EwWorkbook::EwWorksheet.new(definition)
    ws.add_row([{label: "Col 1", value: "XXXX"}])
    ws.add_row([{label: "Col 1", value: "XXXX"}, {label: "Col 2", value: "ZZZZZZZZ"}])
    ws.add_row([{label: "Col 1", value: "XXXX"}, {label: "Col 2", value: "ZZZZZZZZ"}, {label: "Col 3", value: "ZZZZZZZZ"}, {label: "Col 4", value: "ZZZZZZZZ"}])
    ws.add_row([{label: "Col 1", value: "XXXX"}, {label: "Col 2", value: "ZZZZZZZZ"}, {label: "Col 3", value: "ZZZZZZZZ"}, {label: "Col 4", value: "ZZZZZZZZ"}])
    ws.add_row([{label: "Col 1", value: "XXXX"}, {label: "Col 2", value: "ZZZZZZZZ"}, {label: "Col 3", value: "ZZZZZZZZ"}, {label: "Col 4", value: "ZZZZZZZZ"}])
    ws.add_row([{label: "Col 1", value: "XXXX"}, {label: "Col 2", value: "ZZZZZZZZ"}, {label: "Col 3", value: "ZZZZZZZZ"}, {label: "Col 4", value: "ZZZZZZZZ"}])
    ws.add_row([{label: "Col 1", value: "XXXX"}, {label: "Col 2", value: "ZZZZZZZZ"}, {label: "Col 3", value: "ZZZZZZZZ"}, {label: "Col 4", value: "ZZZZZZZZ"}])
    ws.add_row([{label: "Col 1", value: "XXX1"}, {label: "Col 2", value: "222222"}, {label: "Col 3", value: "333"}, {label: "Col 4", value: "444"}])
    ws.add_row([{label: "Col 4", value: "XXX4"}])
    ws.add_row([{label: "Col 3", value: "XXX3"}])
    ws.add_row([{label: "Col 2", value: "XXX2"}])
  #write_text_file(ws.to_csv, sub_dir, "add_row_2.txt")
    expected = read_text_file(sub_dir, "add_row_2.txt")
    expect(ws.to_csv).to eq(expected)
  end

end