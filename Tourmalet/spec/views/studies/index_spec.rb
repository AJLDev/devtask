require 'rails_helper'

describe 'studies/index.html.erb', :type => :view do

  include UserAccountHelpers
  include ViewHelpers

  it 'allows studies to be viewed' do
    
    def view.policy(name)
      # Do nothing
    end

    studies = []
    studies << FactoryGirl.build(:study, identifier: "XXXX", label: "Study 1", id: 1)
    studies << FactoryGirl.build(:study, identifier: "YYYYY", label: "Study 2", id: 1)
    assign(:studies, studies)

    render

    #page_to_s

    expect(rendered).to have_content("Studies")
    
    expect_table_cell_text("main", 1, 1, "XXXX")
    expect_table_cell_text("main", 1, 2, "Study 1")
    expect_table_cell_link("main", 1, 3, "History")
    expect_table_cell_link("main", 1, 4, "Delete")
    
    expect_table_cell_text("main", 2, 1, "YYYYY")
    expect_table_cell_text("main", 2, 2, "Study 2")
    expect_table_cell_link("main", 2, 3, "History")
    expect_table_cell_link("main", 2, 4, "Delete")

    expect_link("new_study_button")

  end

end