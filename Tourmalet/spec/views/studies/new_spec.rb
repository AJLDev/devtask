require 'rails_helper'

describe 'studies/new.html.erb', :type => :view do

  include UserAccountHelpers
  include ViewHelpers

  it 'creates new user' do
    
    def view.policy(name)
      # Do nothing
    end

    render

    #page_to_s

    expect(rendered).to have_content("New Study")
    expect(rendered).to have_content("Identifier:")
    expect(rendered).to have_content("Useful Label:")

    expect_link("close_button")
    expect_button("create_button")

  end

end