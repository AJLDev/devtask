require 'rails_helper'

describe 'audit_trail/index.html.erb', :type => :view do

  include UserAccountHelpers
  include ViewHelpers

  it 'allows audit trail to be viewed' do
    
    def view.policy(name)
      # Do nothing
    end

    defaults = {:user => "", :identifier => "", :owner => "", :event => AuditTrail.event_types[:empty_action]}
    assign(:defaults, defaults)

    events = AuditTrail.event_types
    assign(:events, events)

    users = []
    users << FactoryGirl.build(:user, :email => "fred@fred.com", :name => "User One")
    users << FactoryGirl.build(:user, :email => "sid@fred.com", :name => "User Two")
    assign(:users, users)

    items = []
    items << FactoryGirl.build(:audit_trail, date_time: Time.now(), user: "fred@fred.com", identifier: "XXXX", 
      version: "0.1.0", event: 1, description: "Well!")
    items << FactoryGirl.build(:audit_trail, date_time: Time.now() - 20, user: "sid@fred.com", identifier: "", 
      version: "", event: 1, description: "Well what?")
    assign(:items, items)

    render

    #page_to_s

    expect(rendered).to have_content("Audit Trail")
    expect(rendered).to have_content("Search")
    
    expect_table_cell_text("main", 1, 2, "fred@fred.com")
    expect_table_cell_text("main", 1, 3, "XXXX")
    expect_table_cell_text("main", 1, 4, "0.1.0")
    expect_table_cell_text("main", 1, 5, "")
    expect_table_cell_text("main", 1, 6, "Well!")

    expect_table_cell_text("main", 2, 2, "sid@fred.com")
    expect_table_cell_text("main", 2, 3, "")
    expect_table_cell_text("main", 2, 4, "")
    expect_table_cell_text("main", 2, 5, "")
    expect_table_cell_text("main", 2, 6, "Well what?")

    expect_link("all_button")
    expect_link("export_csv_button")

  end

end