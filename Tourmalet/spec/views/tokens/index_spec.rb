require 'rails_helper'

describe 'tokens/index.html.erb', :type => :view do

  include UserAccountHelpers
  include ViewHelpers

  it 'allows tokens to be viewed' do
    
    def view.policy(name)
      # Do nothing
    end

    user1 = User.create :email => "token_user@assero.co.uk", :password => "cHangeMe14%", :name => "USER1"
    token1 = Token.obtain("ITEM 1", "info 1", user1)
    token2 = Token.obtain("ITEM 2", "info 2", user1)

    timeout = Token.set_timeout(300)
    timeout = Token.get_timeout
    tokens = Token.all

    assign(:tokens, tokens)
    assign(:timeout, timeout)

    render

    #page_to_s

    expect(rendered).to have_content("Edit Locks")
    
    expect_table_cell_text("main", 1, 2, "05:00")
    expect_table_cell_text("main", 1, 3, "ITEM 1")
    expect_table_cell_text("main", 1, 4, "info 1")
    expect_table_cell_text("main", 1, 6, "0")

    expect_table_cell_text("main", 2, 2, "05:00")
    expect_table_cell_text("main", 2, 3, "ITEM 2")
    expect_table_cell_text("main", 2, 4, "info 2")
    expect_table_cell_text("main", 2, 6, "0")

  end

end