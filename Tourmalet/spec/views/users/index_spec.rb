require 'rails_helper'

describe 'users/index.html.erb', :type => :view do

  include UserAccountHelpers
  include ViewHelpers

  it 'displays users' do
    
    def view.policy(name)
      # Do nothing
    end

    UserSettings.reset_settings_metadata
    users = []
    users << FactoryGirl.build(:user, :email => "user1@example.com", :name => "User One", :id => 1)
    users << FactoryGirl.build(:user, :email => "user2@example.com", :name => "User Two", :id => 2)
    users << FactoryGirl.build(:user, :email => "user3@example.com", :name => "User Three", :id => 3)
    users << FactoryGirl.build(:user, :email => "user4@example.com", :name => "User Four", :id => 4)
    users << FactoryGirl.build(:user, :email => "user5@example.com", :name => "User Five", :id => 5)
    user1 = FactoryGirl.build(:user, :email => "user6@example.com", :name => "User SA", :id => 6)
    user1.access_level = User.access_levels[:system_admin]
    users << user1

    allow(view).to receive(:current_user).and_return(user1)

    assign(:users, users)
    render

    #page_to_s

    expect(rendered).to have_content("User & Role Management")

    row = 1
    expect_table_cell_text("main", row, 1, "user1@example.com")
    expect_table_cell_text("main", row, 2, "User One")
    expect_table_cell_text("main", row, 3, "Read")
    expect_table_cell_link("main", row, 4, "Edit")
    expect_table_cell_link("main", row, 5, "Delete")
    
    row += 1
    expect_table_cell_text("main", row, 1, "user2@example.com")
    expect_table_cell_text("main", row, 2, "User Two")
    expect_table_cell_text("main", row, 3, "Read")
    expect_table_cell_link("main", row, 4, "Edit")
    expect_table_cell_link("main", row, 5, "Delete")
    
    row += 1
    expect_table_cell_text("main", row, 1, "user3@example.com")
    expect_table_cell_text("main", row, 2, "User Three")
    expect_table_cell_text("main", row, 3, "Read")
    expect_table_cell_link("main", row, 4, "Edit")
    expect_table_cell_link("main", row, 5, "Delete")
    
    row += 1
    expect_table_cell_text("main", row, 1, "user4@example.com")
    expect_table_cell_text("main", row, 2, "User Four")
    expect_table_cell_text("main", row, 3, "Read")
    expect_table_cell_link("main", row, 4, "Edit")
    expect_table_cell_link("main", row, 5, "Delete")
    
    row += 1
    expect_table_cell_text("main", row, 1, "user5@example.com")
    expect_table_cell_text("main", row, 2, "User Five")
    expect_table_cell_text("main", row, 3, "Read")
    expect_table_cell_link("main", row, 4, "Edit")
    expect_table_cell_link("main", row, 5, "Delete")
    
    row += 1
    expect_table_cell_text("main", row, 1, "user6@example.com")
    expect_table_cell_text("main", row, 2, "User SA")
    expect_table_cell_text("main", row, 3, "Read, Edit, System Admin")
    expect_table_cell_link("main", row, 4, "Edit")
    expect_table_cell_link("main", row, 5, nil)
    
    expect_link("new_user_button")

  end

end