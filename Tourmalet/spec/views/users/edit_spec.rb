require 'rails_helper'

describe 'users/edit.html.erb', :type => :view do

  include UserAccountHelpers
  include ViewHelpers

  it 'creates new user' do
    
    def view.policy(name)
      # Do nothing
    end

    user = FactoryGirl.build(:user, :email => "user1@example.com", :name => "User One", :id => 1)
    assign(:user, user)

    render

    #page_to_s

    expect(rendered).to have_content("Current Settings: User One (user1@example.com)")
    
    expect_table_cell_text("user_info", 1, 2, "user1@example.com")
    expect_table_cell_text("user_info", 2, 2, "User One")
    expect_table_cell_text("user_info", 3, 2, "Read")
    expect_table_cell_text("user_info", 4, 2, "0")

    expect(rendered).to have_content("Reader Role")
    expect(rendered).to have_content("Editor Role")
    expect(rendered).to have_content("Read, Edit & System Admin Role")
    
    expect_link("read_button")
    expect_link("edit_button")
    expect_link("system_admin_button")

  end

end