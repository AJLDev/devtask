require 'rails_helper'

describe 'devise/passwords/new.html.erb', :type => :view do

  include UserAccountHelpers
  include ViewHelpers
  
  before do
  	allow(view).to receive(:resource).and_return(User.new)
  	allow(view).to receive(:resource_name).and_return(:user)
  	allow(view).to receive(:devise_mapping).and_return(Devise.mappings[:user])
	end
  
	it 'creates new user' do

  	render

    #page_to_s

    expect(rendered).to have_content("Forgot Password?")
    expect(rendered).to have_content("Email:")
    expect_submit_button("Send me reset password instructions")

  end

end