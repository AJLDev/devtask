require 'rails_helper'

describe DeviseHelper, type: :helper do

  include FileHelpers
  
  def sub_dir
    return "helpers"
  end

  let(:resource_name) { (:user) }
  let(:devise_mapping){ (Devise.mappings[:user]) }

  context 'no error messages' do
    let(:resource) { (User.new) }
    it 'blank error message' do
      expect(devise_error_messages!).to eq("")
    end
  end

  context 'with error messages' do
    let(:resource) { (User.create) } 
    it 'non blank error message' do
    	resource.errors.add :name, "Is Invalid"
    	expected = 
    		"\t    <div class=\"alert alert-danger fade in\">\n" + 
    		"\t      <button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-label=\"Close\">\n" +
				"\t        <span aria-hidden=\"true\">&times;</span>\n" +
       	"\t      </button>\n" +
    		"\t      <p class=\"text-center\">" + 
    		"<li>Email can&#39;t be blank</li>" + 
    		"<li>Password can&#39;t be blank</li>" + 
    		"<li>Password is too short (minimum is 8 characters)</li>" + 
    		"<li>Password must contain upper and lower case letters, digits and special characters.</li>" + 
    		"<li>Name Is Invalid</li></p>\n" + 
    		"\t    </div>\n"
      expect(devise_error_messages!).to eq(expected)
    end
  end

end
