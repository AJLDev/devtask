require 'rails_helper'

describe "S8 Study SOA", :type => :feature do
  
  include PauseHelpers
  include UiHelpers
  include UserAccountHelpers
  include AuditHelpers
  include WaitForAjaxHelper

  before :all do
    Study.destroy_all
    Token.delete_all
    audit_clear
    ua_create
  end

  after :all do
    Study.destroy_all
    Token.delete_all
    audit_clear
    ua_destroy
  end
    
  describe "Edit User", :type => :feature do

    it "study version edit, detail", js: true do
      login_editor
      visit '/'
      expect(page).to have_content 'Studies'
      create_study("TEST", "A test study")
      table_click_link("main", "TEST", "History")
      expect(page).to have_content 'Study History: A test study'
      table_click_link("main", "0.1.0", "Edit")
      expect(page).to have_content 'Study Information'
      study_tab_present
      #study_tab_not_present
      soa_tab_present
      #soa_tab_not_present
      detail_tab_present
      #detail_tab_not_present
      crf_tab_present
      #crf_tab_not_present
      acrf_tab_present
      #acrf_tab_not_present
      
      fill_input 'study_version_name', "Study Name Text"
      fill_input 'study_version_description', "Description of the study"
      fill_input 'study_version_protocol_name', "Protocol Name"
      click_button 'save_button'
      check_input 'study_version_name', "Study Name Text"
      check_input 'study_version_description', "Description of the study"
      check_input 'study_version_protocol_name', "Protocol Name"
      click_link 'close_button'
      expect(page).to have_content 'Study History: A test study'
      table_click_link("main", "0.1.0", "Edit")
      check_input 'study_version_name', "Study Name Text"
      check_input 'study_version_description', "Description of the study"
      check_input 'study_version_protocol_name', "Protocol Name"
      click_link 'close_button'

      sleep 30

      click_logoff
    end

    it "study version edit, tabs", js: true do
      login_editor
      visit '/'
      expect(page).to have_content 'Studies'
    #pause
      table_click_link("main", "TEST", "History")
      expect(page).to have_content 'Study History: A test study'
      table_click_link("main", "0.1.0", "Edit")
      expect(page).to have_content 'Study Information'
      
      soa_tab_click
      expect(page).to have_content 'Schedule of Assessments'
      expect(page).to have_content 'Visit Details'
      expect(page).to have_content 'List of Forms'

      detail_tab_click
      expect(page).to have_content 'Study Detail'
      
      crf_tab_click
      expect(page).to have_content 'Study CRF'
      
      acrf_tab_click
      expect(page).to have_content 'Study Annotated CRF'

      sleep 30

      click_logoff
    end

    it "study version edit, visits", js: true do
      login_editor
      visit '/'
      expect(page).to have_content 'Studies'
    #pause
      table_click_link("main", "TEST", "History")
      expect(page).to have_content 'Study History: A test study'
      table_click_link("main", "0.1.0", "Edit")
      expect(page).to have_content 'Study Information'
      
      soa_tab_click

      # Initial state
      check_visit_left_disabled
      check_visit_right_disabled
      check_visit_add_enabled
      check_visit_delete_disabled
      check_form_up_disabled
      check_form_down_disabled
      check_form_add_enabled
      check_element_clear_disabled
      check_study_clear_disabled

      # Add visits
      click_visit_add
      soa_check_visit(2, "V1")
      check_visit_add_enabled
      check_visit_delete_enabled
      check_element_clear_enabled
      check_study_clear_enabled
      click_visit_add
      soa_check_visit(3, "V2")
      check_visit_left_enabled
      check_visit_right_disabled
      click_visit_add
      soa_check_visit(4, "V3")
      
      # Move visits
      soa_click_visit(3)
      check_visit_left_enabled
      check_visit_right_enabled
      click_visit_left
      wait_for_ajax
      soa_check_visit(2, "V2")
      soa_check_visit(3, "V1")
      soa_check_visit(4, "V3")
      
      soa_click_visit(4)
      check_visit_left_enabled
      check_visit_right_disabled
      click_visit_left
      wait_for_ajax
      soa_check_visit(2, "V2")
      soa_check_visit(3, "V3")
      soa_check_visit(4, "V1")
      
      soa_click_visit(2)
      check_visit_left_disabled
      check_visit_right_enabled
      click_visit_right
      wait_for_ajax
      soa_check_visit(2, "V3")
      soa_check_visit(3, "V2")
      soa_check_visit(4, "V1")
      
      check_visit_left_enabled
      check_visit_right_enabled
      click_visit_right
      wait_for_ajax
      soa_check_visit(2, "V3")
      soa_check_visit(3, "V1")
      soa_check_visit(4, "V2")

      check_visit_left_enabled
      check_visit_right_disabled

      soa_click_visit(2)
      check_visit_left_disabled
      check_visit_right_enabled
      soa_click_visit(3)
      check_visit_left_enabled
      check_visit_right_enabled
      soa_click_visit(4)
      check_visit_left_enabled
      check_visit_right_disabled

      # Edit details
      soa_click_visit(2)
      wait_for_ajax
      fill_input "soa_visit_short", "V0"
      fill_input "soa_visit_long", "Visit Zero - Screening"
      fill_input "soa_visit_timing", "Day 0, the beginning."
      click_visit_update
      wait_for_ajax
      soa_check_visit(2, "V0")
      check_input "soa_visit_short", "V0"
      check_input "soa_visit_long", "Visit Zero - Screening"
      check_input "soa_visit_timing", "Day 0, the beginning."

      soa_click_visit(3)
      wait_for_ajax      
      fill_input "soa_visit_short", "V1"
      fill_input "soa_visit_long", "Visit One"
      fill_input "soa_visit_timing", "Day 7"
      click_visit_update
      wait_for_ajax
      soa_check_visit(3, "V1")
      check_input "soa_visit_short", "V1"
      check_input "soa_visit_long", "Visit One"
      check_input "soa_visit_timing", "Day 7"

      soa_click_visit(4)
      wait_for_ajax
      fill_input "soa_visit_short", "V2"
      fill_input "soa_visit_long", "Visit Two"
      fill_input "soa_visit_timing", "Day 14"
      click_visit_update
      wait_for_ajax
      soa_check_visit(4, "V2")
      check_input "soa_visit_short", "V2"
      check_input "soa_visit_long", "Visit Two"
      check_input "soa_visit_timing", "Day 14"
      
      # Delete visits
      soa_click_visit(3)
      click_visit_delete
      wait_for_ajax
      soa_check_visit(2, "V0")
      soa_check_visit(3, "V2")

      sleep 30

      click_logoff
    end

    it "study version edit, forms", js: true do
      set_screen_size(1500, 1000)
      login_editor
      visit '/'
      expect(page).to have_content 'Studies'
      table_click_link("main", "TEST", "History")
      expect(page).to have_content 'Study History: A test study'
      table_click_link("main", "0.1.0", "Edit")
      expect(page).to have_content 'Study Information'
      
      soa_tab_click
      check_form_up_disabled
      check_form_down_disabled
      form_index_check_form(1, 1, "AE NEW")
      form_index_check_form(2, 1, "AE1 01")
      form_index_click_identifier "EQ 5D 3L"
      click_form_toggle  # To history
      form_history_check_form(1, 1, "EQ 5D 3 Level")
      #form_history_check_form(2, 1, "AE1 01")
      #form_history_check_form(3, 1, "DM1 01")
      click_form_toggle  # To index
      form_index_check_form(4, 1, "EQ 5D 3L")
      #click_form_toggle
      #form_history_check_form(1, 1, "AE NEW")
			# EQ 5D 3L
      #click_form_toggle
      form_index_click_identifier "EQ 5D 3L"
      click_form_add
      wait_for_ajax
      soa_check_form(1, "EQ 5D 3 Level")
      check_element_clear_enabled
      check_study_clear_enabled
      # MH1 01
      form_index_click_identifier "MH1 01"
      click_form_add
      wait_for_ajax
      soa_check_form(2, "Medical History")
      wait_for_ajax     
      # DM1 01
      form_index_click_identifier "DM1 01"
      click_form_toggle # To history
      wait_for_ajax     
      form_history_click_version "0.0.0"
      click_form_add
      wait_for_ajax
      soa_check_form(3, "Demographics")
      # AE NEW
      click_form_toggle # To index
      wait_for_ajax     
      form_index_click_identifier "AE NEW"
      click_form_toggle # To history
      wait_for_ajax     
      form_history_click_version "0.1.0"
      click_form_add
      wait_for_ajax
      # SOA
      soa_check_form(4, "Adverse Events")
      soa_click_form(3)
      click_form_delete
      wait_for_ajax
      soa_check_form(1, "EQ 5D 3 Level")
      soa_check_form(2, "Medical History")
      soa_check_form(3, "Adverse Events")
      soa_click_form(3)
      click_form_delete
      wait_for_ajax
      soa_check_form(1, "EQ 5D 3 Level")
      soa_check_form(2, "Medical History")
      # DM1 01
      click_form_toggle # To index
      form_index_click_identifier "DM1 01"
      click_form_toggle # To history
      wait_for_ajax     
      form_history_click_version "0.0.0"
      click_form_add
      wait_for_ajax
      soa_check_form(3, "Demographics")

      check_form_up_enabled
      check_form_down_disabled
      soa_click_form(1)
      check_form_up_disabled
      check_form_down_enabled
      soa_click_form(2)
      check_form_up_enabled
      check_form_down_enabled
      soa_click_form(3)
      check_form_up_enabled
      check_form_down_disabled

      sleep 30

      click_logoff
    end

    it "study version edit, elements", js: true do
      set_screen_size(800, 800)
      login_editor
      visit '/'
      expect(page).to have_content 'Studies'
      table_click_link("main", "TEST", "History")
      expect(page).to have_content 'Study History: A test study'
      table_click_link("main", "0.1.0", "Edit")
      expect(page).to have_content 'Study Information'
      
      soa_tab_click
      soa_click_element(1, 2)
      wait_for_ajax
      soa_check_element(1, 2, true)
      soa_check_element(1, 3, false)
      soa_check_element(2, 2, false)
      soa_check_element(2, 3, false)
      soa_check_element(3, 2, false)
      soa_check_element(3, 3, false)
    
      soa_click_element(3, 3)
      wait_for_ajax
      soa_check_element(1, 2, true)
      soa_check_element(1, 3, false)
      soa_check_element(2, 2, false)
      soa_check_element(2, 3, false)
      soa_check_element(3, 2, false)
      soa_check_element(3, 3, true)
      
      soa_click_element(1, 2)
      wait_for_ajax
      soa_check_element(1, 2, false)
      soa_check_element(1, 3, false)
      soa_check_element(2, 2, false)
      soa_check_element(2, 3, false)
      soa_check_element(3, 2, false)
      soa_check_element(3, 3, true)
      
      soa_click_element(2, 2)
      wait_for_ajax
      soa_check_element(1, 2, false)
      soa_check_element(1, 3, false)
      soa_check_element(2, 2, true)
      soa_check_element(2, 3, false)
      soa_check_element(3, 2, false)
      soa_check_element(3, 3, true)

      click_element_clear
      wait_for_ajax
      soa_check_element(1, 2, false)
      soa_check_element(1, 3, false)
      soa_check_element(2, 2, false)
      soa_check_element(2, 3, false)
      soa_check_element(3, 2, false)
      soa_check_element(3, 3, false)

      click_study_clear
      wait_for_ajax

      check_visit_left_disabled
      check_visit_right_disabled
      check_visit_add_enabled
      check_visit_delete_disabled
      check_form_up_disabled
      check_form_down_disabled
      check_form_add_enabled
      check_element_clear_disabled
      check_study_clear_disabled

      sleep 30

      click_logoff
    end

  end

end