require 'rails_helper'

describe "S1 Access Control", :type => :feature do
  
  include UserAccountHelpers
  include UiHelpers
  include PauseHelpers
  
  describe "Login and Logout, Reset Email", :type => :feature do
  
    before :all do
      ua_create
    end

    after :all do
      ua_destroy
    end

    before :each do
      AuditTrail.delete_all
    end

    it "allows valid credentials and logs" do
      audit_count = AuditTrail.count
      login_reader
      expect(AuditTrail.count).to eq(audit_count + 1)
    end

    it "rejects invalid credentials" do
      audit_count = AuditTrail.count
      visit '/users/sign_in'
      fill_in 'Email', with: reader_email
      fill_in 'Password', with: 'example1234'
      click_login
      expect(page).to have_content 'Log in'
      expect(AuditTrail.count).to eq(audit_count)
    end

    it "allows logout and audit logs" do
      audit_count = AuditTrail.count
      login_reader
      click_logoff
      expect(AuditTrail.count).to eq(audit_count + 2)
    end

    it "allows password reset email to be sent" do
      email_count = ActionMailer::Base.deliveries.count
      visit '/users/sign_in'
      click_link 'Forgot your password?'
      fill_in 'Email', with: reader_email
      click_button 'Send me reset password instructions'
      expect(page).to have_content 'Log in'
      expect(ActionMailer::Base.deliveries.count).to eq(email_count + 1)
    end

  end

  describe "User Management", :type => :feature do
  
    before :all do
      User.destroy_all
      ua_create
    end

    after :all do
      ua_destroy
      User.destroy_all
    end

    before :each do
      AuditTrail.delete_all
    end
          
    it "allows correct reader access" do
      login_reader
      check_edit_lock_not_present
      check_audit_not_present
      check_users_not_present
      check_print_present
      check_settings_present
      check_home_present
      check_logoff_present
      expect(page).to have_content 'A Reader [Read]'
      click_logoff
    end

    it "allows correct editor access" do
      login_editor
    #save_and_open_page
      check_edit_lock_not_present
      check_audit_not_present
      check_users_not_present
      check_print_present
      check_settings_present
      check_home_present
      check_logoff_present
      expect(page).to have_content 'A Editor [Read, Edit]'   
      click_logoff
    end

    it "allows correct system admin access" do
      login_system_admin
      check_edit_lock_present
      check_audit_present
      check_users_present
      check_print_present
      check_settings_present
      check_home_present
      check_logoff_present
      expect(page).to have_content 'A Admin [Read, Edit, System Admin]'   
      click_logoff
    end

    it "allows new user to be created" do
      audit_count = AuditTrail.count
      login_system_admin
      click_link 'users_button'
      expect(page).to have_content 'User & Role Management'
      click_link 'New'
      expect(page).to have_content 'New User'
      fill_in 'Email', with: 'new_user@assero.co.uk'
      fill_in 'Display Name', with: 'New User'
      fill_in 'Password:', with: '123456789aA*'
      fill_in 'Password Confirmation:', with: '123456789aA*'
      click_button 'Create'
      expect(page).to have_content 'User was successfully created.'
      expect(page).to have_content 'New User'
      expect(AuditTrail.count).to eq(audit_count + 3)
      click_logoff
    end

    it "prevents a new user with short password being created" do
      login_system_admin
      click_link 'users_button'
      expect(page).to have_content 'User & Role Management'
      click_link 'New'
      expect(page).to have_content 'New User'
      fill_in 'Email', with: 'new_user_2@assero.co.uk'
      fill_in 'Password:', with: '1234567'
      fill_in 'Password Confirmation:', with: '1234567'
      click_button 'Create'
      expect(page).to have_content 'User was not created.'
      click_logoff
    end    

    it "allows a user's role to be modified", js: true do
      audit_count = AuditTrail.count
      login_system_admin
      click_link 'users_button'
      expect(page).to have_content 'User & Role Management'
      create_user('new_user@assero.co.uk', '123456789aA*', "N User")

      table_row_link_click("main", "new_user@assero.co.uk", "Edit")
      click_link 'edit_button'
      row = table_get_row("main", 1, "new_user@assero.co.uk")
      expect(row).to_not eq(-1)
      role = table_check_cell("main", row, 3, "Read, Edit")

      table_row_link_click("main", "new_user@assero.co.uk", "Edit")
      click_link 'system_admin_button'
      row = table_get_row("main", 1, "new_user@assero.co.uk")
      expect(row).to_not eq(-1)
      role = table_check_cell("main", row, 3, "Read, Edit, System Admin")

      table_row_link_click("main", "new_user@assero.co.uk", "Edit")
      click_link 'read_button'
      row = table_get_row("main", 1, "new_user@assero.co.uk")
      expect(row).to_not eq(-1)
      role = table_check_cell("main", row, 3, "Read")

      table_row_link_click("main", "new_user@assero.co.uk", "Delete")
      alert_click_ok
    end

    it "allows a user to be deleted", js: true do
      audit_count = AuditTrail.count
      login_system_admin
      expect(AuditTrail.count).to eq(audit_count + 1)
      click_users
      create_user('new_user@assero.co.uk', '123456789aA*', "N User")
      table_row_link_click("main", "new_user@assero.co.uk", "Delete")
      alert_click_ok
      expect(page).to have_content 'User new_user@assero.co.uk was successfully deleted.'
    #pause
      expect(AuditTrail.count).to eq(audit_count + 4)
      click_logoff
    end
      
    it "allows a user to be deleted, decline", js: true do
      audit_count = AuditTrail.count
      login_system_admin
      expect(AuditTrail.count).to eq(audit_count + 1)
      click_users
      create_user('new_user@assero.co.uk', '123456789aA*', "N User")
      table_row_link_click("main", "new_user@assero.co.uk", "Delete")
      alert_click_cancel
      table_row_link_click("main", "new_user@assero.co.uk", "Edit")
      expect(page).to have_content "Current Settings: N User"
      expect(AuditTrail.count).to eq(audit_count + 3)
      click_logoff
    end

    it "allows a user to change their password" do
      audit_count = AuditTrail.count
      user = User.create :email => "amend@assero.co.uk", :password => "Changeme1%", :name => "A Amend"
      login_user "amend@assero.co.uk", "Changeme1%" 
      click_settings
      fill_in 'user_password', with: 'Changeme1@'
      fill_in 'user_password_confirmation', with: 'Changeme1@'
      fill_in 'Current Password', with: 'Changeme1%'
      click_button 'password_update_button'
      expect(page).to have_content 'Your account has been updated successfully.'
      expect(AuditTrail.count).to eq(audit_count + 3)
    end

    it "allows a user to change their password - incorrect current password" do
      audit_count = AuditTrail.count
      user = User.create :email => "amend@assero.co.uk", :password => "Changeme1@", :name => "A Amend" 
      login_user "amend@assero.co.uk", "Changeme1@" 
      click_settings
      fill_in 'user_password', with: 'Changeme1^'
      fill_in 'user_password_confirmation', with: 'Changeme1^'
      fill_in 'Current Password', with: 'Changeme1x'
      click_button 'password_update_button'
      expect(page).to have_content 'Current password is invalid'
      expect(AuditTrail.count).to eq(audit_count + 2)
    end

    it "force a password change - correct change" do
			Devise.expire_password_after = 1.seconds
      audit_count = AuditTrail.count
      user = User.create :email => "amend@assero.co.uk", :password => "Changeme1@", :name => "A Amend" 
      login_user "amend@assero.co.uk", "Changeme1@" 
      sleep 5
      click_logoff
      visit '/users/sign_in'
    	fill_in 'Email', with: "amend@assero.co.uk"
    	fill_in 'Password', with: "Changeme1@"
    	click_login 
      expect(page).to have_content 'Update Password'
      fill_in 'Current Password', with: 'Changeme1@'
      fill_in 'user_password', with: 'Changeme1^'
      fill_in 'user_password_confirmation', with: 'Changeme1^'
      click_button 'submit_button'
      expect(page).to have_content 'Your account has been updated successfully'
      expect(AuditTrail.count).to eq(audit_count + 5)
      epa = ENV['expire_password_after'].to_i
  		Devise.expire_password_after = epa.days
    end
    
    it "force a password change -incorrect current password" do
			Devise.expire_password_after = 1.seconds
      audit_count = AuditTrail.count
      user = User.create :email => "amend@assero.co.uk", :password => "Changeme1@", :name => "A Amend" 
      login_user "amend@assero.co.uk", "Changeme1@" 
      sleep 5
      click_logoff
      visit '/users/sign_in'
    	fill_in 'Email', with: "amend@assero.co.uk"
    	fill_in 'Password', with: "Changeme1@"
    	click_login 
      expect(page).to have_content 'Update Password'
      fill_in 'Current Password', with: 'Changeme1^'
      fill_in 'user_password', with: 'Changeme1^'
      fill_in 'user_password_confirmation', with: 'Changeme1^'
      click_button 'submit_button'
      expect(page).to have_content 'Update Password'
      expect(page).to have_content 'Current password is invalid'
      fill_in 'Current Password', with: 'Changeme1@'
      fill_in 'user_password', with: 'Changeme1^'
      fill_in 'user_password_confirmation', with: 'Changeme1^'
      click_button 'submit_button'
      expect(page).to have_content 'Your account has been updated successfully'
      expect(AuditTrail.count).to eq(audit_count + 5)
      epa = ENV['expire_password_after'].to_i
  		Devise.expire_password_after = epa.days
    end

  end

end