require 'rails_helper'

describe "S8 Study CRF", :type => :feature do

  include PauseHelpers
  include UiHelpers
  include UserAccountHelpers
  include AuditHelpers
  include WaitForAjaxHelper

  before :all do
    Study.destroy_all
    Token.delete_all
    audit_clear
    ua_create
  end

  after :all do
    Study.destroy_all
    Token.delete_all
    audit_clear
    ua_destroy
  end

  describe "Edit User", :type => :feature do

    it "study version edit, tabs", js: true do
      login_editor
      visit '/'
      expect(page).to have_content 'Studies'
      create_study("TEST", "A test study")
      table_click_link("main", "TEST", "History")
      expect(page).to have_content 'Study History: A test study'
      table_click_link("main", "0.1.0", "Edit")
      expect(page).to have_content 'Study Information'

      mini_study

      crf_tab_click
      expect(page).to have_content 'Study CRF'

      click_logoff
    end

  end

  describe "Read User", :type => :feature do

    it "study version, tabs", js: true do
      login_reader
      visit '/'
      expect(page).to have_content 'Studies'
      table_click_link("main", "TEST", "History")
      expect(page).to have_content 'Study History: A test study'
      table_click_link("main", "0.1.0", "View")
      expect(page).to have_content 'Study Information'
      crf_tab_click
      expect(page).to have_content 'Study CRF'
      click_logoff
    end

    it "generates the CRF", js: true do
      # Capybara.page.driver.browser.manage.window.maximize
      login_editor
      visit '/'
      create_study("TEST1", "A test1 study")
      table_click_link("main", "TEST1", "History")
      expect(page).to have_content 'Study History: A test1 study'
      table_click_link("main", "0.1.0", "Edit")
      expect(page).to have_content 'Study Information'
      mini_study
      crf_tab_click
      find("#study_crf").click # check for html report in same tab
      wait_for_ajax
      html_report = find("#crfHtml")
      # expect(html_report).to have_selector('table', count: 19)
      expect(html_report).to have_selector('#soa_table', count: 1)

      sleep 10

      click_logoff
    end

    it "generates pdf for the CRF in new window", js: true do
      # Capybara.page.driver.browser.manage.window.maximize
      login_editor
      visit '/'
      create_study("TEST2", "A test2 study")
      table_click_link("main", "TEST2", "History")
      expect(page).to have_content 'Study History: A test2 study'
      table_click_link("main", "0.1.0", "Edit")
      expect(page).to have_content 'Study Information'
      mini_study
      crf_tab_click  # open last tab to generate CRF in pdf or HTML format
      # check for pdf format and new window
      new_window = window_opened_by { find("#export_crf").click }
      within_window new_window do
      end
      sleep 10
      click_logoff
    end

  end

end