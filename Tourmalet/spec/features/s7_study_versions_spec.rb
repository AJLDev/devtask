require 'rails_helper'

describe "S7 Study Versions", :type => :feature do
  
  include PauseHelpers
  include UiHelpers
  include UserAccountHelpers
  include AuditHelpers

  before :all do
    Study.destroy_all
    audit_clear
    ua_create
  end

  after :all do
    Study.destroy_all
    audit_clear
    ua_destroy
  end
    
  describe "Edit User", :type => :feature do

    it "creates a study and view history", js: true do
      login_editor
      visit '/'
      expect(page).to have_content 'Studies'
      create_study("TEST", "A test study")
    #pause
      table_click_link("main", "TEST", "History")
      expect(page).to have_content 'Study History: A test study'
      table_check_cell_exists("main", 1, "0.1.0")
      table_check_cell_exists("main", 2, "A test study")
      click_logoff
    end

    it "study version view", js: true do
      login_editor
      visit '/'
      expect(page).to have_content 'Studies'
      table_click_link("main", "TEST", "History")
      expect(page).to have_content 'Study History: A test study'
      table_click_link("main", "0.1.0", "View")
      expect(page).to have_content 'Study Information'
      study_tab_present
      #study_tab_not_present
      soa_tab_present
      #soa_tab_not_present
      #detail_tab_present
      detail_tab_not_present
      crf_tab_present
      #crf_tab_not_present
      acrf_tab_present
      #acrf_tab_not_present
      click_link 'close_button'
      expect(page).to have_content 'Study History: A test study'
      click_logoff
    end

    it "study version edit", js: true do
      login_editor
      visit '/'
      expect(page).to have_content 'Studies'
      table_click_link("main", "TEST", "History")
      expect(page).to have_content 'Study History: A test study'
      table_click_link("main", "0.1.0", "Edit")
      expect(page).to have_content 'Study Information'
      study_tab_present
      #study_tab_not_present
      soa_tab_present
      #soa_tab_not_present
      detail_tab_present
      #detail_tab_not_present
      crf_tab_present
      #crf_tab_not_present
      acrf_tab_present
      #acrf_tab_not_present
      click_link 'close_button'
      expect(page).to have_content 'Study History: A test study'
      click_logoff
    end

    it "study version status", js: true do
      login_editor
      visit '/'
      expect(page).to have_content 'Studies'
      table_click_link("main", "TEST", "History")
      expect(page).to have_content 'Study History: A test study'
      table_click_link("main", "0.1.0", "Status")
      expect(page).to have_content 'Status'
      click_link 'close_button'
      expect(page).to have_content 'Study History: A test study'
      click_logoff
    end

  end

end