require 'rails_helper'

describe "Tokens", :type => :feature do
  
  include PauseHelpers
  include UiHelpers
  include UserAccountHelpers
  
  before :all do
    ua_create
    Token.delete_all
    Token.set_timeout(60)
    @user1 = User.create :email => "token@assero.co.uk", :password => "12345678We!" 
    item1 = "ITEMX"
    item2 = "ITEMY"
    item3 = "ITEMZ"
    item4 = "ITEMA"
    token1 = Token.obtain(item1, "", @user1)
    token2 = Token.obtain(item2, "", @user1)
    token3 = Token.obtain(item3, "", @user1)
    token4 = Token.obtain(item4, "", @user1)
  end

  after :all do
    ua_destroy
    user = User.where(:email => "token@assero.co.uk").first
    user.destroy
  end

  describe "System Admin User", :type => :feature do
  
    it "allows the tokens to be viewed", js: true do
      login_system_admin
      click_edit_locks        
      expect(page).to have_content 'Edit Locks'  
      expect(page.all('table#main tr').count).to eq(5)
      expect(page).to have_content "ITEMX"
      expect(page).to have_content "ITEMY"
      expect(page).to have_content "ITEMZ"
      expect(page).to have_content "ITEMA"
    end

    it "allows a lock to be released", js: true do
      login_system_admin
      click_edit_locks        
      expect(page).to have_content 'Edit Locks'  
      expect(page.all('table#main tr').count).to eq(5)
      table_click_link("main", "ITEMX", "Release")
      alert_click_ok("Are you sure?")
      expect(page.all('table#main tr').count).to eq(4)
      expect(page).to have_content "ITEMY"
      expect(page).to have_content "ITEMZ"
      expect(page).to have_content "ITEMA"
      #pause
    end

    it "allows a lock to be released, rejection", js: true do
      login_system_admin
      click_edit_locks        
      expect(page).to have_content 'Edit Locks'  
      table_click_link("main", "ITEMY", "Release")
      alert_click_cancel("Are you sure?")
      expect(page.all('table#main tr').count).to eq(4)
      expect(page).to have_content "ITEMY"
      expect(page).to have_content "ITEMZ"
      expect(page).to have_content "ITEMA"
    end

  end

end