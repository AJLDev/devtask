require 'rails_helper'

describe "S6 Studies", :type => :feature do
  
  include PauseHelpers
  include UiHelpers
  include UserAccountHelpers
  include AuditHelpers

  before :all do
    Study.destroy_all
    audit_clear
    ua_create
  end

  after :all do
    Study.destroy_all
    audit_clear
    ua_destroy
  end
    
  describe "Edit User", :type => :feature do

    it "creates a study", js: true do
      audit_base
      login_editor
      visit '/'
      expect(page).to have_content 'Studies'
      create_study("TEST", "A test study")
      table_click_link("main", "TEST", "History")
      expect(page).to have_content 'Study History'
      click_logoff
      audit_check_inc(4) # 2 events, study creation, version creation
    end

    it "creates a study, duplicate", js: true do
      login_editor
      visit '/'
      expect(page).to have_content 'Studies'
      create_study("TEST", "A test study")
      expect(page).to have_content 'Identifier has already been taken'
      click_logoff
    end

    it "deletes a study, ok", js: true do
      login_editor
      visit '/'
      expect(page).to have_content 'Studies'
      create_study("TEST1", "Another test study")
      table_click_link("main", "TEST1", "Delete")
      alert_click_ok("Are you sure?")
      expect(page).to have_content 'Studies'
      table_check_row_count("main", 1)
      click_logoff
    end

    it "deletes a study, ok", js: true do
      login_editor
      visit '/'
      expect(page).to have_content 'Studies'
      create_study("TEST2", "Another test study")
      table_click_link("main", "TEST2", "Delete")
      alert_click_cancel("Are you sure?")
      expect(page).to have_content 'Studies'
      table_check_row_count("main", 2)
      click_logoff
    end

  end

end