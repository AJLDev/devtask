require 'rails_helper'

describe "S12 API", :type => :request do
  
  include PauseHelpers
  include UiHelpers
  include UserAccountHelpers
  include StudyVersionHelpers
  
  before :all do
  end

  after :all do
  end

  describe "access to studies", :type => :request do

	  def set_http_headers
  		# JSON and username, password
  		headers = {}
  		headers['HTTP_ACCEPT'] = "application/json"
    	headers['HTTP_AUTHORIZATION'] = ActionController::HttpAuthentication::Basic.encode_credentials(ENV['api_username'],ENV['api_password'])
    	return headers
  	end

    before :each do
      Study.delete_all
      StudyVersion.delete_all
    end

    after :each do
      Study.delete_all
      StudyVersion.delete_all
    end

    it "provides a studies index" do
      s1 = Study.create(label: "Study 1", identifier: "STUDY 1")
      s2 = Study.create(label: "Study 2", identifier: "STUDY 2")
      s3 = Study.create(label: "Study 3", identifier: "STUDY 3")
      get "/api/v1/studies", {}, set_http_headers
      expect(response.status).to eq 200
      result = JSON.parse(response.body)
      expect(result.length).to eq(3)
    end

    it "provides a study history, no study version" do
      s1 = Study.create(label: "Study 1", identifier: "STUDY 1")
      s2 = Study.create(label: "Study 2", identifier: "STUDY 2")
      s3 = Study.create(label: "Study 3", identifier: "STUDY 3")
      get "/api/v1/studies/#{s1.id}/history", {}, set_http_headers
      expect(response.status).to eq 200
      result = JSON.parse(response.body)
      expect(result).to eq([])
    end

    it "provides a study history, no study version" do
      s1 = Study.create(label: "Study 1", identifier: "STUDY 1")
      sv1 = create_study_version(s1.id, "V1", "description", "whatever", StudyVersion.states[:incomplete], 1, "0.1.0")
      sv2 = create_study_version(s1.id, "V2", "description", "whatever", StudyVersion.states[:standard], 1, "1.0.0")
      sv3 = create_study_version(s1.id, "V3", "description", "whatever", StudyVersion.states[:standard], 1, "2.0.0")
      s2 = Study.create(label: "Study 2", identifier: "STUDY 2")
      s3 = Study.create(label: "Study 3", identifier: "STUDY 3")
      get "/api/v1/studies/#{s1.id}/history", {}, set_http_headers
      expect(response.status).to eq 200
      result = JSON.parse(response.body)
      expect(result.count).to eq(3)
      expect(result[0]["id"]).to eq(sv1.id)
      expect(result[1]["id"]).to eq(sv2.id)
      expect(result[2]["id"]).to eq(sv3.id)
    end

    it "provides a study history, error" do
      get "/api/v1/studies/1/history", {}, set_http_headers
      expect(response.status).to eq 404
      result = JSON.parse(response.body)
      expect(result).to eq({"errors"=>["Failed to find study with identifier 1"]})
    end

    it "provides a study version" do
      s1 = Study.create(label: "Study 1", identifier: "STUDY 1")
      sv1 = create_study_version(s1.id, "V1", "description", "whatever", StudyVersion.states[:incomplete], 1, "0.1.0")
      get "/api/v1/studies/#{s1.id}/study_versions/#{sv1.id}", {}, set_http_headers
      expect(response.status).to eq 200
      result = JSON.parse(response.body)
      expect(result["id"]).to eq(sv1.id)
    end

    it "provides a study version" do
      s1 = Study.create(label: "Study 1", identifier: "STUDY 1")
      sv1 = create_study_version(s1.id, "V1", "description", "whatever", StudyVersion.states[:incomplete], 1, "0.1.0")
      get "/api/v1/studies/#{s1.id}/study_versions/1", {}, set_http_headers
      expect(response.status).to eq 404
      result = JSON.parse(response.body)
      expect(result).to eq({"errors"=>["Failed to find study version with identifier 1"]})
    end

  end

end
