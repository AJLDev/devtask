require 'rails_helper'

describe "S3 Audit Trail", :type => :feature do
  
  include PauseHelpers
  include UiHelpers
  include UserAccountHelpers
  
  before :all do
    ua_create
    user1 = User.create :email => "audit_trail_user_1@assero.co.uk", :password => "Changeme2%", name: "fred"
    user2 = User.create :email => "audit_trail_user_2@assero.co.uk", :password => "Changeme1%", name: "boris"
    AuditTrail.delete_all
    @now1 = Time.now - 70
    @now2 = Time.now - 80
    ar = AuditTrail.create(date_time: @now1, user: "audit_trail_user_1@assero.co.uk", identifier: "I1", version: "1", event: 1, description: "description")
    ar = AuditTrail.create(date_time: @now2, user: "audit_trail_user_1@assero.co.uk", identifier: "I2", version: "1", event: 1, description: "description")
    ar = AuditTrail.create(date_time: Time.now - 90, user: "audit_trail_user_1@assero.co.uk", identifier: "T1", version: "1", event: 1, description: "description")
    ar = AuditTrail.create(date_time: Time.now - 100, user: "audit_trail_user_1@assero.co.uk", identifier: "T2", version: "2", event: 1, description: "description")
    ar = AuditTrail.create(date_time: Time.now - 110, user: "audit_trail_user_1@assero.co.uk", identifier: "T3", version: "3", event: 1, description: "description")
    ar = AuditTrail.create(date_time: Time.now - 120, user: "audit_trail_user_1@assero.co.uk", identifier: "T1", version: "1", event: 2, description: "description")
    ar = AuditTrail.create(date_time: Time.now - 130, user: "audit_trail_user_1@assero.co.uk", identifier: "T2", version: "2", event: 2, description: "description")
    ar = AuditTrail.create(date_time: Time.now - 140, user: "audit_trail_user_1@assero.co.uk", identifier: "T3", version: "3", event: 2, description: "description")
    ar = AuditTrail.create(date_time: Time.now - 150, user: "audit_trail_user_1@assero.co.uk", identifier: "T1", version: "1", event: 3, description: "description")
    ar = AuditTrail.create(date_time: Time.now - 160, user: "audit_trail_user_1@assero.co.uk", identifier: "T2", version: "2", event: 3, description: "description")
    ar = AuditTrail.create(date_time: Time.now - 170, user: "audit_trail_user_1@assero.co.uk", identifier: "T3", version: "3", event: 3, description: "description")
    ar = AuditTrail.create(date_time: Time.now - 180, user: "audit_trail_user_1@assero.co.uk", identifier: "", version: "", event: 4, description: "Login")
    ar = AuditTrail.create(date_time: Time.now - 190, user: "audit_trail_user_1@assero.co.uk", identifier: "", version: "", event: 4, description: "Logout")
    ar = AuditTrail.create(date_time: Time.now - 200, user: "audit_trail_user_2@assero.co.uk", identifier: "", version: "", event: 4, description: "Login")
    ar = AuditTrail.create(date_time: Time.now - 210, user: "audit_trail_user_2@assero.co.uk", identifier: "", version: "", event: 4, description: "Logout")
  end

  after :all do
    ua_destroy
    user = User.where(:email => "audit_trail_user_1@assero.co.uk").first
    user.destroy
    user = User.where(:email => "audit_trail_user_2@assero.co.uk").first
    user.destroy
  end

  describe "system admin allowed access to audit", :type => :feature do
  
    it "allows viewing" do
      login_system_admin
      #save_and_open_page
      click_audit
      expect(page).to have_content 'Audit Trail'
    end

    it "check ordering with latest in first row", js: true do
      login_system_admin
      click_audit
      expect(page).to have_content 'Audit Trail'
      table_check_row("main", 2, [Timestamp.new(@now1).to_datetime, "audit_trail_user_1@assero.co.uk", "I1", "1", "Create"])
      table_check_row("main", 3, [Timestamp.new(@now2).to_datetime, "audit_trail_user_1@assero.co.uk", "I2", "1", "Create"])
    end

    it "allows searching - event" do
      login_system_admin
      click_audit
      expect(page).to have_content 'Audit Trail'
      select 'User', from: "audit_trail_event"
      click_button 'Submit'
      expect(page.all('table#main tr').count).to eq(7) # Note, these counts are records expected + 1 for the header row. Also include logins in the tests (3)
    end

    it "allows searching - user" do
      login_system_admin
      click_audit
      expect(page).to have_content 'Audit Trail'
      select 'audit_trail_user_2@assero.co.uk', from: "audit_trail_user"
      click_button 'Submit'
      expect(page.all('table#main tr').count).to eq(3)
    end

    it "allows searching - identifier" do
      login_system_admin
      click_audit
      expect(page).to have_content 'Audit Trail'
      fill_in 'Identifier', with: 'T1'
      click_button 'Submit'
      expect(page.all('table#main tr').count).to eq(4)
    end

    it "allows searching - combined" do
      login_system_admin
      click_audit
      expect(page).to have_content 'Audit Trail'
      select 'audit_trail_user_1@assero.co.uk', from: "audit_trail_user"
      fill_in 'Identifier', with: 'I2'
      click_button 'Submit'
      expect(page.all('table#main tr').count).to eq(2)
    end

  end

end