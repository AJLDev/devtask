require 'rails_helper'

describe "S14 General", :type => :feature do
  
  include PauseHelpers
  include UiHelpers
  include UserAccountHelpers
  include AuditHelpers

  before :all do
    Study.destroy_all
    audit_clear
    ua_create
  end

  after :all do
    Study.destroy_all
    audit_clear
    ua_destroy
  end
    
  describe "Edit User", :type => :feature do

    it "check page title", js: true do
      audit_base
      login_editor
      visit '/'
      expect(page.title).to have_content "Glandon Study Builder"
      expect(page).to have_content "Glandon Study Builder (v#{Version::VERSION})"
    end

  end

end