require 'rails_helper'

describe "S2 User Settings", :type => :feature do
  
  include PauseHelpers
  include UserAccountHelpers
  include UiHelpers

  before :all do
    ua_create
  end

  after :all do
    ua_destroy
  end

  describe "amending settings", :type => :feature do
  
    it "allows paper size to be amended", js: true do
      login_reader
      click_link 'settings_button'
    #pause
      expect(page).to have_content 'Current Settings:'
      tr = page.find('#user_settings tbody tr', text: 'Paper Size')
      expect(tr).to have_css("a", text: "Letter")
      expect(tr).to have_css("a", text: "A3")
      expect(tr).to have_css("a", text: "A4")
      click_link 'A3'
      tr = page.find('#user_settings tbody tr', text: 'Paper Size')
      expect(tr).to have_css("a", text: "A3")
      click_link 'A4'
      tr = page.find('#user_settings tbody tr', text: 'Paper Size')
      expect(tr).to have_css("a", text: "A4")
      click_link 'Letter'
      tr = page.find('#user_settings tbody tr', text: 'Paper Size')
      expect(tr).to have_css("a", text: "Letter")
      click_logoff
    end

    it "allows table rows to be amended", js: true do
      login_reader
      click_link 'settings_button'
      expect(page).to have_content 'Current Settings:'
      tr = page.find('#user_settings tbody tr', text: 'Table Rows')
      expect(tr).to have_css("a", text: "10")
      expect(tr).to have_css("a", text: "5")
      expect(tr).to have_css("a", text: "15")
      expect(tr).to have_css("a", text: "25")
      expect(tr).to have_css("a", text: "50")
      expect(tr).to have_css("a", text: "100")
      expect(tr).to have_css("a", text: "All")
      click_link '25'
      tr = page.find('#user_settings tbody tr', text: 'Table Rows')
      expect(tr).to have_css("a", text: "25")
      click_link 'All'
      tr = page.find('#user_settings tbody tr', text: 'Table Rows')
      expect(tr).to have_css("a", text: "All")
      click_link '100'
      tr = page.find('#user_settings tbody tr', text: 'Table Rows')
      expect(tr).to have_css("a", text: "100")
      click_logoff
    end

    it "allows edit lock timeout to be amended", js: true do
      login_reader
      click_link 'settings_button'
      expect(page).to have_content 'Current Settings:'
      tr = page.find('#user_settings tbody tr', text: 'Edit Lock Warning')
      expect(tr).to have_css("a", text: "1m")
      expect(tr).to have_css("a", text: "30s")
      expect(tr).to have_css("a", text: "1m 30s")
      expect(tr).to have_css("a", text: "2m")
      expect(tr).to have_css("a", text: "3m")
      expect(tr).to have_css("a", text: "5m")
      click_link '30s'
      tr = page.find('#user_settings tbody tr', text: 'Edit Lock Warning')
      expect(tr).to have_css("a", text: "30s")
      click_link '1m 30s'
      tr = page.find('#user_settings tbody tr', text: 'Edit Lock Warning')
      expect(tr).to have_css("a", text: "1m 30s")
      click_link '2m'
      tr = page.find('#user_settings tbody tr', text: 'Edit Lock Warning')
      expect(tr).to have_css("a", text: "2m")
      click_link '3m'
      tr = page.find('#user_settings tbody tr', text: 'Edit Lock Warning')
      expect(tr).to have_css("a", text: "3m")
      click_link '5m'
      tr = page.find('#user_settings tbody tr', text: 'Edit Lock Warning')
      expect(tr).to have_css("a", text: "5m")
      click_link '1m'
      tr = page.find('#user_settings tbody tr', text: 'Edit Lock Warning')
      expect(tr).to have_css("a", text: "1m")
      click_logoff
    end

    it "allows display user name to be amended", js: true do
      login_reader
      click_link 'settings_button'
      expect(page).to have_content 'Current Settings: A Reader (read@assero.co.uk)'
      expect(page).to have_content 'A Reader [Read]'
      tr = page.find('#user_settings tbody tr', text: 'Display User Name')
      expect(tr).to have_css("a", text: "Yes")
      expect(tr).to have_css("a", text: "No")
      find(:xpath, "//tr[contains(.,'Display User Name')]/td/a", :text => 'No').click
      expect(page).to have_no_content 'A Reader [Read]'
      expect(page).to have_content '[Read]'
      tr = page.find('#user_settings tbody tr', text: 'Display User Name')
      expect(tr).to have_css("a", text: "No")
      find(:xpath, "//tr[contains(.,'Display User Name')]/td/a", :text => 'Yes').click
      tr = page.find('#user_settings tbody tr', text: 'Display User Name')
      expect(tr).to have_css("a", text: "Yes")
      expect(page).to have_content 'A Reader [Read]'
      click_logoff
    end

    it "allows display user role to be amended", js: true do
      login_reader
      click_link 'settings_button'
      expect(page).to have_content 'Current Settings: A Reader (read@assero.co.uk)'
      expect(page).to have_content 'A Reader [Read]'
      tr = page.find('#user_settings tbody tr', text: 'Display User Roles')
      expect(tr).to have_css("a", text: "Yes")
      find(:xpath, "//tr[contains(.,'Display User Roles')]/td/a", :text => 'No').click
      expect(page).to have_no_content 'A Reader [Read]'
      expect(page).to have_content 'A Reader'
      tr = page.find('#user_settings tbody tr', text: 'Display User Roles')
      expect(tr).to have_css("a", text: "No")
      find(:xpath, "//tr[contains(.,'Display User Roles')]/td/a", :text => 'Yes').click
      tr = page.find('#user_settings tbody tr', text: 'Display User Roles')
      expect(tr).to have_css("a", text: "Yes")
      expect(page).to have_content 'A Reader [Read]'
      click_logoff
    end

    it "settings are user specific", js: true do
      login_reader
      click_link 'settings_button'
      expect(page).to have_content 'Current Settings:'
      tr = page.find('#user_settings tbody tr', text: 'Table Rows')
      expect(tr).to have_css("a", text: "10")
      click_link '50'
      expect(tr).to have_css("a", text: "50")
      click_logoff
      login_editor
      click_link 'settings_button'
      expect(page).to have_content 'Current Settings:'
      tr = page.find('#user_settings tbody tr', text: 'Table Rows')
      expect(tr).to have_css("a", text: "10")
      click_link '25'
      expect(tr).to have_css("a", text: "25")
      click_logoff
      login_reader
      click_link 'settings_button'
      expect(page).to have_content 'Current Settings:'
      tr = page.find('#user_settings tbody tr', text: 'Table Rows')
      expect(tr).to have_css("a", text: "50")
      click_logoff
      login_editor
      click_link 'settings_button'
      expect(page).to have_content 'Current Settings:'
      expect(tr).to have_css("a", text: "25")
      click_logoff
    end

  end

end