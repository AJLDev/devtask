require 'rails_helper'

describe "S9 Study Detail", :type => :feature do
  
  include PauseHelpers
  include UiHelpers
  include UserAccountHelpers
  include AuditHelpers
  include WaitForAjaxHelper

  before :all do
    Study.destroy_all
    Token.delete_all
    audit_clear
    ua_create
  end

  after :all do
    Study.destroy_all
    Token.delete_all
    audit_clear
    ua_destroy
  end
    
  describe "Edit User", :type => :feature do

    it "study version edit, tabs", js: true do

    	set_screen_size(800, 800)
      login_editor
      visit '/'
      expect(page).to have_content 'Studies'
      create_study("TEST", "A test study")
      table_click_link("main", "TEST", "History")
      expect(page).to have_content 'Study History: A test study'
      table_click_link("main", "0.1.0", "Edit")
      
      mini_study

      detail_tab_click
      expect(page).to have_content 'Study Detail'
      
      click_logoff
    end

  end

end