require 'rails_helper'

describe "S5 Locks", :type => :feature do
  
  include PauseHelpers
  include UiHelpers
  include BrowserSessionHelpers
  include UserAccountHelpers

  before :all do
    Token.delete_all
    user1 = User.create :email => "token_user_1@assero.co.uk", :password => "Ma12345678%", :name => "Token One"
    user1.access_level = User.access_levels[:edit]
    user1.save
    user2 = User.create :email => "token_user_2@assero.co.uk", :password => "Ma12345678%", :name => "Token Two"
    user2.access_level = User.access_levels[:edit]
    user2.save
  end

  after :all do
    user = User.where(:email => "token_user_1@assero.co.uk").first
    user.destroy
    user = User.where(:email => "token_user_2@assero.co.uk").first
    user.destroy
  end

  describe "Curator User", :type => :feature do

    it "locks a study" do

      in_browser(:one) do
        login_user('token_user_1@assero.co.uk', "Ma12345678%" )
        visit '/'
      #save_and_open_page
        create_study("TEST", "A test study")
        find(:xpath, "//tr[contains(.,'TEST')]/td/a", :text => 'History').click
        expect(page).to have_content 'Study History'
        find(:xpath, "//tr[contains(.,'0.1.0')]/td/a", :text => 'Edit').click
        expect(page).to have_content 'Study Information'
      end

      in_browser(:two) do
        login_user('token_user_2@assero.co.uk', "Ma12345678%" )
        visit '/'
        find(:xpath, "//tr[contains(.,'TEST')]/td/a", :text => 'History').click
        expect(page).to have_content 'Study History'
        find(:xpath, "//tr[contains(.,'0.1.0')]/td/a", :text => 'Edit').click
        expect(page).to have_content 'The study is locked for editing by another user'
      end

    end

  end

end