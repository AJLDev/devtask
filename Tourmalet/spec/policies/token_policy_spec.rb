require 'rails_helper'

describe TokenPolicy do

  include UserAccountHelpers
  include PermissionsHelpers

  subject { described_class.new(user, token) }
  let (:token) { Token.new }

  before :each do
    ua_create
  end

  after :each do
    ua_destroy
  end

  context "for read access" do

    let (:user) { @user_r }

    it "allows access" do
      allow_list [:release, :status, :extend_token]
    end

    it "deny access" do
      deny_list [:index]
    end

  end

  context "for edit access" do

    let (:user) { @user_e }

    it "allows access" do
      allow_list [:release, :status, :extend_token]
    end

    it "deny access" do
      deny_list [:index]
    end

  end

  context "for system admin access" do
  
    let (:user) { @user_sa }

    it "allows access" do
      allow_list [:index, :release, :status, :extend_token]
    end

  end

end