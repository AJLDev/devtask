require 'rails_helper'

describe Mdr::FormsPolicy do

  include UserAccountHelpers
  include PermissionsHelpers

  subject { described_class.new(user, mdr) }
  let (:mdr) { Mdr.new }

  before :each do
    ua_create
  end

  after :each do
    ua_destroy
  end

  context "for read access" do

    let (:user) { @user_r }

    it "allows access" do
      allow_list [:history, :latest]
    end

  end

  context "for edit access" do

    let (:user) { @user_e }

    it "allows access" do
      allow_list [:history, :latest]
    end

  end

  context "for system admin access" do
  
    let (:user) { @user_sa }

    it "allows access" do
      allow_list [:history, :latest]
    end

  end

end