require 'rails_helper'

describe UserPolicy do

  include UserAccountHelpers
  include PermissionsHelpers

  subject { described_class.new(user, user_setting) }
  let (:user_setting ) { UserSetting.new }

  before :each do
    ua_create
  end

  after :each do
    ua_destroy
  end

  context "for read access" do

    let (:user) { @user_r }

    it "allows access" do
      allow_list [:update_name]
    end

    it "deny access" do
      deny_list [:set_read, :set_edit, :set_system_admin, :index, :show, :new, :create, :edit, :destroy]
    end

  end

  context "for edit access" do

    let (:user) { @user_e }

    it "allows access" do
      allow_list [:update_name]
    end

    it "deny access" do
      deny_list [:set_read, :set_edit, :set_system_admin, :index, :show, :new, :create, :edit, :destroy]
    end

  end

  context "for system admin access" do
  
    let (:user) { @user_sa }

    it "allows access" do
      allow_list [:set_read, :set_edit, :set_system_admin, :index, :show, :new, :update_name, :create, :edit, :destroy]
    end

  end

end