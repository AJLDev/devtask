require 'rails_helper'

describe FormPolicy do

  include UserAccountHelpers
  include PermissionsHelpers

  subject { described_class.new(user, form) }
  let (:form) { Form.new }

  before :each do
    ua_create
  end

  after :each do
    ua_destroy
  end

  context "for read access" do

    let (:user) { @user_r }

    it "allows access" do
      allow_list [:index, :show]
    end

    it "deny access" do
      deny_list [:create, :new, :update, :edit, :destroy, :move]
    end

  end

  context "for edit access" do

    let (:user) { @user_e }

    it "allows access" do
      allow_list [:index, :show, :move, :create, :new, :update, :edit, :destroy]
    end

  end

  context "for system admin access" do
  
    let (:user) { @user_sa }

    it "allows access" do
      allow_list [:index, :show, :move, :create, :new, :update, :edit, :destroy]
    end

  end

end