require 'rails_helper'

describe MarkdownEnginePolicy do

  include UserAccountHelpers
  include PermissionsHelpers

  subject { described_class.new(user, study) }
  let (:study) { Study.new }

  before :each do
    ua_create
  end

  after :each do
    ua_destroy
  end

  context "for read access" do

    let (:user) { @user_r }

    it "deny access" do
      deny_list [:create]
    end

  end

  context "for edit access" do

    let (:user) { @user_e }

    it "allows access" do
      allow_list [:create]
    end

  end

  context "for system admin access" do
  
    let (:user) { @user_sa }

    it "allows access" do
      allow_list [:create]
    end

  end

end