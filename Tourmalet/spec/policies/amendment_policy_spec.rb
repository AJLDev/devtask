require 'rails_helper'

describe AmendmentPolicy do

  include UserAccountHelpers
  include PermissionsHelpers

  subject { described_class.new(user, amendment) }
  let (:amendment ) { Amendment.new }

  before :each do
    ua_create
  end

  after :each do
    ua_destroy
  end

  context "for read access" do

    let (:user) { @user_r }

    it "allows access" do
      allow_list [:index, :show, :find]
    end

    it "deny access" do
      deny_list [:create, :new, :update, :edit, :destroy]
    end

  end

  context "for edit access" do

    let (:user) { @user_e }

    it "allows access" do
      allow_list [:index, :show, :find, :create, :new, :update, :edit, :destroy]
    end

    #it "deny access" do
    #  deny_list [:set_read, :set_edit, :set_system_admin, :index, :show, :new, :create, :edit, :destroy]
    #end

  end

  context "for system admin access" do
  
    let (:user) { @user_sa }

    it "allows access" do
      allow_list [:index, :show, :find, :create, :new, :update, :edit, :destroy]
    end

  end

end