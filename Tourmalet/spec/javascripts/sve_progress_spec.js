//= require application
//= require sinon
//= require sve_progress

describe("Study Version Editor Progress", function() {
	
	beforeEach(function() {
  	html = 
  	'<div class="progress-bar" id="pb" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%;"></div>';
  	fixture.set(html);
	});

  it("initialises the object", function() {
  	var pb = new SveProgress("#pb", 10);
  	expect(pb.name).to.equal("#pb");
  	expect(pb.max).to.equal(10);
  });

 	it("clear progress", function() {
 		stub = sinon.stub(SveProgress.prototype , "show")
  	var pb = new SveProgress("#pb", 10);
  	pb.clear(20);
  	expect(pb.name).to.equal("#pb");
  	expect(pb.max).to.equal(20);
  	expect(pb.count).to.equal(0);
  	expect(stub.calledOnce).to.be.true;
  	expect(stub.getCall(0).args[0]).to.equal(0);
  	SveProgress.prototype.show.restore();
 	});

 	it("increments progress", function() {
 		stub = sinon.stub(SveProgress.prototype , "show")
  	var pb = new SveProgress("#pb", 10);
  	pb.increment();
  	expect(pb.name).to.equal("#pb");
  	expect(pb.max).to.equal(10);
  	expect(pb.count).to.equal(1);
  	expect(stub.calledOnce).to.be.true;
  	expect(stub.getCall(0).args[0]).to.equal(10);
  	SveProgress.prototype.show.restore();
 	});

 	it("show progress", function() {
 		var pb = new SveProgress("#pb", 10);
  	pb.show(10);
  	expect($("#pb").html()).to.have.string("10%");
  	// @todo Sort this check out.
  	//.css('width', p + '%').attr('aria-valuenow', p); 
  	//expect($("#pb").css('width')).to.have.string("10%");
 	});

});