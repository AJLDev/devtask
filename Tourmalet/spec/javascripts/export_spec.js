//= require application
//= require sinon
//= require export

describe("Export Study Version", function() {

    beforeEach(function() {
        html = "<form id='preview_form' action='/study_versions/export_acrf?format=pdf' accept-charset='UTF-8' method='post'><div id='waiting'><input name='utf8' type='hidden' value='✓'><input type='hidden' name='authenticity_token' value='HFFcGDB6NsN17Rz9N+z63cz+dowOj6KmZdCcprkrnkyOPn/3kYDnJnjTpsO7ng4N6PO1vWlENmiIGrpAxb4jVg=='><input type='hidden' name='id' value='4'><input type='hidden' name='table_data' value='<table id=&quot;soa_table&quot; class=&quot;table table-striped table-bordered table-condensed soa-table&quot;><thead><tr><th>SoA</th><th id=&quot;150&quot; class=&quot;soa-column text-center&quot;>V1</th><th id=&quot;151&quot; class=&quot;soa-column text-center&quot;>V2</th><th id=&quot;152&quot; class=&quot;soa-column text-center&quot;>V3</th><th id=&quot;153&quot; class=&quot;soa-column text-center&quot;>V4</th><th id=&quot;154&quot; class=&quot;soa-column text-center&quot;>V5</th><th id=&quot;155&quot; class=&quot;soa-column text-center&quot;>V6</th><th id=&quot;156&quot; class=&quot;soa-column text-center success&quot;>V7</th></tr></thead><tbody><tr><td class=&quot;soa-row success&quot; id=&quot;32&quot;>Demographics (Pilot)</td><td class=&quot;soa-element&quot; form_id=&quot;32&quot; visit_id=&quot;150&quot; id=&quot;0&quot;></td><td class=&quot;soa-element&quot; form_id=&quot;32&quot; visit_id=&quot;151&quot; id=&quot;0&quot;></td><td class=&quot;soa-element&quot; form_id=&quot;32&quot; visit_id=&quot;152&quot; id=&quot;0&quot;></td><td class=&quot;soa-element&quot; form_id=&quot;32&quot; visit_id=&quot;153&quot; id=&quot;0&quot;></td><td class=&quot;soa-element&quot; form_id=&quot;32&quot; visit_id=&quot;154&quot; id=&quot;0&quot;></td><td class=&quot;soa-element&quot; form_id=&quot;32&quot; visit_id=&quot;155&quot; id=&quot;0&quot;></td><td class=&quot;soa-element&quot; form_id=&quot;32&quot; visit_id=&quot;156&quot; id=&quot;0&quot;></td></tr></tbody></table>'><input type='text' name='study_version[export_type]' value='crf'><input type='submit' value='Submit'><input type='hidden' name='authenticity_token' id='authenticity_token' value='HFFcGDB6NsN17Rz9N+z63cz+dowOj6KmZdCcprkrnkyOPn/3kYDnJnjTpsO7ng4N6PO1vWlENmiIGrpAxb4jVg=='><%= javascript_tag 'var export_lock_id = 1' %></form>"
        fixture.set(html);
        server = sinon.fakeServer.create();
    });

    afterEach(function() {
        server.restore();
    })


    it("initialises the object", function() {
        expect($("form").attr("id")).to.equal("preview_form");
        expect($("form").attr("action")).to.have.string("export_acrf");
    });

    it("get status", function() {
        stub_status = sinon.stub(window, "getStatus");
        getStatus(1);
        server.respond();
        expect(stub_status.calledOnce).to.be.true;
    });

    it("export acrf", function() {
        var export_lock_id = 1;
        stub_status.returns(200, { "Content-Type": "application/json" })
        statusTimeout(export_lock_id, 1000)
        server.respond
        expect(stub_status.calledOnce).to.be.true;
    });

});