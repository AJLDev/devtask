//= require application
//= require sinon
//= require sve_progress
//= require sve_crf

describe("Study Version CRF", function() {

    beforeEach(function() {
        html =
            '<div class="progress-bar" id="crfPb" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%;"></div>';
        html += '<div id="crfHtml"></div>';
        fixture.set(html);
        server = sinon.fakeServer.create();
    });

    afterEach(function() {
        server.restore();
    })

    it("initialises the object", function() {
        var crf = new SveCrf(10);
        expect(crf.studyVersionId).to.equal(10);
        expect(crf.html).to.eql([]);
    });

    it("start CRF process", function() {
        var stub_3_return = "<b>Return</b>";
        stub_1 = sinon.stub(SveCrf.prototype, "clear")
        stub_2 = sinon.stub(SveCrf.prototype, "getForm")
        stub_3 = sinon.stub(SveCrf.prototype, "placeholder")
        stub_3.returns(stub_3_return)
        var crf = new SveCrf(5);
        data = {
            data: {
                children: [{
                    id: 16,
                    form_id: "F-ACME_AEPI103",
                    form_namespace: "http://www.assero.co.uk/MDRForms/ACME/V1",
                    identifier: "AE1 01",
                    ordinal: 1,
                    label: "Form Label"
                }],
                description: "To be set",
                id: 5,
                identifier: "DEMO 2",
                name: "2nd Demonstration",
                protocol_name: "To be set",
                semantic_version: "0.1.0"
            }
        };
        server.respondWith("GET", "/study_versions/5", [
            200, { "Content-Type": "application/json" },
            JSON.stringify(data)
        ]);
        crf.start();
        server.respond();
        expect(stub_1.calledOnce).to.be.true;
        expect(stub_2.calledOnce).to.be.true;
        expect(stub_3.calledOnce).to.be.true;
        //expect(stub_2.getCall(0).args[0]).to.equal(data.data.children[0]);
        expect(stub_2.getCall(0).args[0].id).to.equal(data.data.children[0].id);
        expect(stub_2.getCall(0).args[1]).to.equal(0);
        expect(stub_3.getCall(0).args[1]).to.equal("Form Label");
        expect($("#crfHtml").html()).to.have.string(stub_3_return);
        stub_1.restore();
        stub_2.restore();
        stub_3.restore();
    });

    it("start CRF process, no forms", function() {
        stub_1 = sinon.stub(SveCrf.prototype, "clear")
        stub_2 = sinon.stub(window, 'displayWarning');
        var crf = new SveCrf(5);
        data = {
            data: {
                children: [],
                description: "To be set",
                id: 5,
                identifier: "DEMO 2",
                name: "2nd Demonstration",
                protocol_name: "To be set",
                semantic_version: "0.1.0"
            }
        };
        server.respondWith("GET", "/study_versions/5", [
            200, { "Content-Type": "application/json" },
            JSON.stringify(data)
        ]);
        crf.start();
        server.respond();
        expect(stub_1.calledOnce).to.be.true;
        expect(stub_2.calledOnce).to.be.true;
        expect(stub_2.getCall(0).args[0]).to.equal("Study does not include any forms at present, nothing to display.");
        SveCrf.prototype.clear.restore();
        stub_2.restore();
    });

    it("get a form", function() {
        stub_1 = sinon.stub(SveCrf.prototype, "displayForm")
        var crf = new SveCrf(5);
        data = "<b>Bold</b>";
        server.respondWith("GET", "/mdrs/form_show?id=A&namespace=http%3A%2F%2Fwww.example.com", [200, { "Content-Type": "html" }, data]);
        crf.getForm({ form_id: "A", form_namespace: "http://www.example.com" }, 2);
        server.respond();
        expect(stub_1.calledOnce).to.be.true;
        expect(stub_1.getCall(0).args[0]).to.equal(data);
        expect(stub_1.getCall(0).args[1]).to.equal(2);
        SveCrf.prototype.displayForm.restore();
    });

    it("display a form", function() {
        stub = sinon.stub(SveProgress.prototype, "increment")
        var crf = new SveCrf(5);
        crf.html = ["", "", "", "", ""]
        crf.displayForm("<b>Form</b>", 3);
        expect(stub.calledOnce).to.be.true;
        expect(crf.html).to.eql(["", "", "", "<b>Form</b>", ""]);
        expect($("#crfHtml").html()).to.have.string("<b>Form</b>");
        SveProgress.prototype.increment.restore();
    });

    it("display a form, multiple forms", function() {
        stub = sinon.stub(SveProgress.prototype, "increment")
        var crf = new SveCrf(5);
        crf.html = ["", "", "", "", "", ""]
        crf.displayForm("<b>Form 1</b>", 1);
        crf.displayForm("<b>Form 3</b>", 3);
        expect(stub.calledTwice).to.be.true;
        expect(crf.html).to.eql(["", "<b>Form 1</b>", "", "<b>Form 3</b>", "", ""]);
        expect($("#crfHtml").html()).to.have.string("<b>Form 1</b><b>Form 3</b>");
        SveProgress.prototype.increment.restore();
    });

    it("clears the display", function() {
        stub = sinon.stub(SveProgress.prototype, "clear")
        var crf = new SveCrf(5);
        crf.clear();
        expect(stub.calledOnce).to.be.true;
        expect(stub.getCall(0).args[0]).to.equal(0);
        expect($("#crfHtml").html()).to.have.string("");
        SveProgress.prototype.clear.restore();
    });

    it("creates the placeholder html", function() {
        var expected = '<div class="row spinner_and_label_1"><div class="col-md-3 col-sm-4"><p><i class="fa fa-spinner fa-spin fa-lg fa-fw margin-bottom"></i>' +
            '</p></div><div class="col-md-9 col-sm-8"><p>Form: Name will appear here ...</p></div></div>';
        var crf = new SveCrf(5);
        result = crf.placeholder(1, "Name");
        expect(result).to.equal(expected);
    });

});