module UserAccountHelpers

  def ua_create
  	@user_r = User.create :email => "read@assero.co.uk", :password => "Changeme1%", :name => "A Reader"
    @user_e = User.create :email => "edit@assero.co.uk", :password => "Changeme1%", :name => "A Editor" 
    @user_e.access_level = User.access_levels[:edit]
    @user_e.save
    @user_sa = User.create :email => "system_admin@assero.co.uk", :password => "Changeme1%", :name => "A Admin" 
    @user_sa.access_level = User.access_levels[:system_admin]
    @user_sa.save
  end

  def ua_destroy
    user = User.where(:email => "read@assero.co.uk").first
    user.destroy
    user = User.where(:email => "edit@assero.co.uk").first
    user.destroy
    user = User.where(:email => "system_admin@assero.co.uk").first
    user.destroy
  end

  def login_with_read_access
    #before(:each) do
      @request.env["devise.mapping"] = Devise.mappings[:user]
      sign_in @user_r
    #end
  end

  def login_with_edit_access
    @request.env["devise.mapping"] = Devise.mappings[:user]
    sign_in @user_e
  end

  def login_with_system_admin_access
    @request.env["devise.mapping"] = Devise.mappings[:user]
    sign_in @user_sa
  end

  def reader_email
    return @user_r.email
  end

  def login_reader
    login_user("read@assero.co.uk", "Changeme1%")
  end

  def login_editor
    login_user("edit@assero.co.uk", "Changeme1%")
  end
  
  def login_system_admin
    login_user("system_admin@assero.co.uk", "Changeme1%")
  end
  
  def login_user(username, password)
    visit '/users/sign_in'
    fill_in 'Email', with: username
    fill_in 'Password', with: password
    click_login 
    expect(page).to have_content 'Signed in successfully'
  end

  def create_user(email, password, name)
    click_link 'New'
    expect(page).to have_content 'New User'
    fill_in 'Email', with: email
    fill_in 'Display Name', with: name
    fill_in 'Password:', with: password
    fill_in 'Password Confirmation:', with: password
    click_button 'Create'
  end

end