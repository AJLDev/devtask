module UiHelpers

  def click_logoff
    click_link 'logoff_button'
  end

  def click_login
    click_button 'Log in'
  end

  def click_new_study
    click_link "new_study_button"
  end

  def click_settings
    click_link 'settings_button'
  end

  def click_audit
    click_link 'audit_button'
  end

  def click_edit_locks
    click_link 'lock_button'
  end

  def click_users
    click_link 'users_button'
  end

  def check_audit_present
    check_link_present 'audit_button'
  end

  def check_audit_not_present
    check_link_not_present 'audit_button'
  end

  def check_edit_lock_present
    check_link_present 'lock_button'
  end

  def check_edit_lock_not_present
    check_link_not_present 'lock_button'
  end

  def check_users_present
    check_link_present 'users_button'
  end

  def check_users_not_present
    check_link_not_present 'users_button'
  end

  def check_print_present
    check_link_present 'print_button'
  end

  def check_settings_present
    check_link_present 'settings_button'
  end

  def check_home_present
    check_link_present 'home_button'
  end

  def check_logoff_present
    check_link_present 'logoff_button'
  end

  def check_link_present(link_id)
    expect(page).to have_link link_id
  end

  def check_link_not_present(link_id)
    expect(page).to have_no_link link_id
  end

  def check_button_disabled(button_id)
    expect(page).to have_button("#{button_id}", disabled: true)
  end

  def check_button_enabled(button_id)
    expect(page).to have_button("#{button_id}", disabled: false)
  end

  # Pop-up Alerts
  def alert_click_ok(text="")
    a = page.driver.browser.switch_to.alert
    expect(a.text).to eq(text) if !text.empty?
    a.accept
  end

  def alert_click_cancel(text="")
    a = page.driver.browser.switch_to.alert
    expect(a.text).to eq(text) if !text.empty?
    a.dismiss
  end

  # Table functions
  def table_row_link_click(table_id, content, link_text)
    find(:xpath, "//table[@id='#{table_id}']/tbody/tr[contains(.,'#{content}')]/td/a", :text => link_text).click
  end

  def table_row_click(table_id, content)
    within "##{table_id}" do
      find('td', :text => "#{content}").click
    end
  end

  def table_get_row(table_id, col, text)
    row = 0
    page.all("##{table_id} tbody tr").each do |tr|
      row += 1
      cell_text = find(:xpath, "//*[@id=\"#{table_id}\"]/tbody/tr[#{row}]/td[#{col}]").text
      return row if cell_text == text
    end
    return -1
  end

  def table_check_cell(table_id, row, col, text)
    cell = find(:xpath, "//table[@id='#{table_id}']/tbody/tr[#{row}]/td[#{col}]").text
    expect(cell).to eq(text)
  end

  def table_check_cell_exists(table_id, col, text)
    row = table_get_row(table_id, col, text)
    table_check_cell(table_id, row, col, text)
  end

  def table_check_row(table_id, row, data)
    page.all("table##{table_id} tbody tr:nth-child(#{row}) td").each_with_index do |td, index|
      if index < data.length
        expect(td.text).to eq(data[index]) if !data[index].nil?
      end
    end
  end

  def table_click_link(table_id, key_text, link_text)
    find(:xpath, "//table[@id='#{table_id}']/tbody/tr[contains(.,'#{key_text}')]/td/a", :text => "#{link_text}").click
  end

  def table_check_row_count(table_id, count)
    expect(page.all("table##{table_id} tbody tr").count).to eq(count)
  end

  # Tabs
  def study_tab_click
    tab_click("study")
  end

  def study_tab_present
    tab_present("Study Information")
  end

  def study_tab_not_present
    tab_not_present("Study Information")
  end

  def soa_tab_click
    tab_click("soa")
  end

  def export_acrf_click
    click_link "export_acrf"
  end

  def soa_tab_present
    tab_present("Schedule of Assessments")
  end

  def soa_tab_not_present
    tab_not_present("Schedule of Assessments")
  end

  def detail_tab_click
    tab_click("detail")
  end

  def detail_tab_present
    tab_present("Study Detail")
  end

  def detail_tab_not_present
    tab_not_present("Study Detail")
  end

  def crf_tab_click
    tab_click("crf")
  end

  def crf_tab_present
    tab_present("Case Report Form")
  end

  def crf_tab_not_present
    tab_not_present("Case Report Form")
  end

  def acrf_tab_click
    tab_click("acrf")
  end

  def acrf_tab_present
    tab_present("Annotated CRF")
  end

  def acrf_tab_not_present
    tab_not_present("Annotated CRF")
  end

  def tab_present(text)
    expect(page).to have_selector('a', text: "#{text}", visible: true)
    #page.should have_no_selector(:xpath, "//input[@type='#{type}' and @name='#{name}']")
  end

  def tab_not_present(text)
    expect(page).to have_no_selector('a', text: "#{text}", visible: false)
  end

  def tab_click(tab_href)
    find(:xpath, "//li[@role='presentation']/a[@href='##{tab_href}']").click
  end

  # Study Edit Buttons
  def click_visit_add
    click_button 'soa_add_visit'
  end

  def check_visit_add_disabled
    check_button_disabled 'soa_add_visit'
  end

  def check_visit_add_enabled
    check_button_enabled 'soa_add_visit'
  end

  def click_visit_delete
    click_button 'soa_delete_visit'
  end

  def check_visit_delete_disabled
    check_button_disabled 'soa_delete_visit'
  end

  def check_visit_delete_enabled
    check_button_enabled 'soa_delete_visit'
  end

  def click_visit_left
    click_button 'soa_visit_left'
  end

  def check_visit_left_disabled
    check_button_disabled 'soa_visit_left'
  end

  def check_visit_left_enabled
    check_button_enabled 'soa_visit_left'
  end

  def click_visit_right
    click_button 'soa_visit_right'
  end

  def check_visit_right_disabled
    check_button_disabled 'soa_visit_right'
  end

  def check_visit_right_enabled
    check_button_enabled 'soa_visit_right'
  end

  def click_visit_update
    click_button 'soa_update_visit'
  end

  def click_visit_delete
    click_button 'soa_delete_visit'
  end

  def click_form_add
    script = "$('button[id=\"imis_add_button\"]').click()"
    page.execute_script(script)
  end

  def click_form_toggle
    script = "$('button[id=\"imis_toggle_button\"]').click()"
    page.execute_script(script)
  end

  def check_form_add_disabled
    check_button_disabled 'imis_add_button'
  end

  def check_form_add_enabled
    check_button_enabled 'imis_add_button'
  end

  def click_form_delete
    click_button 'soa_delete_form'
  end

  def check_form_delete_disabled
    check_button_disabled 'soa_delete_form'
  end

  def check_form_delete_enabled
    check_button_enabled 'soa_delete_form'
  end

  def click_form_up
    click_button 'soa_form_up'
  end

  def check_form_up_disabled
    check_button_disabled 'soa_form_up'
  end

  def check_form_up_enabled
    check_button_enabled 'soa_form_up'
  end

  def click_form_down
    click_button 'soa_form_down'
  end

  def check_form_down_disabled
    check_button_disabled 'soa_form_down'
  end

  def check_form_down_enabled
    check_button_enabled 'soa_form_down'
  end

  def click_element_clear
    click_button 'soa_element_clear'
  end

  def check_element_clear_disabled
    check_button_disabled 'soa_element_clear'
  end

  def check_element_clear_enabled
    check_button_enabled 'soa_element_clear'
  end

  def click_study_clear
    click_button 'soa_study_version_clear'
  end

  def check_study_clear_disabled
    check_button_disabled 'soa_study_version_clear'
  end

  def check_study_clear_enabled
    check_button_enabled 'soa_study_version_clear'
  end


  # SOA Table
  def soa_check_visit(col, text)
    cell = find(:xpath, "//table[@id='soa_table']/thead/tr/th[#{col}]").text
    expect(cell).to eq(text)
  end

  def soa_click_visit(col)
    find(:xpath, "//table[@id='soa_table']/thead/tr/th[#{col}]").click
  end

  def soa_check_form(row, text)
    cell = find(:xpath, "//table[@id='soa_table']/tbody/tr[#{row}]/td[1]").text
    expect(cell).to eq(text)
  end

  def soa_click_form(row)
    find(:xpath, "//table[@id='soa_table']/tbody/tr[#{row}]/td[1]").click
  end

  def soa_click_element(row, col)
    find(:xpath, "//table[@id='soa_table']/tbody/tr[#{row}]/td[#{col}]").click
  end

  def soa_check_element(row, col, state)
    element = find(:xpath, "//table[@id='soa_table']/tbody/tr[#{row}]/td[#{col}]")
    if state
      expect(element[:id]).not_to eq("0")
    else
      expect(element[:id]).to eq("0")
    end
  end

  # Form Released Table
  def form_index_check_form(row, col, text)
    cell = find(:xpath, "//table[@id='imis_index_table']/tbody/tr[#{row}]/td[#{col}]").text
    expect(cell).to eq(text)
  end

  def form_index_click_identifier(text)
    script = "$('table[id=\"imis_index_table\"] tbody tr:contains(\"#{text}\")').click()"
    page.execute_script(script)
  end

  # Form UnReleased Table
  def form_history_check_form(row, col, text)
    cell = find(:xpath, "//table[@id='imis_history_table']/tbody/tr[#{row}]/td[#{col}]").text
    expect(cell).to eq(text)
  end

  def form_history_click_version(text)
    script = "$('table[id=\"imis_history_table\"] tbody tr:contains(\"#{text}\")').click()"
    page.execute_script(script)
  end

  # Input Fields
  def fill_input(input_id, value)
    fill_in input_id, with: value
  end

  def check_input(input_id, value)
    expect(find_field("#{input_id}").value).to eq "#{value}"
  end

  # Perform action functions
  def create_study(identifier, label)
    click_new_study
    expect(page).to have_content 'New Study'
    fill_in 'study_identifier', with: identifier
    fill_in 'study_label', with: label
    click_button 'create_button'
  end

  # Screen Size
  def set_screen_size(width=1200, height=786)
    page.driver.browser.manage.window.resize_to(width, height)
  end

  # Scroll
  def scroll_to_id(id)
    page.execute_script("document.getElementById(#{id}).scrollTop += 100")
  end

  def scroll_to_top()
    page.execute_script("window.scrollTo(0, 0);")
  end

  # Mini study
  def mini_study
    soa_tab_click
    click_visit_add
    soa_check_visit(2, "V1")
    click_visit_add
    soa_check_visit(3, "V2")
    click_visit_add
    soa_check_visit(4, "V3")
    # Selec EQ-5D-3L
    form_index_click_identifier "EQ 5D 3L"
    click_form_add
    wait_for_ajax
    soa_check_form(1, "EQ 5D 3 Level")
    # Selec MH1 01
    form_index_click_identifier "MH1 01"
    click_form_add
    wait_for_ajax
    soa_check_form(2, "Medical History")
    # Select DM1 01
    form_index_click_identifier "DM1 01"
    click_form_toggle # Toggle to form history
    wait_for_ajax
    form_history_click_version "0.0.0"
    click_form_add
    wait_for_ajax
    soa_check_form(3, "Demographics")
    # Select AE NEW
    click_form_toggle # Toggle to form index
    form_index_click_identifier "AE NEW"
    click_form_toggle # Toggle to form history
    wait_for_ajax
    form_history_click_version "0.1.0"
    click_form_add
    wait_for_ajax
    soa_check_form(4, "Adverse Events")
    scroll_to_top
    soa_click_form(1)
    soa_click_element(1, 2)
    wait_for_ajax
    soa_click_element(3, 3)
    wait_for_ajax
    soa_click_element(2, 2)
    wait_for_ajax
    soa_click_element(4, 4)
    wait_for_ajax
    soa_check_element(1, 2, true)
    soa_check_element(1, 3, false)
    soa_check_element(1, 4, false)
    soa_check_element(2, 2, true)
    soa_check_element(2, 3, false)
    soa_check_element(2, 4, false)
    soa_check_element(3, 2, false)
    soa_check_element(3, 3, true)
    soa_check_element(3, 4, false)
    soa_check_element(4, 2, false)
    soa_check_element(4, 3, false)
    soa_check_element(4, 4, true)
  end

end
