module AuditHelpers

	def audit_clear
		AuditTrail.destroy_all
	end

	def audit_base
		@audit_count = AuditTrail.count
	end

	def audit_check_inc(new_count)
		expect(AuditTrail.count).to eq(@audit_count + new_count)
	end

end