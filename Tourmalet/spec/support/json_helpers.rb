module JsonHelpers

  def remove_id_and_timestamps(response)
    hash = JSON.parse(response.body, symbolize_names: true)
    new_hash = { data: [] }
    hash[:data].each do |item|
      inner_hash = JSON.parse(item, symbolize_names: true)
      inner_hash.except!(:id, :created_at, :updated_at)
      new_hash[:data] << inner_hash
    end
    return new_hash
  end

  def api_remove_id(response)
    hash = JSON.parse(response.body, symbolize_names: true)
    new_hash = []
    hash.each do |item|
      inner_hash = item.except(:id)
      new_hash << inner_hash
    end
    return new_hash
  end

end