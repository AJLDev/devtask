module FileHelpers

  def read_yaml_file(sub_dir, filename)
    full_path = set_path(sub_dir, filename)
    return YAML.load_file(full_path)
  end

  def write_yaml_file(item, sub_dir, filename)
    full_path = set_path(sub_dir, filename)
    File.open(full_path, "w+") do |f|
      f.write(item.to_yaml)
    end
  end

  def read_text_file(sub_dir, filename)
    text = ""
    full_path = set_path(sub_dir, filename)
    File.open(full_path, "r") do |f|
      text = f.read
    end
    return text
  end

  def write_text_file(item, sub_dir, filename)
    full_path = set_path(sub_dir, filename)
    File.open(full_path, "w+") do |f|
      f.write(item)
    end
  end

  def test_file_path(sub_dir, filename)
    return set_path(sub_dir, filename)
  end
  
  def test_file_partial_path(sub_dir, filename)
    return partial_path(sub_dir, filename)
  end

  def test_delete_file(sub_dir, filename)
    file = set_path(sub_dir, filename)
    File.delete(file)
  rescue => e
  end

  def test_copy_file(sub_dir, source_filename, dest_filename)
    source_file = set_path(sub_dir, source_filename)
    dest_file = set_path(sub_dir, dest_filename)
    FileUtils.cp source_file, dest_file
  end

private

  def set_path(sub_dir, filename)
    return Rails.root.join partial_path(sub_dir, filename)
  end

  def partial_path(sub_dir, filename)
    return "spec/fixtures/#{sub_dir}/#{filename}"
  end

end