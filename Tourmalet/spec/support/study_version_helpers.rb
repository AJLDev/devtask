module StudyVersionHelpers

  def create_study(identifier, label)
    return Study.create(identifier: identifier, label: label)
  end

  def create_study_version(study_id, name, description, protocol_name, state, version, semantic_version)
    return StudyVersion.create(study_id: study_id, name: name, description: description, protocol_name: protocol_name,
                               state: state, version: version, semantic_version: semantic_version)
  end

  def create_form(study_version_id, form_id, form_namespace, identifier, ordinal, label)
    form = Form.create(study_version_id: study_version_id, ordinal: ordinal, form_id: form_id,
                       form_namespace: form_namespace, identifier: identifier, label: label)
    return form
  end

  def create_visit(study_version_id, ordinal, short_name, long_name, timing)
    return Visit.create(study_version_id: study_version_id, ordinal: ordinal, short_name: short_name, long_name: long_name, timing: timing)
  end

  def create_element(study_version_id, form_id, visit_id)
    return Element.create(study_version_id: study_version_id, visit_id: visit_id, form_id: form_id)
  end

  def soa_html
    "<table id=\"soa_table\" class=\"table table-striped table-bordered table-condensed soa-table\">
  <thead><tr><th>SoA</th><th id=\"14\" class=\"soa-column text-center\">V1</th><th id=\"15\" class=\"soa-column text-center\">V2</th><th id=\"16\" class=\"soa-column text-center\">V3</th><th id=\"17\" class=\"soa-column text-center success\">V4</th></tr></thead><tbody><tr><td class=\"soa-row success\" id=\"9\"> Alzheimer Disease Onset Date (Pilot)</td><td class=\"soa-element\" form_id=\"9\" visit_id=\"14\" id=\"0\"></td><td class=\"soa-element\" form_id=\"9\" visit_id=\"15\" id=\"0\"></td><td class=\"soa-element\" form_id=\"9\" visit_id=\"16\" id=\"0\"></td><td class=\"soa-element\" form_id=\"9\" visit_id=\"17\" id=\"0\"></td></tr></tbody></table>"
  end

end