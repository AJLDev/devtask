FactoryGirl.define do
  factory :audit_trail do
    date_time ""
    user "MyString"
    identifier "MyString"
    version "MyString"
    event 1
    description "MyString"
  end
end
