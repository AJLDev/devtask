FactoryGirl.define do
    
    # Define a basic devise user.
    factory :user do    
    	id 1
      email "user@example.com"
      password "example1234"
      password_confirmation "example1234"
      name "User Fred"
      access_level 0
    end

end