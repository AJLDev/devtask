FactoryGirl.define do
  factory :token do
    locked_at "MyString"
    datetime "MyString"
    refresh_count 1
    study_version_id 1
    item_info "MyString"
    user_id 1
  end
end
