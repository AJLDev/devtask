FactoryGirl.define do
  factory :study_version do
    study_id 1
    name "MyString"
    description "MyString"
    protocol_name "MyString"
    state 1
    version 1
    semantic_version "MyString"
  end
end
