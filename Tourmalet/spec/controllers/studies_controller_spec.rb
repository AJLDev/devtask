require 'rails_helper'

describe StudiesController, type: :controller do

  include UserAccountHelpers
  include FileHelpers
  include JsonHelpers
  include StudyVersionHelpers

  def sub_dir
    return "controllers"
  end

  describe "Edit Access" do

    before :all do
      ua_create
    end
    
    after :all do
      ua_destroy
    end
    
    before :each do
      login_with_edit_access
      Study.delete_all
      StudyVersion.delete_all
      AuditTrail.delete_all
    end

    after :each do
      Study.delete_all
      StudyVersion.delete_all
      AuditTrail.delete_all
    end

    it "lists studies" do
      s1 = Study.create(label: "Study 1", identifier: "STUDY 1")
      s2 = Study.create(label: "Study 2", identifier: "STUDY 2")
      s3 = Study.create(label: "Study 3", identifier: "STUDY 3")
      get :index
      result = assigns[:studies]
      expect(result.count).to eq(3)
      expect(response).to render_template("index")
    end

    it "creates an study" do
      audit_count = AuditTrail.all.count
      post :create, {study: {label: "Study 1", identifier: "STUDY 1" }}
      expect(response).to redirect_to("/studies")
      expect(AuditTrail.all.count).to eq(audit_count + 1)
    end

    it "destroys a study" do
      audit_count = AuditTrail.all.count
      s1 = Study.create(label: "Study 1", identifier: "STUDY 1")
      delete :destroy, {id: s1.id}
      expect(Study.all.count).to eq(0)
      expect(AuditTrail.all.count).to eq(audit_count + 1)
    end

    it "displays history for a study, no study version" do
      audit_count = AuditTrail.all.count
      s1 = Study.create(label: "Study 1", identifier: "STUDY 1")
      get :history, {id: s1.id}
      result_s = assigns[:study]
      result_sv = assigns[:study_versions]
      expect(result_s.id).to eq(s1.id)
      expect(result_sv.count).to eq(1)
      expect(response).to render_template("history")
      expect(AuditTrail.all.count).to eq(audit_count + 1)
    end

    it "displays histry for a study, study version" do
      s1 = Study.create(label: "Study 2", identifier: "STUDY 2")
      sv1 = create_study_version(s1.id, "V1", "description", "whatever", StudyVersion.states[:qualified], 1, "0.1.0")
      sv2 = create_study_version(s1.id, "V2", "description", "whatever", StudyVersion.states[:standard], 2, "1.0.0")
      sv3 = create_study_version(s1.id, "V3", "description", "whatever", StudyVersion.states[:incomplete], 2, "1.1.0")
      get :history, {id: s1.id}
      result_s = assigns[:study]
      result_sv = assigns[:study_versions]
      expect(result_s.id).to eq(s1.id)
      expect(result_sv.count).to eq(3)
      expect(response).to render_template("history")
    end

  end

  describe "Unauthorized User" do
    
    it "move" do
      get :index
      expect(response).to redirect_to("/users/sign_in")
    end

    it "create" do
      post :create
      expect(response).to redirect_to("/users/sign_in")
    end

    it 'destroy' do
      delete :destroy, {id: 1}
      expect(response).to redirect_to("/users/sign_in")
    end

  end

end
