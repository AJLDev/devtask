require 'rails_helper'

describe ElementsController, type: :controller do

  include UserAccountHelpers
  include FileHelpers
  include JsonHelpers

  def sub_dir
    return "controllers"
  end

  describe "Edit Access" do

    before :all do
      Element.delete_all
      ua_create
    end
    
    after :all do
      Element.delete_all
      ua_destroy
    end
    
    before :each do
      login_with_edit_access
    end

    after :each do
    end

    it "creates an element" do
      request.env['HTTP_ACCEPT'] = "application/json"
      post :create, {element: {study_version_id: 3, form_id: 6, visit_id: 5 }}
      expect(response.content_type).to eq("application/json")
      expect(response.code).to eq("200")
      expect(Element.all.count).to eq(1)
    end

    it "destroys an element" do
      e = Element.create(study_version_id: 3, form_id: 6, visit_id: 5)
      request.env['HTTP_ACCEPT'] = "application/json"
      delete :destroy, {id: e.id}
      expect(response.content_type).to eq("application/json")
      expect(response.code).to eq("200")
      expect(Element.all.count).to eq(0)
    end

    it "clears elements" do
      e = Element.create(study_version_id: 3, form_id: 6, visit_id: 4)
      e = Element.create(study_version_id: 3, form_id: 7, visit_id: 4)
      e = Element.create(study_version_id: 3, form_id: 8, visit_id: 4)
      expect(Element.all.count).to eq(3)
      request.env['HTTP_ACCEPT'] = "application/json"
      put :clear, {element: {study_version_id: 3}}
      expect(response.content_type).to eq("application/json")
      expect(response.code).to eq("200")
      expect(Element.all.count).to eq(0)
    end

  end

  describe "Unauthorized User" do
    
    it "clear" do
      put :clear
      expect(response).to redirect_to("/users/sign_in")
    end

    it "create" do
      post :create
      expect(response).to redirect_to("/users/sign_in")
    end

    it 'destroy' do
      delete :destroy, {id: 1}
      expect(response).to redirect_to("/users/sign_in")
    end

  end

end
