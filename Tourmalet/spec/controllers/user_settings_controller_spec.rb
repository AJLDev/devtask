require 'rails_helper'

describe UserSettingsController do

  include UserSettingsHelpers
  include UserAccountHelpers

  describe "user settings as reader" do
  	
    before :all do
      ua_create
    end
   
    after :all do
      ua_destroy
    end
    
    #login_with_read_access

    it "index settings" do
      login_with_read_access
      get :index
      expect(assigns(:settings_metadata)).to eq(us_expected_metadata)
      expect(response).to render_template("index")
    end

    it "updates user" do
      login_with_read_access
      user = @user_r
      put :update, {id: user.id, :user_settings => {:name => "paper_size", :value => "Letter"}}
      expect(user.read_setting(:paper_size).value).to eq("Letter")
      expect(response).to redirect_to("/user_settings")
    end

  end

  describe "Unauthorized User" do
    
    it "index user" do
      get :index
      expect(response).to redirect_to("/users/sign_in")
    end

    it 'creates user' do
      user = User.create :email => "fred@assero.co.uk", :password => "1changEme%" 
      put :update, {id: user.id, :user_settings => {:name => :paper_size, :value => "LETTER"}}
      expect(response).to redirect_to("/users/sign_in")
      user = User.where(:email => "fred@assero.co.uk").first
      user.destroy
    end

  end

end