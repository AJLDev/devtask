require 'rails_helper'

describe AmendmentsController, type: :controller do

  include UserAccountHelpers
  include FileHelpers
  include JsonHelpers

  def sub_dir
    return "controllers"
  end

  describe "Edit Access" do

    before :all do
      Amendment.delete_all
      a = Amendment.create(cid: "X1", namespace:  "http://www.example.com", property: "question", 
        study_value: "ADEF", default_value: "A1", datatype: "string")
      a = Amendment.create(cid: "X1", namespace:  "http://www.example.com", property: "enabled", 
        study_value: "true", default_value: "true", datatype: "boolean")
      a = Amendment.create(cid: "Y1", namespace:  "http://www.example.com", property: "enabled", 
        study_value: "false", default_value: "true", datatype: "boolean")
      ua_create
    end
    
    after :all do
      Amendment.delete_all
      ua_destroy
    end
    
    before :each do
      login_with_edit_access
    end

    after :each do
    end

    it "finds amendments for a given item" do
      request.env['HTTP_ACCEPT'] = "application/json"
      get :find, {amendment: {cid: "X1", namespace: "http://www.example.com"}}
      expect(response.content_type).to eq("application/json")
      expect(response.code).to eq("200")
      result = remove_id_and_timestamps(response)
    #write_yaml_file(result, sub_dir, "amendments_controller_find_1.txt")
      expected = read_yaml_file(sub_dir, "amendments_controller_find_1.txt")
      expect(result).to eq(expected)
    end
    
    it "finds amendments for a given item, none present" do
      request.env['HTTP_ACCEPT'] = "application/json"
      get :find, {amendment: {cid: "X11", namespace: "http://www.example.com"}}
      expect(response.content_type).to eq("application/json")
      expect(response.code).to eq("200")
    #write_text_file(response.body, sub_dir, "amendments_controller_find_2.txt")
      expected = read_text_file(sub_dir, "amendments_controller_find_2.txt")
      expect(response.body).to eq(expected)
    end
    
    it "creates an amendment" do
      request.env['HTTP_ACCEPT'] = "application/json"
      post :create, {amendment: {cid: "X11", namespace: "http://www.example.com", property: "question", 
        study_value: "SSS", default_value: "AAA", datatype: "string"}}
      expect(response.content_type).to eq("application/json")
      expect(response.code).to eq("200")
    end

    it "updates an amendment" do
      a = Amendment.create(cid: "X11", namespace:  "http://www.example.com", property: "question", 
        study_value: "ADEF", default_value: "A1", datatype: "string")
      request.env['HTTP_ACCEPT'] = "application/json"
      put :update, {id: a.id, amendment: {property: "question", 
        study_value: "SSS", default_value: "AAA", datatype: "string"}}
      expect(response.content_type).to eq("application/json")
      expect(response.code).to eq("200")
    #write_text_file(response.body, sub_dir, "amendments_controller_create_1.txt")
      expected = read_text_file(sub_dir, "amendments_controller_create_1.txt")
      expect(response.body).to eq(expected)
    end

    it "destroys an amendment" do
      a = Amendment.create(cid: "X11", namespace:  "http://www.example.com", property: "question", 
        study_value: "ADEF", default_value: "A1", datatype: "string")
      request.env['HTTP_ACCEPT'] = "application/json"
      delete :destroy, {id: a.id}
      expect(response.content_type).to eq("application/json")
      expect(response.code).to eq("200")
    end

  end

  describe "Unauthorized User" do
    
    it "find" do
      get :find
      expect(response).to redirect_to("/users/sign_in")
    end

    it "create" do
      post :create
      expect(response).to redirect_to("/users/sign_in")
    end

    it 'update' do
      put :update, {id: 1}
      expect(response).to redirect_to("/users/sign_in")
    end

    it 'destroy' do
      delete :destroy, {id: 1}
      expect(response).to redirect_to("/users/sign_in")
    end

  end

end
