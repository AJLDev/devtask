require 'rails_helper'

describe Mdrs::FormsController, type: :controller do

  include UserAccountHelpers
  include FileHelpers
  include JsonHelpers

  def sub_dir
    return "controllers/mdrs"
  end

  describe "Edit Access" do

    before :all do
      Study.delete_all
      ua_create
    end
    
    after :all do
      Study.delete_all
      ua_destroy
    end
    
    before :each do
      login_with_edit_access
    end

    after :each do
    end

    it "gets a forms history, I" do
    	mdr = Mdr.new
    	mdr.data = []
    	data = read_yaml_file(sub_dir, "history_1.yaml")
    	data.each {|x| mdr.data << x.deep_symbolize_keys}
    	expect(Mdr).to receive(:forms_all).and_return(mdr)
    	request.env['HTTP_ACCEPT'] = "application/json"
      get :history, {id: "DM1 01"}
      expect(response.content_type).to eq("application/json")
      expect(response.code).to eq("200")
    #write_text_file(response.body, sub_dir, "history_1_result.txt")
    	expected = read_text_file(sub_dir, "history_1_result.txt")
      expect(response.body).to eq(expected)
    end

    it "gets a forms history, II" do
    	mdr = Mdr.new
    	mdr.data = []
    	data = read_yaml_file(sub_dir, "history_2.yaml")
    	data.each {|x| mdr.data << x.deep_symbolize_keys}
    	expect(Mdr).to receive(:forms_all).and_return(mdr)
    	request.env['HTTP_ACCEPT'] = "application/json"
      get :history, {id: "DM1 01"}
      expect(response.content_type).to eq("application/json")
      expect(response.code).to eq("200")
    #write_text_file(response.body, sub_dir, "history_2_result.txt")
    	expected = read_text_file(sub_dir, "history_2_result.txt")
      expect(response.body).to eq(expected)
    end

    it "gets a forms history, III" do
    	mdr = Mdr.new
    	mdr.data = []
    	data = read_yaml_file(sub_dir, "history_2.yaml")
    	data.each {|x| mdr.data << x.deep_symbolize_keys}
    	expect(Mdr).to receive(:forms_all).and_return(mdr)
    	request.env['HTTP_ACCEPT'] = "application/json"
      get :history, {id: "DM1 01x"}
      expect(response.content_type).to eq("application/json")
      expect(response.code).to eq("200")
    #write_text_file(response.body, sub_dir, "history_3_result.txt")
    	expected = read_text_file(sub_dir, "history_3_result.txt")
      expect(response.body).to eq(expected)
    end

    it "gets a forms history, error" do
    	mdr = Mdr.new
    	mdr.data = []
    	mdr.errors.add(:base, "Error")
    	expect(Mdr).to receive(:forms_all).and_return(mdr)
    	request.env['HTTP_ACCEPT'] = "application/json"
      get :history, {id: "DM1 01"}
      expect(response.content_type).to eq("application/json")
      expect(response.code).to eq("422")
    #write_text_file(response.body, sub_dir, "history_4_result.txt")
    	expected = read_text_file(sub_dir, "history_4_result.txt")
      expect(response.body).to eq(expected)
    end

    it "gets a forms latest, I" do
    	mdr = Mdr.new
    	mdr.data = []
    	data = read_yaml_file(sub_dir, "history_1.yaml")
    	data.each {|x| mdr.data << x.deep_symbolize_keys}
    	expect(Mdr).to receive(:forms_all).and_return(mdr)
    	request.env['HTTP_ACCEPT'] = "application/json"
      get :latest, {id: "DM1 01"}
      expect(response.content_type).to eq("application/json")
      expect(response.code).to eq("404")
    #write_text_file(response.body, sub_dir, "latest_1_result.txt")
    	expected = read_text_file(sub_dir, "latest_1_result.txt")
      expect(response.body).to eq(expected)
    end

    it "gets a forms latest, II" do
    	mdr = Mdr.new
    	mdr.data = []
    	data = read_yaml_file(sub_dir, "history_2.yaml")
    	data.each {|x| mdr.data << x.deep_symbolize_keys}
    	expect(Mdr).to receive(:forms_all).and_return(mdr)
    	request.env['HTTP_ACCEPT'] = "application/json"
      get :latest, {id: "DM1 01"}
      expect(response.content_type).to eq("application/json")
      expect(response.code).to eq("200")
    write_text_file(response.body, sub_dir, "latest_2_result.txt")
    	expected = read_text_file(sub_dir, "latest_2_result.txt")
      expect(response.body).to eq(expected)
    end

    it "gets a forms latest, error" do
    	mdr = Mdr.new
    	mdr.data = []
    	mdr.errors.add(:base, "Error")
    	expect(Mdr).to receive(:forms_all).and_return(mdr)
    	request.env['HTTP_ACCEPT'] = "application/json"
      get :latest, {id: "DM1 01"}
      expect(response.content_type).to eq("application/json")
      expect(response.code).to eq("422")
    #write_text_file(response.body, sub_dir, "latest_3_result.txt")
    	expected = read_text_file(sub_dir, "latest_3_result.txt")
      expect(response.body).to eq(expected)
    end

  end

	describe "Unauthorized User" do
    
    it "history" do
      get :history, {id: "DM1 01"}
      expect(response).to redirect_to("/users/sign_in")
    end

    it "latest" do
      get :latest, {id: "DM1 01"}
      expect(response).to redirect_to("/users/sign_in")
    end

  end

end
