require 'rails_helper'

describe TokensController do

  include UserAccountHelpers

  describe "Token as Sys Admin" do
    
    before :all do
      Token.delete_all
      @user1 = User.create :email => "token@assero.co.uk", :password => "chanGeme1^" 
      item1 = "ITEM1"
      item2 = "ITEM2"
      item3 = "ITEM3"
      item4 = "ITEM4"
      @token1 = Token.obtain(item1, "info 1", @user1)
      @token2 = Token.obtain(item2, "info 2", @user1)
      @token3 = Token.obtain(item3, "info 3", @user1)
      @token4 = Token.obtain(item4, "info 4", @user1)
      ua_create
    end
    
    after :all do
      @user1 = User.where(:email => "token@assero.co.uk")
      @user1[0].destroy
      Token.delete_all
      ua_destroy
    end

    before :each do
      login_with_system_admin_access
    end

    after :each do
    end

    it "provides index of the tokens" do
      Token.set_timeout(5)
      get :index
      tokens = assigns(:tokens)
      expected = 
      [
        { refresh_count: 0, item: "ITEM1", item_info: "info 1", user_id: @user1.id },
        { refresh_count: 0, item: "ITEM2", item_info: "info 2", user_id: @user1.id },
        { refresh_count: 0, item: "ITEM3", item_info: "info 3", user_id: @user1.id },
        { refresh_count: 0, item: "ITEM4", item_info: "info 4", user_id: @user1.id }
      ]
      expect(assigns(:timeout)).to eq(5)
      expect(tokens.count).to eq(4)
      expected.each_with_index do |item, index|
        #token = tokens[index]
        token = tokens.find{|x| x.item == item[:item]}
        expect(token.refresh_count).to eq(item[:refresh_count]) 
        expect(token.item).to eq(item[:item]) 
        expect(token.item_info).to eq(item[:item_info]) 
        expect(token.user_id).to eq(item[:user_id])
        expect(token.locked_at).to be_within(2.second).of Time.now
      end
      expect(response).to render_template("index")
    end

  end

  describe "Token as Edit" do
    
    before :all do
      Token.delete_all
      @user1 = User.create :email => "token@assero.co.uk", :password => "chanGeme1^" 
      item1 = "ITEM1"
      item2 = "ITEM2"
      item3 = "ITEM3"
      item4 = "ITEM4"
      @token1 = Token.obtain(item1, "info 1", @user1)
      @token2 = Token.obtain(item2, "info 2", @user1)
      @token3 = Token.obtain(item3, "info 3", @user1)
      @token4 = Token.obtain(item4, "info 4", @user1)
      ua_create
    end
    
    after :all do
      @user1 = User.where(:email => "token@assero.co.uk")
      @user1[0].destroy
      Token.delete_all
      ua_destroy
    end

    before :each do
      login_with_edit_access
    end

    after :each do
    end

    it "will release a token, HTTP" do
      post :release, :id => @token1.id
      tokens = Token.all
      expected = 
      [
        { refresh_count: 0, item: "ITEM2", item_info: "info 2", user_id: @user1.id },
        { refresh_count: 0, item: "ITEM3", item_info: "info 3", user_id: @user1.id },
        { refresh_count: 0, item: "ITEM4", item_info: "info 4", user_id: @user1.id }
      ]
      expect(tokens.count).to eq(3)
      expected.each_with_index do |item, index|
        #token = tokens[index]
        token = tokens.find{|x| x.item == item[:item]}
        expect(token.refresh_count).to eq(item[:refresh_count]) 
        expect(token.item).to eq(item[:item]) 
        expect(token.item_info).to eq(item[:item_info]) 
        expect(token.user_id).to eq(item[:user_id])
        expect(token.locked_at).to be_within(2.second).of Time.now
      end
      expect(response).to redirect_to("/tokens")
    end

    it "will release a token, JSON" do
      request.env['HTTP_ACCEPT'] = "application/json"
      post :release, :id => @token2.id
      expect(response.content_type).to eq("application/json")
      expect(response.code).to eq("200")  
      expect(response.body).to eq("{}")
      tokens = Token.all
      expect(tokens.count).to eq(3)
      expected = 
      [
        { refresh_count: 0, item: "ITEM1", item_info: "info 1", user_id: @user1.id },
        { refresh_count: 0, item: "ITEM3", item_info: "info 3", user_id: @user1.id },
        { refresh_count: 0, item: "ITEM4", item_info: "info 4", user_id: @user1.id }
      ]
      expected.each_with_index do |item, index|
        #token = tokens[index]
        token = tokens.find{|x| x.item == item[:item]}
        expect(token.refresh_count).to eq(item[:refresh_count]) 
        expect(token.item).to eq(item[:item]) 
        expect(token.item_info).to eq(item[:item_info]) 
        expect(token.user_id).to eq(item[:user_id])
        expect(token.locked_at).to be_within(2.second).of Time.now
      end

    end

    it "status" do
      expected = { running: false, remaining: 0 }
      request.env['HTTP_ACCEPT'] = "application/json"
      get :status, :id => 6
      expect(response.code).to eq("200")  
      expect(response.body).to eq(expected.to_json)
    end

  end

  describe "Unauthorized User" do
    
    it "index" do
      get :index
      expect(response).to redirect_to("/users/sign_in")
    end

  end

  describe "Read Access User" do
    
    before :all do
      Token.delete_all
      @user1 = User.create :email => "token@assero.co.uk", :password => "Changeme1^" 
      item1 = "ITEM ONE"
      @token1 = Token.obtain(item1, "Item One", @user1)
      ua_create
    end
    
    after :all do
      @user1 = User.where(:email => "token@assero.co.uk")
      @user1[0].destroy
      Token.delete_all
      ua_destroy
    end

    before :each do
      login_with_read_access
    end

    after :each do
    end

    it "index" do
      get :index
      expect(response).to redirect_to("/")
    end

    it "allows the staus of a token to be obtained" do
      request.env['HTTP_ACCEPT'] = "application/json"
      remaining = @token1.remaining
      post :status, :id => @token1.id
      expect(response.content_type).to eq("application/json")
      expect(response.code).to eq("200")  
      result = JSON.parse(response.body)
      expect(result["running"]).to eq(true)
      expect(result["remaining"]).to eq(remaining)
    end

    it "allows the staus of a token to be obtained, no token" do
      request.env['HTTP_ACCEPT'] = "application/json"
      post :status, :id => 6
      expect(response.content_type).to eq("application/json")
      expect(response.code).to eq("200")  
      result = JSON.parse(response.body)
      expect(result["running"]).to eq(false)
      expect(result["remaining"]).to eq(0)
    end

    it "allows the token to be extended, no token" do
      request.env['HTTP_ACCEPT'] = "application/json"
      post :extend_token, :id => 6
      expect(response.content_type).to eq("application/json")
      expect(response.code).to eq("200")  
      expect(response.body).to eq("{}")
    end

    it "allows the token to be extended" do
      request.env['HTTP_ACCEPT'] = "application/json"
      post :extend_token, :id => @token1.id
      expect(response.content_type).to eq("application/json")
      expect(response.code).to eq("200")  
      expect(response.body).to eq("{}")
      expect(@token1.remaining).to eq(Token.get_timeout)
    end

    it "allows the token to be extended, no token" do
      request.env['HTTP_ACCEPT'] = "application/json"
      post :extend_token, :id => 6
      expect(response.content_type).to eq("application/json")
      expect(response.code).to eq("200")  
      expect(response.body).to eq("{}")
    end

  end

end