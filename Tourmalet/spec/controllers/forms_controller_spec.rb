require 'rails_helper'

describe FormsController, type: :controller do

  include UserAccountHelpers
  include FileHelpers
  include JsonHelpers

  def sub_dir
    return "controllers"
  end

  describe "Edit Access" do

    before :all do
      Form.delete_all
      ua_create
    end
    
    after :all do
      Form.delete_all
      ua_destroy
    end
    
    before :each do
      login_with_edit_access
    end

    after :each do
    end

    it "creates an form" do
      request.env['HTTP_ACCEPT'] = "application/json"
      post :create, {form: {form_id: "F1", form_namespace: "http://www.example.com", identifier: "FORM 1", label: "Form 1", ordinal: 1 }}
      expect(response.content_type).to eq("application/json")
      expect(response.code).to eq("200")
      expect(Form.all.count).to eq(1)
    end

    it "destroys a form" do
      f = Form.create(form_id: "F1", form_namespace: "http://www.example.com", identifier: "FORM 1", label: "Form 1", ordinal: 1 )
      request.env['HTTP_ACCEPT'] = "application/json"
      delete :destroy, {id: f.id}
      expect(response.content_type).to eq("application/json")
      expect(response.code).to eq("200")
      expect(Form.all.count).to eq(0)
    end

    it "moves form" do
      f1 = Form.create(form_id: "F1", form_namespace: "http://www.example.com", identifier: "FORM 1", label: "Form 1", ordinal: 1)
      f2 = Form.create(form_id: "F2", form_namespace: "http://www.example.com", identifier: "FORM 2", label: "Form 2", ordinal: 2)
      f3 = Form.create(form_id: "F3", form_namespace: "http://www.example.com", identifier: "FORM 3", label: "Form 3", ordinal: 3)
      expect(Form.all.count).to eq(3)
      request.env['HTTP_ACCEPT'] = "application/json"
      put :move, {id: f1.id, form: {to_id: f2.id}}
      expect(response.content_type).to eq("application/json")
      expect(response.code).to eq("200")
      expect(Form.all.count).to eq(3)
      f1 = Form.find(f1.id)
      f2 = Form.find(f2.id)
      expect(f1.ordinal).to eq(2)
      expect(f2.ordinal).to eq(1)
    end

  end

  describe "Unauthorized User" do
    
    it "move" do
      put :move, { id: 3 }
      expect(response).to redirect_to("/users/sign_in")
    end

    it "create" do
      post :create
      expect(response).to redirect_to("/users/sign_in")
    end

    it 'destroy' do
      delete :destroy, {id: 1}
      expect(response).to redirect_to("/users/sign_in")
    end

  end

end
