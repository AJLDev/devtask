require 'rails_helper'

describe Api::V1::StudyVersionsController, type: :controller do

  include UserAccountHelpers
  include FileHelpers
  include JsonHelpers
  include StudyVersionHelpers

  def sub_dir
    return "controllers/api/v1"
  end

  def set_http_request
  	# JSON and username, password
  	request.env['HTTP_ACCEPT'] = "application/json"
    request.env['HTTP_AUTHORIZATION'] = ActionController::HttpAuthentication::Basic.encode_credentials(ENV['api_username'],ENV['api_password'])
  end

  describe "Edit Access" do

    before :all do
    end
    
    after :all do
    end
    
    before :each do
      Study.delete_all
      StudyVersion.delete_all
    end

    after :each do
      Study.delete_all
      StudyVersion.delete_all
    end

    it "provides a study version, no version" do
      s1 = Study.create(label: "Study 1", identifier: "STUDY 1")
      set_http_request
      get :show, {study_id: s1.id, id: 1}
      expected_hash = {"errors"=>["Failed to find study version with identifier 1"]}
      result_hash = JSON.parse(response.body)
      expect(result_hash).to eq(expected_hash)
      expect(response.status).to eq 404
    end

    it "provides a study version" do
      s1 = Study.create(label: "Study 1", identifier: "STUDY 1")
      sv1 = create_study_version(s1.id, "V1", "description", "whatever", StudyVersion.states[:incomplete], 1, "0.1.0")
      set_http_request
      get :show, {study_id: s1.id, id: sv1.id}
      result = JSON.parse(response.body)
    #write_yaml_file(result, sub_dir, "study_version_show_1.yaml")
      expected = read_yaml_file(sub_dir, "study_version_show_1.yaml")
      result.except!("id")
      expected.except!("id")
      expect(result).to eq(expected)
    end

  end

  describe "Unauthorized User" do
    
    it "rejects unauthorised user, 2" do
      s1 = Study.create(label: "Study 1", identifier: "STUDY 1")
      sv1 = create_study_version(s1.id, "V1", "description", "whatever", StudyVersion.states[:incomplete], 1, "0.1.0")
      request.env['HTTP_ACCEPT'] = "application/json"
      get :show, {study_id: s1.id, id: sv1.id}
      expect(response.status).to eq 401
    end

  end

end
