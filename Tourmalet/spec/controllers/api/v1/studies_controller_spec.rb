require 'rails_helper'

describe Api::V1::StudiesController, type: :controller do

  include UserAccountHelpers
  include FileHelpers
  include JsonHelpers
  include StudyVersionHelpers

  def sub_dir
    return "controllers/api/v1"
  end

  def set_http_request
  	# JSON and username, password
  	request.env['HTTP_ACCEPT'] = "application/json"
    request.env['HTTP_AUTHORIZATION'] = ActionController::HttpAuthentication::Basic.encode_credentials(ENV['api_username'],ENV['api_password'])
  end

  describe "Edit Access" do

    before :all do
    end
    
    after :all do
    end
    
    before :each do
      Study.delete_all
      StudyVersion.delete_all
    end

    after :each do
      Study.delete_all
      StudyVersion.delete_all
    end

    it "provides a studies index, empty" do
      set_http_request
      get :index
      expected = []
      result_hash = JSON.parse(response.body)
      expect(result_hash).to eq(expected)
    end

    it "provides a studies index" do
      s1 = Study.create(label: "Study 1", identifier: "STUDY 1")
      s2 = Study.create(label: "Study 2", identifier: "STUDY 2")
      s3 = Study.create(label: "Study 3", identifier: "STUDY 3")
      set_http_request
      get :index
      result = api_remove_id(response)
    #write_yaml_file(result, sub_dir, "study_index_1.yaml")
      expected = read_yaml_file(sub_dir, "study_index_1.yaml")
      expect(result).to eq(expected)
    end

    it "provides a study history, no study" do
      set_http_request
      get :history, {id: 1}
      expected_hash = {"errors"=>["Failed to find study with identifier 1"]}
      result_hash = JSON.parse(response.body)
      expect(result_hash).to eq(expected_hash)
      expect(response.status).to eq 404
    end

    it "provides a study history, no study version" do
      s1 = Study.create(label: "Study 1", identifier: "STUDY 1")
      set_http_request
      get :history, {id: s1.id}
      result = api_remove_id(response)
    #write_yaml_file(result, sub_dir, "study_history_1.yaml")
      expected = read_yaml_file(sub_dir, "study_history_1.yaml")
      expect(result).to eq(expected)
    end

    it "provides a study history" do
      s1 = Study.create(label: "Study 1", identifier: "STUDY 1")
      sv1 = create_study_version(s1.id, "V1", "description", "whatever", StudyVersion.states[:incomplete], 1, "0.1.0")
      sv2 = create_study_version(s1.id, "V2", "description", "whatever", StudyVersion.states[:qualified], 2, "0.2.0")
      set_http_request
      get :history, {id: s1.id}
      result = api_remove_id(response)
    #write_yaml_file(result, sub_dir, "study_history_2.yaml")
      expected = read_yaml_file(sub_dir, "study_history_2.yaml")
      expect(result).to eq(expected)
    end

  end

  describe "Unauthorized User" do
    
    it "rejects unauthorised user, 1" do
      request.env['HTTP_ACCEPT'] = "application/json"
      get :index
      expect(response.status).to eq 401
    end

    it "rejects unauthorised user, 3" do
      s1 = Study.create(label: "Study 1", identifier: "STUDY 1")
      request.env['HTTP_ACCEPT'] = "application/json"
      get :history, {id: s1.id}
      expect(response.status).to eq 401
    end

  end

end
