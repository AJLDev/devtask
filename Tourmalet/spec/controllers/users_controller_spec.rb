require 'rails_helper'

describe UsersController do

  include FileHelpers
  include UserAccountHelpers
  
  describe "Authorized User" do
  	
    before :all do
      AuditTrail.delete_all
      ua_create
    end

    after :all do
      ua_destroy
    end
    
    before :each do
      login_with_system_admin_access
    end

    after :each do
    end

    it "index users" do
      user1 = User.create :email => "fred@assero.co.uk", :password => "cHangeMe14%" 
      user2 = User.create :email => "sid@assero.co.uk", :password => "cHangeMe14%" 
      user3 = User.create :email => "boris@assero.co.uk", :password => "cHangeMe14%" 
      users = User.all
      get :index
      expect(assigns(:users)).to match_array(users)
      expect(response).to render_template("index")
    end

    it "new user" do
      get :new
      expect(response).to render_template("new")
    end

    it 'creates user' do
      user1 = User.create :email => "fred@assero.co.uk", :password => "cHangeMe14%" 
      user2 = User.create :email => "sid@assero.co.uk", :password => "cHangeMe14%" 
      user3 = User.create :email => "boris@assero.co.uk", :password => "cHangeMe14%" 
      user_count = User.all.count
      post :create, user: { email: "new1@assero.co.uk", password: "Ca1234*5678", password_confirmation: "Ca1234*5678" }
      expect(User.all.count).to eq(user_count + 1)
      expect(flash[:success]).to be_present
      expect(response).to redirect_to("/users")
    end

    it 'creates user, fails, short password' do
      password = "1" * (User.password_length.min - 1)
      user_count = User.all.count
      post :create, user: { email: "new2@assero.co.uk", password: password, password_confirmation: password }
      expect(User.all.count).to eq(user_count)
      expect(flash[:error]).to be_present
      expect(response).to redirect_to("/users")
    end

    it 'creates user, fails, lower case' do
      password = "a" * (User.password_length.min)
      user_count = User.all.count
      post :create, user: { email: "new2@assero.co.uk", password: password, password_confirmation: password }
      expect(User.all.count).to eq(user_count)
      expect(flash[:error]).to be_present
      expect(response).to redirect_to("/users")
    end

    it 'creates user, fails, upper case' do
      password = "B" * (User.password_length.min)
      user_count = User.all.count
      post :create, user: { email: "new2@assero.co.uk", password: password, password_confirmation: password }
      expect(User.all.count).to eq(user_count)
      expect(flash[:error]).to be_present
      expect(response).to redirect_to("/users")
    end

    it 'creates user, fails, digits case' do
      password = "3" * (User.password_length.min)
      user_count = User.all.count
      post :create, user: { email: "new2@assero.co.uk", password: password, password_confirmation: password }
      expect(User.all.count).to eq(user_count)
      expect(flash[:error]).to be_present
      expect(response).to redirect_to("/users")
    end

    it 'creates user, fails, no special' do
      password = "a" * (User.password_length.min)
      password = "#{password}A1"
      user_count = User.all.count
      post :create, user: { email: "new2@assero.co.uk", password: password, password_confirmation: password }
      expect(User.all.count).to eq(user_count)
      expect(flash[:error]).to be_present
      expect(response).to redirect_to("/users")
    end

    it 'creates user' do
      password = "a" * (User.password_length.min)
      password = "#{password}A1^"
      user_count = User.all.count
      post :create, user: { email: "new2@assero.co.uk", password: password, password_confirmation: password }
      expect(User.all.count).to eq(user_count + 1)
      expect(flash[:success]).to be_present
      expect(response).to redirect_to("/users")
    end

    it 'deletes user' do
      user1 = User.create :email => "fred@assero.co.uk", :password => "cHangeMe14%" 
      user2 = User.create :email => "sid@assero.co.uk", :password => "cHangeMe14%" 
      user3 = User.create :email => "boris@assero.co.uk", :password => "cHangeMe14%" 
      user4 = User.create :email => "new@assero.co.uk", :password => "cHangeMe14%" 
      user_count = User.all.count
      delete :destroy, :id => user4.id
      expect(User.all.count).to eq(user_count - 1)
    end

    it "edits user" do
      user1 = User.create :email => "fred@assero.co.uk", :password => "cHangeMe14%" 
      user2 = User.create :email => "sid@assero.co.uk", :password => "cHangeMe14%" 
      user3 = User.create :email => "boris@assero.co.uk", :password => "cHangeMe14%" 
      user = User.find_by(:email => "sid@assero.co.uk")
      get :edit, :id => user.id
      expect(response).to render_template("edit")
    end

    it "updates user" do
      user1 = User.create :email => "fred@assero.co.uk", :password => "cHangeMe14%" 
      user2 = User.create :email => "sid@assero.co.uk", :password => "cHangeMe14%" 
      user3 = User.create :email => "boris@assero.co.uk", :password => "cHangeMe14%" 
      put :set_edit, {id: user1.id}
      user1 = User.find_by(:email => "fred@assero.co.uk")
      expect(user1.has_read_access?).to eq(true)
      expect(user1.has_edit_access?).to eq(true)
      expect(user1.has_system_admin_access?).to eq(false)
      expect(response).to redirect_to("/users")
      put :set_read, {id: user1.id}
      user1 = User.find_by(:email => "fred@assero.co.uk")
      expect(user1.has_read_access?).to eq(true)
      expect(user1.has_edit_access?).to eq(false)
      expect(user1.has_system_admin_access?).to eq(false)
      put :set_system_admin, {id: user1.id}
      user1 = User.find_by(:email => "fred@assero.co.uk")
      expect(user1.has_read_access?).to eq(true)
      expect(user1.has_edit_access?).to eq(true)
      expect(user1.has_system_admin_access?).to eq(true)      
    end

  end

  describe "Unauthorized User" do
    
    it "index user" do
      get :index
      expect(response).to redirect_to("/users/sign_in")
    end

    it "new user" do
      get :new
      expect(response).to redirect_to("/users/sign_in")
    end

    it 'creates user' do
      post :create, iso_user: { name: "XXX Pharma", shortName: "XXX" }
      expect(response).to redirect_to("/users/sign_in")
    end

  end

end