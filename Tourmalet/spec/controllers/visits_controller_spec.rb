require 'rails_helper'

describe VisitsController, type: :controller do

  include UserAccountHelpers
  include FileHelpers
  include JsonHelpers

  def sub_dir
    return "controllers"
  end

  describe "Edit Access" do

    before :all do
      Visit.delete_all
      ua_create
    end
    
    after :all do
      Visit.delete_all
      ua_destroy
    end
    
    before :each do
      login_with_edit_access
    end

    after :each do
    end

    it "creates an visit" do
      request.env['HTTP_ACCEPT'] = "application/json"
      post :create, {visit: {short_name: "V1", long_name: "Visit 1", timing: "Day 0", ordinal: 1 }}
      expect(response.content_type).to eq("application/json")
      expect(response.code).to eq("200")
      expect(Visit.all.count).to eq(1)
    end

    it "destroys a visit" do
      v = Visit.create(short_name: "V1", long_name: "Visit 1", timing: "Day 0", ordinal: 2 )
      request.env['HTTP_ACCEPT'] = "application/json"
      delete :destroy, {id: v.id}
      expect(response.content_type).to eq("application/json")
      expect(response.code).to eq("200")
      expect(Visit.all.count).to eq(0)
    end

    it "moves visit" do
      v1 = Visit.create(short_name: "V1", long_name: "Visit 1", timing: "Day 0", ordinal: 2 )
      v2 = Visit.create(short_name: "V2", long_name: "Visit 2", timing: "Day 7", ordinal: 3 )
      v3 = Visit.create(short_name: "V3", long_name: "Visit 3", timing: "Day 14", ordinal: 4 )
      expect(Visit.all.count).to eq(3)
      request.env['HTTP_ACCEPT'] = "application/json"
      put :move, {id: v1.id, visit: {to_id: v2.id}}
      expect(response.content_type).to eq("application/json")
      expect(response.code).to eq("200")
      expect(Visit.all.count).to eq(3)
      v1 = Visit.find(v1.id)
      v2 = Visit.find(v2.id)
      expect(v1.ordinal).to eq(3)
      expect(v2.ordinal).to eq(2)
    end

  end

  describe "Unauthorized User" do
    
    it "move" do
      put :move, { id: 3 }
      expect(response).to redirect_to("/users/sign_in")
    end

    it "create" do
      post :create
      expect(response).to redirect_to("/users/sign_in")
    end

    it 'destroy' do
      delete :destroy, {id: 1}
      expect(response).to redirect_to("/users/sign_in")
    end

  end

end
