require 'rails_helper'

describe StudyVersionsController, type: :controller do

  include UserAccountHelpers
  include StudyVersionHelpers
  include FileHelpers

  def sub_dir
    return "controllers"
  end

  describe "Edit Access" do

    before :all do
      ua_create
    end

    after :all do
      ua_destroy
    end

    before :each do
      login_with_edit_access
      Study.delete_all
      StudyVersion.delete_all
      Token.delete_all
      AuditTrail.delete_all
    end

    after :each do
      Study.delete_all
      StudyVersion.delete_all
      Token.delete_all
      AuditTrail.delete_all
    end

    it "edit a version, no new version" do
      audit_count = AuditTrail.all.count
      s1 = Study.create(label: "Study 1", identifier: "STUDY 1")
      sv = create_study_version(s1.id, "V1", "description", "whatever", StudyVersion.states[:incomplete], 1, "0.1.0")
      get :edit, id: sv.id
      result_s = assigns[:study]
      result_sv = assigns[:study_version]
      result_t = assigns[:token]
      expect(result_s.id).to eq(s1.id)
      expect(result_sv.id).to eq(sv.id)
      tokens = Token.where(item: "STUDY 1")
      expect(result_t.id).to eq(tokens[0].id)
      expect(response).to render_template("edit")
      expect(AuditTrail.all.count).to eq(audit_count + 1)
    end

    it "edit a version, new version" do
      audit_count = AuditTrail.all.count
      s1 = Study.create(label: "Study 1", identifier: "STUDY 1")
      sv1 = create_study_version(s1.id, "V1", "description", "whatever", StudyVersion.states[:incomplete], 1, "0.1.0")
      sv2 = create_study_version(s1.id, "V2", "description", "whatever", StudyVersion.states[:qualified], 2, "0.2.0")
      get :edit, id: sv2.id
      result_s = assigns[:study]
      result_sv = assigns[:study_version]
      result_t = assigns[:token]
      expect(result_s.id).to eq(s1.id)
      new_sv = StudyVersion.where(version: 3)
      tokens = Token.where(item: "STUDY 1")
      expect(result_t.id).to eq(tokens[0].id)
      expect(result_sv.id).to eq(new_sv[0].id)
      expect(response).to render_template("edit")
      expect(AuditTrail.all.count).to eq(audit_count + 2)
    end

    it "edit a version, locked" do
      @request.env['HTTP_REFERER'] = 'http://test.host/home'
      s1 = Study.create(label: "Study 1", identifier: "STUDY 1")
      sv1 = create_study_version(s1.id, "V1", "description", "whatever", StudyVersion.states[:incomplete], 1, "0.1.0")
      sv2 = create_study_version(s1.id, "V2", "description", "whatever", StudyVersion.states[:qualified], 2, "0.2.0")
      token = Token.obtain(s1.identifier, sv2.semantic_version, @user_r)
      get :edit, id: sv2.id
      expect(flash[:error]).to be_present
      expect(StudyVersion.all.count).to eq(2)
      expect(response).to redirect_to("/home")
    end

    it "status of a version" do
      s1 = Study.create(label: "Study 1", identifier: "STUDY 1")
      sv1 = create_study_version(s1.id, "V1", "description", "whatever", StudyVersion.states[:incomplete], 1, "0.1.0")
      sv2 = create_study_version(s1.id, "V2", "description", "whatever", StudyVersion.states[:qualified], 2, "0.2.0")
      get :status, id: sv2.id
      result_s = assigns[:study]
      result_sv = assigns[:study_version]
      expect(result_s.id).to eq(s1.id)
      expect(result_sv.id).to eq(sv2.id)
      expect(response).to render_template("status")
    end

    it "status of a version, locked" do
      @request.env['HTTP_REFERER'] = 'http://test.host/home'
      s1 = Study.create(label: "Study 1", identifier: "STUDY 1")
      sv1 = create_study_version(s1.id, "V1", "description", "whatever", StudyVersion.states[:incomplete], 1, "0.1.0")
      sv2 = create_study_version(s1.id, "V2", "description", "whatever", StudyVersion.states[:qualified], 2, "0.2.0")
      token = Token.obtain(s1.identifier, sv2.semantic_version, @user_r)
      get :status, id: sv2.id
      expect(flash[:error]).to be_present
      expect(response).to redirect_to("/home")
    end

    it "update status of a version" do
      audit_count = AuditTrail.all.count
      s1 = Study.create(label: "Study 1", identifier: "STUDY 1")
      sv1 = create_study_version(s1.id, "V1", "description", "whatever", StudyVersion.states[:incomplete], 1, "0.1.0")
      sv2 = create_study_version(s1.id, "V2", "description", "whatever", StudyVersion.states[:qualified], 2, "0.2.0")
      token = Token.obtain(s1.identifier, sv2.semantic_version, @user_e) # We need to lock it with our user so update works.
      put :update_status, { id: sv2.id, study_version: { state: :standard.to_s }}
      result = StudyVersion.find(sv2.id)
      expect(result.state).to eq("standard")
      expect(AuditTrail.all.count).to eq(audit_count + 1)
    end

    it "update status of a version, no lock"  do
      audit_count = AuditTrail.all.count
      s1 = Study.create(label: "Study 1", identifier: "STUDY 1")
      sv1 = create_study_version(s1.id, "V1", "description", "whatever", StudyVersion.states[:incomplete], 1, "0.1.0")
      sv2 = create_study_version(s1.id, "V2", "description", "whatever", StudyVersion.states[:qualified], 2, "0.2.0")
      put :update_status, { id: sv2.id, study_version: { state: :standard.to_s }}
      result = StudyVersion.find(sv2.id)
      expect(result.state).to eq("qualified")
      expect(AuditTrail.all.count).to eq(audit_count)
    end

    it "update status of a version, error"

    it "soa" do
      study = create_study("STUDY X", "label")
      study_version = create_study_version(study.id, "Name", "description", "protocol_name", StudyVersion.states[:incomplete], 1, "0.1.0")
      form1 = create_form(study_version, "XXX1", "http://www.example.com/form", "F1", 1, "Label 1")
      form2 = create_form(study_version, "XXX2", "http://www.example.com/form", "F2", 2, "Label 2")
      form3 = create_form(study_version, "XXX2", "http://www.example.com/form", "F3", 3, "Label 3")
      visit1 = create_visit(study_version, "V1", "Visit 1", "Day 0", 1)
      visit2 = create_visit(study_version, "V2", "Visit 2", "Day 7", 2)
      element1 = create_element(study_version, form1, visit1)
      element2 = create_element(study_version, form2, visit1)
      request.env['HTTP_ACCEPT'] = "text"
      get :soa, id: study_version.id
      #write_text_file(response.body, sub_dir, "study_versions_soa_1.txt")
      expected = read_text_file(sub_dir, "study_versions_soa_1.txt")
      expect(response.body).to eq(expected)
    end

    it "clear" do
      study = create_study("STUDY X", "label")
      study_version = create_study_version(study.id, "Name", "description", "protocol_name", StudyVersion.states[:incomplete], 1, "0.1.0")
      form1 = create_form(study_version.id, "XXX1", "http://www.example.com/form", "F1", 1, "Label 1")
      form2 = create_form(study_version.id, "XXX2", "http://www.example.com/form", "F2", 2, "Label 2")
      form3 = create_form(study_version.id, "XXX2", "http://www.example.com/form", "F3", 3, "Label 3")
      visit1 = create_visit(study_version.id, "V1", "Visit 1", "Day 0", 1)
      visit2 = create_visit(study_version.id, "V2", "Visit 2", "Day 7", 2)
      element1 = create_element(study_version.id, form1.id, visit1.id)
      element2 = create_element(study_version.id, form2.id, visit1.id)
      element3 = create_element(study_version.id, form2.id, visit2.id)
      element4 = create_element(study_version.id, form3.id, visit2.id)
      study_version.state = StudyVersion.states[:incomplete]
      study_version.semantic_version = "0.1.0"
      study_version.version = 1
      study_version.save
      token = Token.obtain(study.identifier, study_version.semantic_version, @user_e) # We need to lock it with our user so update works.
      put :clear, id: study_version.id
      forms = Form.where(study_version_id: study_version.id)
      visits = Visit.where(study_version_id: study_version.id)
      elements = Element.where(study_version_id: study_version.id)
      expect(forms.count).to eq(0)
      expect(visits.count).to eq(0)
      expect(elements.count).to eq(0)
    end

    it "show" do
      study = create_study("STUDY X", "label")
      study_version = create_study_version(study.id, "Name", "Description", "Protocol Name", StudyVersion.states[:standard], 1, "0.1.0")
      form1 = create_form(study_version.id, "XXX1", "http://www.example.com/form", "F1", 1, "Label 1")
      form2 = create_form(study_version.id, "XXX2", "http://www.example.com/form", "F2", 2, "Label 2")
      form3 = create_form(study_version.id, "XXX2", "http://www.example.com/form", "F3", 3, "Label 3")
      request.env['HTTP_ACCEPT'] = "application/json"
      get :show, id: study_version.id
      #write_text_file(response.body, sub_dir, "study_versions_1.txt")
      expected = read_text_file(sub_dir, "study_versions_1.txt")
      expected_hash = JSON.parse(expected)
      result_hash = JSON.parse(response.body)
      expected_hash['data']['id'] = result_hash['data']['id'] # ids wont match
      expected_hash['data']['children'].each_with_index do |child, index|
        child['id'] = result_hash['data']['children'][index]['id'] # ids wont match
      end
      expect(result_hash).to eq(expected_hash)
    end

    it "allows a study version to be deleted" do
      s1 = create_study("STUDY X", "label")
      sv = create_study_version(s1.id, "Name", "description", "protocol_name", StudyVersion.states[:incomplete], 1, "0.1.0")
      delete :destroy, id: sv.id
      expect(response).to redirect_to(history_study_path(s1))
    end

    it "allows a study version to be deleted, locked"

    it "exports als"

    it "exports json"

    it "exports odm, no lock" do
      s1 = create_study("STUDY X", "label")
      sv = create_study_version(s1.id, "Name", "description", "protocol_name", StudyVersion.states[:incomplete], 1, "0.1.0")
      get :export, {id: sv.id, study_version: { export_type: "odm" }}
      expect(assigns[:path]).to eq(export_odm_study_version_path(sv))
      lock = Token.obtain("#{s1.identifier}_ODM", "", @user_e)
      expect(lock).to_not be(nil)
    end

    it "exports odm, locked" do
      s1 = create_study("STUDY X", "label")
      sv = create_study_version(s1.id, "Name", "description", "protocol_name", StudyVersion.states[:incomplete], 1, "0.1.0")
      lock = Token.obtain("#{s1.identifier}_ODM", "", @user_r) # Lock obtained by different user.
      get :export, {id: sv.id, study_version: { export_type: "odm" }}
      expect(assigns[:lock_id]).to eq(0)
      expect(flash[:error]).to be_present
    end

    it "generates odm and release lock" do
      s1 = create_study("STUDY X", "label")
      sv = create_study_version(s1.id, "Name", "description", "protocol_name", StudyVersion.states[:incomplete], 1, "0.1.0")
      lock = Token.obtain("#{s1.identifier}_ODM", "", @user_e)
      get :export_odm, {id: sv.id}
      lock = Token.find_token("#{s1.identifier}_ODM", @user_e)
      expect(lock).to eq(nil)
      expect(response.content_type).to eq("application/xhtml+xml; header=present")
    end

    it "exports crf, no lock" do
      s1 = create_study("STUDY X", "label")
      sv = create_study_version(s1.id, "Name", "description", "protocol_name", StudyVersion.states[:incomplete], 1, "0.1.0")
      get :export, {id: sv.id, study_version: { export_type: "crf" }}
      expect(assigns[:path]).to eq(export_crf_study_version_path(sv))
      lock = Token.obtain("#{s1.identifier}_CRF", "", @user_e)
      expect(lock).to_not be(nil)
    end

    it "exports crf, locked" do
      s1 = create_study("STUDY X", "label")
      sv = create_study_version(s1.id, "Name", "description", "protocol_name", StudyVersion.states[:incomplete], 1, "0.1.0")
      lock = Token.obtain("#{s1.identifier}_CRF", "", @user_r) # Lock obtained by different user.
      get :export, {id: sv.id, study_version: { export_type: "crf" }}
      expect(assigns[:lock_id]).to eq(0)
      expect(flash[:error]).to be_present
    end

    it "generates crf and release lock" do
      s1 = create_study("STUDY X", "label")
      sv = create_study_version(s1.id, "Name", "description", "protocol_name", StudyVersion.states[:incomplete], 1, "0.1.0")
      lock = Token.obtain("#{s1.identifier}_CRF", "", @user_e)
      get :export_crf, {id: sv.id, table_data: soa_html}
      lock = Token.find_token("#{s1.identifier}_CRF", @user_e)
      expect(lock).to eq(nil)
      expect(response).to be_successful
      expect(response.content_type).to eq("application/pdf")
    end


    it "exports acrf, no lock" do
      s1 = create_study("STUDY X", "label")
      sv = create_study_version(s1.id, "Name", "description", "protocol_name", StudyVersion.states[:incomplete], 1, "0.1.0")
      get :export, {id: sv.id, study_version: { export_type: "acrf" }}
      expect(assigns[:path]).to eq(export_acrf_study_version_path(sv))
      lock = Token.obtain("#{s1.identifier}_ACRF", "", @user_e)
      expect(lock).to_not be(nil)
    end

    it "exports acrf, locked" do
      s1 = create_study("STUDY X", "label")
      sv = create_study_version(s1.id, "Name", "description", "protocol_name", StudyVersion.states[:incomplete], 1, "0.1.0")
      lock = Token.obtain("#{s1.identifier}_ACRF", "", @user_r) # Lock obtained by different user.
      get :export, {id: sv.id, study_version: { export_type: "acrf" }}
      expect(assigns[:lock_id]).to eq(0)
      expect(flash[:error]).to be_present
    end

    it "generates acrf and release lock" do
      s1 = create_study("STUDY X", "label")
      sv = create_study_version(s1.id, "Name", "description", "protocol_name", StudyVersion.states[:incomplete], 1, "0.1.0")
      lock = Token.obtain("#{s1.identifier}_ACRF", "", @user_e)
      get :export_acrf, {id: sv.id, table_data: soa_html}
      lock = Token.find_token("#{s1.identifier}_ACRF", @user_e)
      expect(lock).to eq(nil)
      expect(response).to be_successful
      expect(response.content_type).to eq("application/pdf")
    end

  end

  describe "Unauthorized User" do

    it "edit" do
      get :edit, {id: 1}
      expect(response).to redirect_to("/users/sign_in")
    end

    it "status" do
      get :status, {id: 1}
      expect(response).to redirect_to("/users/sign_in")
    end

    it 'update_status' do
      put :update_status, {id: 1}
      expect(response).to redirect_to("/users/sign_in")
    end

    it 'soa' do
      put :soa, {id: 1}
      expect(response).to redirect_to("/users/sign_in")
    end

    it 'clear' do
      put :clear, {id: 1}
      expect(response).to redirect_to("/users/sign_in")
    end

    it 'show' do
      get :show, {id: 1}
      expect(response).to redirect_to("/users/sign_in")
    end

    it 'destroy' do
      delete :destroy, {id: 1}
      expect(response).to redirect_to("/users/sign_in")
    end

    it 'export' do
      delete :export, {id: 1}
      expect(response).to redirect_to("/users/sign_in")
    end

    it 'export_odm' do
      delete :export_odm, {id: 1}
      expect(response).to redirect_to("/users/sign_in")
    end

    it 'export_json' do
      delete :export_odm, {id: 1}
      expect(response).to redirect_to("/users/sign_in")
    end

    it 'export_als' do
      delete :export_odm, {id: 1}
      expect(response).to redirect_to("/users/sign_in")
    end

    it 'export_crf' do
      delete :export_odm, {id: 1}
      expect(response).to redirect_to("/users/sign_in")
    end

    it 'export_acrf' do
      delete :export_odm, {id: 1}
      expect(response).to redirect_to("/users/sign_in")
    end

  end

end
