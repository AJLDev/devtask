require 'rails_helper'

describe MdrsController, type: :controller do

  include UserAccountHelpers
  include FileHelpers
  include JsonHelpers

  def sub_dir
    return "controllers"
  end

  describe "Edit Access" do

    before :all do
      Study.delete_all
      ua_create
    end
    
    after :all do
      Study.delete_all
      ua_destroy
    end
    
    before :each do
      login_with_edit_access
    end

    after :each do
    end

    it "forms, I" do
    	mdr = Mdr.new
    	mdr.data = []
    	data = read_yaml_file(sub_dir, "forms_1.yaml")
    	data.each {|x| mdr.data << x.deep_symbolize_keys}
    	expect(Mdr).to receive(:forms_all).and_return(mdr)
    	request.env['HTTP_ACCEPT'] = "application/json"
      get :forms
      expect(response.content_type).to eq("application/json")
      expect(response.code).to eq("200")
    #write_text_file(response.body, sub_dir, "forms_1_result.txt")
    	expected = read_text_file(sub_dir, "forms_1_result.txt")
      expect(response.body).to eq(expected)
    end

    it "gets forms, error" do
    	mdr = Mdr.new
    	mdr.data = []
    	mdr.errors.add(:base, "Error")
    	expect(Mdr).to receive(:forms_all).and_return(mdr)
    	request.env['HTTP_ACCEPT'] = "application/json"
      get :forms
      expect(response.content_type).to eq("application/json")
      expect(response.code).to eq("422")
    #write_text_file(response.body, sub_dir, "forms_2_result.txt")
    	expected = read_text_file(sub_dir, "forms_2_result.txt")
      expect(response.body).to eq(expected)
    end

  end

  describe "Unauthorized User" do
    
    it "forms" do
      get :forms
      expect(response).to redirect_to("/users/sign_in")
    end

  end

end
