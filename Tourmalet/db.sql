--
-- PostgreSQL database dump
--

-- Dumped from database version 9.6.5
-- Dumped by pg_dump version 9.6.5

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: amendments; Type: TABLE; Schema: public; Owner: tourmalet
--

CREATE TABLE amendments (
    id integer NOT NULL,
    study_version_id integer,
    cid character varying,
    namespace character varying,
    property character varying,
    study_value character varying,
    default_value character varying,
    datatype character varying,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


ALTER TABLE amendments OWNER TO tourmalet;

--
-- Name: amendments_id_seq; Type: SEQUENCE; Schema: public; Owner: tourmalet
--

CREATE SEQUENCE amendments_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE amendments_id_seq OWNER TO tourmalet;

--
-- Name: amendments_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: tourmalet
--

ALTER SEQUENCE amendments_id_seq OWNED BY amendments.id;


--
-- Name: audit_trails; Type: TABLE; Schema: public; Owner: tourmalet
--

CREATE TABLE audit_trails (
    id integer NOT NULL,
    date_time timestamp without time zone,
    "user" character varying,
    identifier character varying,
    version character varying,
    event integer,
    description character varying,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


ALTER TABLE audit_trails OWNER TO tourmalet;

--
-- Name: audit_trails_id_seq; Type: SEQUENCE; Schema: public; Owner: tourmalet
--

CREATE SEQUENCE audit_trails_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE audit_trails_id_seq OWNER TO tourmalet;

--
-- Name: audit_trails_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: tourmalet
--

ALTER SEQUENCE audit_trails_id_seq OWNED BY audit_trails.id;


--
-- Name: elements; Type: TABLE; Schema: public; Owner: tourmalet
--

CREATE TABLE elements (
    id integer NOT NULL,
    study_version_id integer,
    form_id integer,
    visit_id integer,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


ALTER TABLE elements OWNER TO tourmalet;

--
-- Name: elements_id_seq; Type: SEQUENCE; Schema: public; Owner: tourmalet
--

CREATE SEQUENCE elements_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE elements_id_seq OWNER TO tourmalet;

--
-- Name: elements_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: tourmalet
--

ALTER SEQUENCE elements_id_seq OWNED BY elements.id;


--
-- Name: forms; Type: TABLE; Schema: public; Owner: tourmalet
--

CREATE TABLE forms (
    id integer NOT NULL,
    study_version_id integer,
    form_id character varying,
    form_namespace character varying,
    identifier character varying,
    ordinal integer,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    label character varying
);


ALTER TABLE forms OWNER TO tourmalet;

--
-- Name: forms_id_seq; Type: SEQUENCE; Schema: public; Owner: tourmalet
--

CREATE SEQUENCE forms_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE forms_id_seq OWNER TO tourmalet;

--
-- Name: forms_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: tourmalet
--

ALTER SEQUENCE forms_id_seq OWNED BY forms.id;


--
-- Name: old_passwords; Type: TABLE; Schema: public; Owner: tourmalet
--

CREATE TABLE old_passwords (
    id integer NOT NULL,
    encrypted_password character varying NOT NULL,
    password_salt character varying,
    password_archivable_type character varying NOT NULL,
    password_archivable_id integer NOT NULL,
    created_at timestamp without time zone
);


ALTER TABLE old_passwords OWNER TO tourmalet;

--
-- Name: old_passwords_id_seq; Type: SEQUENCE; Schema: public; Owner: tourmalet
--

CREATE SEQUENCE old_passwords_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE old_passwords_id_seq OWNER TO tourmalet;

--
-- Name: old_passwords_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: tourmalet
--

ALTER SEQUENCE old_passwords_id_seq OWNED BY old_passwords.id;


--
-- Name: schema_migrations; Type: TABLE; Schema: public; Owner: tourmalet
--

CREATE TABLE schema_migrations (
    version character varying NOT NULL
);


ALTER TABLE schema_migrations OWNER TO tourmalet;

--
-- Name: studies; Type: TABLE; Schema: public; Owner: tourmalet
--

CREATE TABLE studies (
    id integer NOT NULL,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    label character varying,
    identifier character varying
);


ALTER TABLE studies OWNER TO tourmalet;

--
-- Name: studies_id_seq; Type: SEQUENCE; Schema: public; Owner: tourmalet
--

CREATE SEQUENCE studies_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE studies_id_seq OWNER TO tourmalet;

--
-- Name: studies_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: tourmalet
--

ALTER SEQUENCE studies_id_seq OWNED BY studies.id;


--
-- Name: study_versions; Type: TABLE; Schema: public; Owner: tourmalet
--

CREATE TABLE study_versions (
    id integer NOT NULL,
    study_id integer,
    name character varying,
    description character varying,
    protocol_name character varying,
    state integer,
    version integer,
    semantic_version character varying,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


ALTER TABLE study_versions OWNER TO tourmalet;

--
-- Name: study_versions_id_seq; Type: SEQUENCE; Schema: public; Owner: tourmalet
--

CREATE SEQUENCE study_versions_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE study_versions_id_seq OWNER TO tourmalet;

--
-- Name: study_versions_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: tourmalet
--

ALTER SEQUENCE study_versions_id_seq OWNED BY study_versions.id;


--
-- Name: tokens; Type: TABLE; Schema: public; Owner: tourmalet
--

CREATE TABLE tokens (
    id integer NOT NULL,
    locked_at timestamp without time zone,
    refresh_count integer,
    study_version_id integer,
    item character varying,
    item_info character varying,
    user_id integer,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


ALTER TABLE tokens OWNER TO tourmalet;

--
-- Name: tokens_id_seq; Type: SEQUENCE; Schema: public; Owner: tourmalet
--

CREATE SEQUENCE tokens_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE tokens_id_seq OWNER TO tourmalet;

--
-- Name: tokens_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: tourmalet
--

ALTER SEQUENCE tokens_id_seq OWNED BY tokens.id;


--
-- Name: user_settings; Type: TABLE; Schema: public; Owner: tourmalet
--

CREATE TABLE user_settings (
    id integer NOT NULL,
    name character varying,
    value character varying,
    user_id integer,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


ALTER TABLE user_settings OWNER TO tourmalet;

--
-- Name: user_settings_id_seq; Type: SEQUENCE; Schema: public; Owner: tourmalet
--

CREATE SEQUENCE user_settings_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE user_settings_id_seq OWNER TO tourmalet;

--
-- Name: user_settings_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: tourmalet
--

ALTER SEQUENCE user_settings_id_seq OWNED BY user_settings.id;


--
-- Name: users; Type: TABLE; Schema: public; Owner: tourmalet
--

CREATE TABLE users (
    id integer NOT NULL,
    email character varying DEFAULT ''::character varying NOT NULL,
    encrypted_password character varying DEFAULT ''::character varying NOT NULL,
    reset_password_token character varying,
    reset_password_sent_at timestamp without time zone,
    remember_created_at timestamp without time zone,
    sign_in_count integer DEFAULT 0 NOT NULL,
    current_sign_in_at timestamp without time zone,
    last_sign_in_at timestamp without time zone,
    current_sign_in_ip inet,
    last_sign_in_ip inet,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    access_level integer DEFAULT 0,
    name character varying,
    password_changed_at timestamp without time zone
);


ALTER TABLE users OWNER TO tourmalet;

--
-- Name: users_id_seq; Type: SEQUENCE; Schema: public; Owner: tourmalet
--

CREATE SEQUENCE users_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE users_id_seq OWNER TO tourmalet;

--
-- Name: users_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: tourmalet
--

ALTER SEQUENCE users_id_seq OWNED BY users.id;


--
-- Name: visits; Type: TABLE; Schema: public; Owner: tourmalet
--

CREATE TABLE visits (
    id integer NOT NULL,
    study_version_id integer,
    short_name character varying,
    long_name character varying,
    timing character varying,
    ordinal integer,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


ALTER TABLE visits OWNER TO tourmalet;

--
-- Name: visits_id_seq; Type: SEQUENCE; Schema: public; Owner: tourmalet
--

CREATE SEQUENCE visits_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE visits_id_seq OWNER TO tourmalet;

--
-- Name: visits_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: tourmalet
--

ALTER SEQUENCE visits_id_seq OWNED BY visits.id;


--
-- Name: amendments id; Type: DEFAULT; Schema: public; Owner: tourmalet
--

ALTER TABLE ONLY amendments ALTER COLUMN id SET DEFAULT nextval('amendments_id_seq'::regclass);


--
-- Name: audit_trails id; Type: DEFAULT; Schema: public; Owner: tourmalet
--

ALTER TABLE ONLY audit_trails ALTER COLUMN id SET DEFAULT nextval('audit_trails_id_seq'::regclass);


--
-- Name: elements id; Type: DEFAULT; Schema: public; Owner: tourmalet
--

ALTER TABLE ONLY elements ALTER COLUMN id SET DEFAULT nextval('elements_id_seq'::regclass);


--
-- Name: forms id; Type: DEFAULT; Schema: public; Owner: tourmalet
--

ALTER TABLE ONLY forms ALTER COLUMN id SET DEFAULT nextval('forms_id_seq'::regclass);


--
-- Name: old_passwords id; Type: DEFAULT; Schema: public; Owner: tourmalet
--

ALTER TABLE ONLY old_passwords ALTER COLUMN id SET DEFAULT nextval('old_passwords_id_seq'::regclass);


--
-- Name: studies id; Type: DEFAULT; Schema: public; Owner: tourmalet
--

ALTER TABLE ONLY studies ALTER COLUMN id SET DEFAULT nextval('studies_id_seq'::regclass);


--
-- Name: study_versions id; Type: DEFAULT; Schema: public; Owner: tourmalet
--

ALTER TABLE ONLY study_versions ALTER COLUMN id SET DEFAULT nextval('study_versions_id_seq'::regclass);


--
-- Name: tokens id; Type: DEFAULT; Schema: public; Owner: tourmalet
--

ALTER TABLE ONLY tokens ALTER COLUMN id SET DEFAULT nextval('tokens_id_seq'::regclass);


--
-- Name: user_settings id; Type: DEFAULT; Schema: public; Owner: tourmalet
--

ALTER TABLE ONLY user_settings ALTER COLUMN id SET DEFAULT nextval('user_settings_id_seq'::regclass);


--
-- Name: users id; Type: DEFAULT; Schema: public; Owner: tourmalet
--

ALTER TABLE ONLY users ALTER COLUMN id SET DEFAULT nextval('users_id_seq'::regclass);


--
-- Name: visits id; Type: DEFAULT; Schema: public; Owner: tourmalet
--

ALTER TABLE ONLY visits ALTER COLUMN id SET DEFAULT nextval('visits_id_seq'::regclass);


--
-- Data for Name: amendments; Type: TABLE DATA; Schema: public; Owner: tourmalet
--

COPY amendments (id, study_version_id, cid, namespace, property, study_value, default_value, datatype, created_at, updated_at) FROM stdin;
\.


--
-- Name: amendments_id_seq; Type: SEQUENCE SET; Schema: public; Owner: tourmalet
--

SELECT pg_catalog.setval('amendments_id_seq', 1, false);


--
-- Data for Name: audit_trails; Type: TABLE DATA; Schema: public; Owner: tourmalet
--

COPY audit_trails (id, date_time, "user", identifier, version, event, description, created_at, updated_at) FROM stdin;
1	2017-10-23 09:33:10.069623	daveih1664@gmail.com			4	User changed password.	2017-10-23 09:33:10.077209	2017-10-23 09:33:10.077209
2	2017-10-23 09:33:10.224379	lemiszewski@gmx.com			4	User changed password.	2017-10-23 09:33:10.224913	2017-10-23 09:33:10.224913
3	2017-10-23 09:34:14.240535	lemiszewski@gmx.com			4	User logged in.	2017-10-23 09:34:14.257081	2017-10-23 09:34:14.257081
4	2017-10-23 11:06:51.880774	lemiszewski@gmx.com			4	User logged in.	2017-10-23 11:06:51.881518	2017-10-23 11:06:51.881518
5	2017-10-23 11:06:57.631308	lemiszewski@gmx.com	JL Testing		2	Study created.	2017-10-23 11:06:57.631887	2017-10-23 11:06:57.631887
6	2017-10-23 11:07:56.377976	lemiszewski@gmx.com	JL Testing	0.1.0	2	Study version created.	2017-10-23 11:07:56.378656	2017-10-23 11:07:56.378656
7	2017-10-23 11:08:38.765834	lemiszewski@gmx.com	JL Testing	0.1.0	2	Study version updated	2017-10-23 11:08:38.766763	2017-10-23 11:08:38.766763
8	2017-10-23 11:12:26.964601	lemiszewski@gmx.com	JL Testing	0.1.0	2	Study version updated	2017-10-23 11:12:26.970741	2017-10-23 11:12:26.970741
9	2017-10-23 11:16:29.847078	lemiszewski@gmx.com	JL Testing	0.1.0	2	Study version updated	2017-10-23 11:16:29.847834	2017-10-23 11:16:29.847834
10	2017-10-23 11:19:45.288417	lemiszewski@gmx.com	JL Testing	0.1.0	2	Study version updated	2017-10-23 11:19:45.289322	2017-10-23 11:19:45.289322
11	2017-10-23 11:21:46.102066	lemiszewski@gmx.com	JL Testing	0.1.0	2	Study version updated	2017-10-23 11:21:46.103208	2017-10-23 11:21:46.103208
12	2017-10-23 11:22:10.893752	lemiszewski@gmx.com	JL Testing	0.1.0	2	Study version updated	2017-10-23 11:22:10.894571	2017-10-23 11:22:10.894571
13	2017-10-23 11:25:07.968953	lemiszewski@gmx.com	JL Testing	0.1.0	2	Study version updated	2017-10-23 11:25:07.970007	2017-10-23 11:25:07.970007
14	2017-10-23 11:28:48.105878	lemiszewski@gmx.com	JL Testing	0.1.0	2	Study version updated	2017-10-23 11:28:48.106625	2017-10-23 11:28:48.106625
15	2017-10-23 12:17:33.938206	lemiszewski@gmx.com	JL Testing	0.1.0	2	Study version updated	2017-10-23 12:17:33.938905	2017-10-23 12:17:33.938905
16	2017-10-23 12:24:25.398616	lemiszewski@gmx.com	JL Testing	0.1.0	2	Study version updated	2017-10-23 12:24:25.399828	2017-10-23 12:24:25.399828
17	2017-10-23 12:32:47.115881	lemiszewski@gmx.com	JL Testing	0.1.0	2	Study version updated	2017-10-23 12:32:47.116743	2017-10-23 12:32:47.116743
18	2017-10-23 12:35:01.704661	lemiszewski@gmx.com	JL Testing	0.1.0	2	Study version updated	2017-10-23 12:35:01.705779	2017-10-23 12:35:01.705779
19	2017-10-23 12:46:08.316812	lemiszewski@gmx.com	JL Testing	0.1.0	2	Study version updated	2017-10-23 12:46:08.317576	2017-10-23 12:46:08.317576
20	2017-10-23 13:01:17.019497	lemiszewski@gmx.com	JL Testing	0.1.0	2	Study version updated	2017-10-23 13:01:17.025308	2017-10-23 13:01:17.025308
21	2017-10-23 13:07:36.19869	lemiszewski@gmx.com	JL Testing	0.1.0	2	Study version updated	2017-10-23 13:07:36.204937	2017-10-23 13:07:36.204937
22	2017-10-23 13:25:17.42718	lemiszewski@gmx.com	JL Testing	0.1.0	2	Study version updated	2017-10-23 13:25:17.427916	2017-10-23 13:25:17.427916
23	2017-10-23 13:38:01.904442	lemiszewski@gmx.com	JL Testing	0.1.0	2	Study version updated	2017-10-23 13:38:01.910104	2017-10-23 13:38:01.910104
24	2017-10-23 13:38:53.938585	lemiszewski@gmx.com	JL Testing	0.1.0	2	Study version updated	2017-10-23 13:38:53.944677	2017-10-23 13:38:53.944677
25	2017-10-23 13:46:22.711743	lemiszewski@gmx.com	JL Testing	0.1.0	2	Study version updated	2017-10-23 13:46:22.712414	2017-10-23 13:46:22.712414
26	2017-10-23 13:47:45.250338	lemiszewski@gmx.com	JL Testing	0.1.0	2	Study version updated	2017-10-23 13:47:45.256693	2017-10-23 13:47:45.256693
27	2017-10-23 21:30:44.543125	lemiszewski@gmx.com	JL Testing		2	Study deleted.	2017-10-23 21:30:44.544112	2017-10-23 21:30:44.544112
28	2017-10-23 21:31:35.470292	lemiszewski@gmx.com	JL Testing		2	Study created.	2017-10-23 21:31:35.471113	2017-10-23 21:31:35.471113
29	2017-10-23 21:32:31.449663	lemiszewski@gmx.com	JL Testing	0.1.0	2	Study version created.	2017-10-23 21:32:31.450461	2017-10-23 21:32:31.450461
30	2017-10-23 21:32:55.591789	lemiszewski@gmx.com	JL Testing	0.1.0	2	Study version updated	2017-10-23 21:32:55.592871	2017-10-23 21:32:55.592871
31	2017-10-24 07:30:04.850821	lemiszewski@gmx.com			4	User logged in.	2017-10-24 07:30:04.851592	2017-10-24 07:30:04.851592
32	2017-10-24 07:31:22.046789	lemiszewski@gmx.com	JL Testing	0.1.0	2	Study version updated	2017-10-24 07:31:22.047879	2017-10-24 07:31:22.047879
33	2017-10-24 08:07:13.665353	lemiszewski@gmx.com	JL Testing	0.1.0	2	Study version updated	2017-10-24 08:07:13.671161	2017-10-24 08:07:13.671161
34	2017-10-24 08:08:25.153345	lemiszewski@gmx.com	JL Testing	0.1.0	2	Study version updated	2017-10-24 08:08:25.159365	2017-10-24 08:08:25.159365
35	2017-10-24 08:09:45.646702	lemiszewski@gmx.com	JL Testing	0.1.0	2	Study version updated	2017-10-24 08:09:45.651073	2017-10-24 08:09:45.651073
36	2017-10-24 08:13:51.16112	lemiszewski@gmx.com	JL Testing	0.1.0	2	Study version updated	2017-10-24 08:13:51.16787	2017-10-24 08:13:51.16787
37	2017-10-24 08:17:55.593153	lemiszewski@gmx.com	JL Testing	0.1.0	2	Study version updated	2017-10-24 08:17:55.599678	2017-10-24 08:17:55.599678
38	2017-10-24 08:19:10.921811	lemiszewski@gmx.com	JL Testing	0.1.0	2	Study version updated	2017-10-24 08:19:10.923042	2017-10-24 08:19:10.923042
39	2017-10-24 08:22:53.873459	lemiszewski@gmx.com	JL Testing	0.1.0	2	Study version updated	2017-10-24 08:22:53.893792	2017-10-24 08:22:53.893792
40	2017-10-24 08:24:01.804235	lemiszewski@gmx.com	JL Testing	0.1.0	2	Study version updated	2017-10-24 08:24:01.810252	2017-10-24 08:24:01.810252
41	2017-10-24 08:26:29.81547	lemiszewski@gmx.com	JL Testing	0.1.0	2	Study version updated	2017-10-24 08:26:29.821658	2017-10-24 08:26:29.821658
42	2017-10-24 08:30:31.980833	lemiszewski@gmx.com	JL Testing	0.1.0	2	Study version updated	2017-10-24 08:30:32.001802	2017-10-24 08:30:32.001802
43	2017-10-24 08:33:59.720479	lemiszewski@gmx.com	JL Testing	0.1.0	2	Study version updated	2017-10-24 08:33:59.726356	2017-10-24 08:33:59.726356
44	2017-10-24 08:38:55.929028	lemiszewski@gmx.com	JL Testing	0.1.0	2	Study version updated	2017-10-24 08:38:55.946717	2017-10-24 08:38:55.946717
45	2017-10-24 08:44:53.492558	lemiszewski@gmx.com	JL Testing	0.1.0	2	Study version updated	2017-10-24 08:44:53.501411	2017-10-24 08:44:53.501411
46	2017-10-24 10:11:58.84398	lemiszewski@gmx.com			4	User logged in.	2017-10-24 10:11:58.850817	2017-10-24 10:11:58.850817
47	2017-10-24 11:06:02.320352	lemiszewski@gmx.com	JL Testing	0.1.0	2	Study version updated	2017-10-24 11:06:02.321262	2017-10-24 11:06:02.321262
48	2017-10-24 12:02:57.418757	lemiszewski@gmx.com	JL Testing	0.1.0	2	Study version updated	2017-10-24 12:02:57.419414	2017-10-24 12:02:57.419414
49	2017-10-24 12:03:31.128498	lemiszewski@gmx.com	JL Testing	0.1.0	2	Study version updated	2017-10-24 12:03:31.129194	2017-10-24 12:03:31.129194
50	2017-10-24 12:30:14.426121	lemiszewski@gmx.com	JL Testing	0.1.0	2	Study version updated	2017-10-24 12:30:14.431101	2017-10-24 12:30:14.431101
51	2017-10-24 12:38:10.227785	lemiszewski@gmx.com	JL Testing	0.1.0	2	Study version updated	2017-10-24 12:38:10.232657	2017-10-24 12:38:10.232657
52	2017-10-24 12:40:37.827025	lemiszewski@gmx.com	JL Testing	0.1.0	2	Study version updated	2017-10-24 12:40:37.831374	2017-10-24 12:40:37.831374
53	2017-10-24 12:44:49.839969	lemiszewski@gmx.com	JL Testing	0.1.0	2	Study version updated	2017-10-24 12:44:49.846065	2017-10-24 12:44:49.846065
54	2017-10-25 07:09:09.442824	lemiszewski@gmx.com			4	User logged in.	2017-10-25 07:09:09.454454	2017-10-25 07:09:09.454454
55	2017-10-25 07:09:15.938621	lemiszewski@gmx.com	JL Testing	0.1.0	2	Study version updated	2017-10-25 07:09:15.939206	2017-10-25 07:09:15.939206
56	2017-10-25 07:24:59.742651	lemiszewski@gmx.com	JL Testing	0.1.0	2	Study version updated	2017-10-25 07:24:59.743662	2017-10-25 07:24:59.743662
57	2017-10-25 07:29:38.369217	lemiszewski@gmx.com	JL Testing	0.1.0	2	Study version updated	2017-10-25 07:29:38.387822	2017-10-25 07:29:38.387822
58	2017-10-25 07:48:15.984059	lemiszewski@gmx.com	JL Testing	0.1.0	2	Study version updated	2017-10-25 07:48:15.984936	2017-10-25 07:48:15.984936
59	2017-10-25 07:49:05.523501	lemiszewski@gmx.com	JL Testing	0.1.0	2	Study version updated	2017-10-25 07:49:05.524649	2017-10-25 07:49:05.524649
60	2017-10-25 07:49:52.19596	lemiszewski@gmx.com	JL Testing	0.1.0	2	Study version updated	2017-10-25 07:49:52.202416	2017-10-25 07:49:52.202416
61	2017-10-25 08:10:07.218347	lemiszewski@gmx.com	JL Testing	0.1.0	2	Study version updated	2017-10-25 08:10:07.224196	2017-10-25 08:10:07.224196
62	2017-10-25 08:11:45.856354	lemiszewski@gmx.com	JL Testing	0.1.0	2	Study version updated	2017-10-25 08:11:45.857177	2017-10-25 08:11:45.857177
63	2017-10-25 09:18:43.69276	lemiszewski@gmx.com	JL Testing	0.1.0	2	Study version updated	2017-10-25 09:18:43.693591	2017-10-25 09:18:43.693591
64	2017-10-25 09:53:15.308957	lemiszewski@gmx.com	JL Testing	0.1.0	2	Study version updated	2017-10-25 09:53:15.309756	2017-10-25 09:53:15.309756
65	2017-10-25 09:56:22.777821	lemiszewski@gmx.com	JL Testing	0.1.0	2	Study version updated	2017-10-25 09:56:22.778655	2017-10-25 09:56:22.778655
66	2017-10-25 09:56:37.525478	lemiszewski@gmx.com	JL Testing	0.1.0	2	Study version updated	2017-10-25 09:56:37.526191	2017-10-25 09:56:37.526191
67	2017-10-25 09:57:12.080435	lemiszewski@gmx.com	JL Testing	0.1.0	2	Study version updated	2017-10-25 09:57:12.081142	2017-10-25 09:57:12.081142
68	2017-10-25 10:58:49.02019	lemiszewski@gmx.com	JL Testing	0.1.0	2	Study version updated	2017-10-25 10:58:49.025752	2017-10-25 10:58:49.025752
69	2017-10-25 10:59:26.931827	lemiszewski@gmx.com	JL Testing	0.1.0	2	Study version updated	2017-10-25 10:59:26.938248	2017-10-25 10:59:26.938248
70	2017-10-25 13:00:23.640714	lemiszewski@gmx.com			4	User logged in.	2017-10-25 13:00:23.641706	2017-10-25 13:00:23.641706
71	2017-10-25 13:00:23.666845	lemiszewski@gmx.com	JL Testing	0.1.0	2	Study version updated	2017-10-25 13:00:23.667649	2017-10-25 13:00:23.667649
72	2017-10-26 07:52:23.654744	lemiszewski@gmx.com			4	User logged in.	2017-10-26 07:52:23.660708	2017-10-26 07:52:23.660708
73	2017-10-26 07:52:23.72308	lemiszewski@gmx.com	JL Testing	0.1.0	2	Study version updated	2017-10-26 07:52:23.723826	2017-10-26 07:52:23.723826
74	2017-10-26 07:57:28.657864	lemiszewski@gmx.com	JL Testing	0.1.0	2	Study version updated	2017-10-26 07:57:28.663333	2017-10-26 07:57:28.663333
75	2017-10-26 07:59:58.921878	lemiszewski@gmx.com	JL Testing	0.1.0	2	Study version updated	2017-10-26 07:59:58.928883	2017-10-26 07:59:58.928883
76	2017-10-26 08:25:40.632616	lemiszewski@gmx.com	JL Testing	0.1.0	2	Study version updated	2017-10-26 08:25:40.633438	2017-10-26 08:25:40.633438
77	2017-10-26 08:37:33.28041	lemiszewski@gmx.com	JL Testing	0.1.0	2	Study version updated	2017-10-26 08:37:33.281057	2017-10-26 08:37:33.281057
78	2017-10-26 08:39:00.373736	lemiszewski@gmx.com	JL Testing	0.1.0	2	Study version updated	2017-10-26 08:39:00.374583	2017-10-26 08:39:00.374583
79	2017-10-26 08:40:58.742348	lemiszewski@gmx.com	JL Testing	0.1.0	2	Study version updated	2017-10-26 08:40:58.743125	2017-10-26 08:40:58.743125
80	2017-10-26 09:21:10.770666	lemiszewski@gmx.com	JL Testing	0.1.0	2	Study version updated	2017-10-26 09:21:10.775701	2017-10-26 09:21:10.775701
81	2017-10-26 09:55:10.164227	lemiszewski@gmx.com	JL Testing	0.1.0	2	Study version updated	2017-10-26 09:55:10.213979	2017-10-26 09:55:10.213979
82	2017-10-26 09:56:01.937552	lemiszewski@gmx.com	JL Testing	0.1.0	2	Study version updated	2017-10-26 09:56:01.945435	2017-10-26 09:56:01.945435
83	2017-10-26 10:40:40.835791	lemiszewski@gmx.com	JL Testing	0.1.0	2	Study version updated	2017-10-26 10:40:40.842776	2017-10-26 10:40:40.842776
84	2017-10-26 10:44:55.609931	lemiszewski@gmx.com	JL Testing	0.1.0	2	Study version updated	2017-10-26 10:44:55.610838	2017-10-26 10:44:55.610838
85	2017-10-26 10:50:12.440642	lemiszewski@gmx.com	JL Testing	0.1.0	2	Study version updated	2017-10-26 10:50:12.441407	2017-10-26 10:50:12.441407
86	2017-10-26 10:50:57.217252	lemiszewski@gmx.com	JL Testing	0.1.0	2	Study version updated	2017-10-26 10:50:57.218033	2017-10-26 10:50:57.218033
87	2017-10-26 10:53:04.508834	lemiszewski@gmx.com	JL Testing	0.1.0	2	Study version updated	2017-10-26 10:53:04.509754	2017-10-26 10:53:04.509754
88	2017-10-26 11:25:09.273393	lemiszewski@gmx.com	JL Testing	0.1.0	2	Study version updated	2017-10-26 11:25:09.2741	2017-10-26 11:25:09.2741
89	2017-10-26 11:26:35.272438	lemiszewski@gmx.com	JL Testing	0.1.0	2	Study version updated	2017-10-26 11:26:35.273308	2017-10-26 11:26:35.273308
90	2017-10-26 18:06:35.609601	lemiszewski@gmx.com			4	User logged in.	2017-10-26 18:06:35.615028	2017-10-26 18:06:35.615028
91	2017-10-26 18:06:43.135632	lemiszewski@gmx.com	JL Testing	0.1.0	2	Study version updated	2017-10-26 18:06:43.136363	2017-10-26 18:06:43.136363
92	2017-10-26 18:17:53.632976	lemiszewski@gmx.com	JL Testing	0.1.0	2	Study version updated	2017-10-26 18:17:53.633697	2017-10-26 18:17:53.633697
93	2017-10-26 18:20:34.597428	lemiszewski@gmx.com	JL Testing	0.1.0	2	Study version updated	2017-10-26 18:20:34.598371	2017-10-26 18:20:34.598371
94	2017-10-26 18:54:06.81281	lemiszewski@gmx.com	JL Testing	0.1.0	2	Study version updated	2017-10-26 18:54:06.813464	2017-10-26 18:54:06.813464
95	2017-10-26 18:58:09.570714	lemiszewski@gmx.com	JL Testing	0.1.0	2	Study version updated	2017-10-26 18:58:09.571986	2017-10-26 18:58:09.571986
96	2017-10-26 18:59:15.329024	lemiszewski@gmx.com	JL Testing	0.1.0	2	Study version updated	2017-10-26 18:59:15.329861	2017-10-26 18:59:15.329861
97	2017-10-26 20:03:59.616182	lemiszewski@gmx.com	JL Testing	0.1.0	2	Study version updated	2017-10-26 20:03:59.622846	2017-10-26 20:03:59.622846
98	2017-10-26 20:15:53.814926	lemiszewski@gmx.com	JL Testing	0.1.0	2	Study version updated	2017-10-26 20:15:53.815834	2017-10-26 20:15:53.815834
99	2017-10-26 20:18:21.643341	lemiszewski@gmx.com	JL Testing	0.1.0	2	Study version updated	2017-10-26 20:18:21.644019	2017-10-26 20:18:21.644019
100	2017-10-27 06:41:12.355221	lemiszewski@gmx.com			4	User logged in.	2017-10-27 06:41:12.360504	2017-10-27 06:41:12.360504
101	2017-10-27 06:41:12.423138	lemiszewski@gmx.com	JL Testing	0.1.0	2	Study version updated	2017-10-27 06:41:12.423716	2017-10-27 06:41:12.423716
102	2017-10-27 07:53:44.184489	lemiszewski@gmx.com	JL Testing	0.1.0	2	Study version updated	2017-10-27 07:53:44.185606	2017-10-27 07:53:44.185606
103	2017-10-27 07:56:54.48894	lemiszewski@gmx.com	JL Testing	0.1.0	2	Study version updated	2017-10-27 07:56:54.489613	2017-10-27 07:56:54.489613
104	2017-10-27 08:12:04.498211	lemiszewski@gmx.com	JL Testing	0.1.0	2	Study version updated	2017-10-27 08:12:04.498946	2017-10-27 08:12:04.498946
105	2017-10-27 08:21:52.517432	lemiszewski@gmx.com	JL Testing	0.1.0	2	Study version updated	2017-10-27 08:21:52.517971	2017-10-27 08:21:52.517971
106	2017-10-27 08:48:17.652942	lemiszewski@gmx.com	JL Testing	0.1.0	2	Study version updated	2017-10-27 08:48:17.653597	2017-10-27 08:48:17.653597
107	2017-10-27 10:48:09.339682	lemiszewski@gmx.com	JL Testing	0.1.0	2	Study version updated	2017-10-27 10:48:09.340218	2017-10-27 10:48:09.340218
108	2017-10-27 11:12:25.912974	lemiszewski@gmx.com	JL Testing	0.1.0	2	Study version updated	2017-10-27 11:12:25.913707	2017-10-27 11:12:25.913707
109	2017-10-27 11:15:17.123859	lemiszewski@gmx.com	JL Testing	0.1.0	2	Study version updated	2017-10-27 11:15:17.124778	2017-10-27 11:15:17.124778
110	2017-10-27 11:17:39.036305	lemiszewski@gmx.com	JL Testing	0.1.0	2	Study version updated	2017-10-27 11:17:39.037034	2017-10-27 11:17:39.037034
111	2017-10-27 11:18:20.739622	lemiszewski@gmx.com	JL Testing	0.1.0	2	Study version updated	2017-10-27 11:18:20.742459	2017-10-27 11:18:20.742459
112	2017-10-27 11:18:36.927143	lemiszewski@gmx.com	JL Testing	0.1.0	2	Study version updated	2017-10-27 11:18:36.928237	2017-10-27 11:18:36.928237
113	2017-10-27 11:18:37.099017	lemiszewski@gmx.com	JL Testing	0.1.0	2	Study version updated	2017-10-27 11:18:37.09995	2017-10-27 11:18:37.09995
114	2017-10-27 11:20:27.956857	lemiszewski@gmx.com	JL Testing	0.1.0	2	Study version updated	2017-10-27 11:20:27.957858	2017-10-27 11:20:27.957858
115	2017-10-27 11:30:41.891067	lemiszewski@gmx.com	JL Testing	0.1.0	2	Study version updated	2017-10-27 11:30:41.8917	2017-10-27 11:30:41.8917
116	2017-10-27 11:30:50.432774	lemiszewski@gmx.com	JL Testing	0.1.0	2	Study version updated	2017-10-27 11:30:50.438819	2017-10-27 11:30:50.438819
117	2017-10-27 11:34:39.216177	lemiszewski@gmx.com	JL Testing	0.1.0	2	Study version updated	2017-10-27 11:34:39.216851	2017-10-27 11:34:39.216851
118	2017-10-27 11:36:20.388268	lemiszewski@gmx.com	JL Testing	0.1.0	2	Study version updated	2017-10-27 11:36:20.389197	2017-10-27 11:36:20.389197
119	2017-10-27 11:43:00.509005	lemiszewski@gmx.com	JL Testing	0.1.0	2	Study version updated	2017-10-27 11:43:00.515085	2017-10-27 11:43:00.515085
120	2017-10-27 11:46:53.631865	lemiszewski@gmx.com	JL Testing	0.1.0	2	Study version updated	2017-10-27 11:46:53.632779	2017-10-27 11:46:53.632779
121	2017-10-27 13:34:54.901088	lemiszewski@gmx.com			4	User logged in.	2017-10-27 13:34:54.902292	2017-10-27 13:34:54.902292
122	2017-10-27 13:34:54.93202	lemiszewski@gmx.com	JL Testing	0.1.0	2	Study version updated	2017-10-27 13:34:54.932627	2017-10-27 13:34:54.932627
123	2017-10-27 13:35:02.022794	lemiszewski@gmx.com	JL Testing	0.1.0	2	Study version updated	2017-10-27 13:35:02.023787	2017-10-27 13:35:02.023787
124	2017-10-27 13:36:11.374641	lemiszewski@gmx.com	JL Testing	0.1.0	2	Study version updated	2017-10-27 13:36:11.380776	2017-10-27 13:36:11.380776
125	2017-10-27 13:36:36.496752	lemiszewski@gmx.com	JL Testing	0.1.0	2	Study version updated	2017-10-27 13:36:36.497503	2017-10-27 13:36:36.497503
126	2017-10-27 13:46:21.468204	lemiszewski@gmx.com	JL Testing	0.1.0	2	Study version updated	2017-10-27 13:46:21.469627	2017-10-27 13:46:21.469627
127	2017-10-27 13:55:05.665494	lemiszewski@gmx.com	JL Testing	0.1.0	2	Study version updated	2017-10-27 13:55:05.672451	2017-10-27 13:55:05.672451
128	2017-10-27 14:14:10.57471	lemiszewski@gmx.com	JL Testing	0.1.0	2	Study version updated	2017-10-27 14:14:10.577497	2017-10-27 14:14:10.577497
129	2017-10-29 17:34:04.592593	lemiszewski@gmx.com			4	User logged in.	2017-10-29 17:34:04.600499	2017-10-29 17:34:04.600499
130	2017-10-29 17:34:04.682046	lemiszewski@gmx.com	JL Testing	0.1.0	2	Study version updated	2017-10-29 17:34:04.683341	2017-10-29 17:34:04.683341
131	2017-10-29 17:36:30.642234	lemiszewski@gmx.com	JL Testing	0.1.0	2	Study version updated	2017-10-29 17:36:30.649088	2017-10-29 17:36:30.649088
132	2017-10-29 18:51:38.874031	lemiszewski@gmx.com	JL Testing	0.1.0	2	Study version updated	2017-10-29 18:51:38.874661	2017-10-29 18:51:38.874661
133	2017-10-29 18:53:13.417457	lemiszewski@gmx.com	JL Testing	0.1.0	2	Study version updated	2017-10-29 18:53:13.418175	2017-10-29 18:53:13.418175
134	2017-10-29 19:09:02.856559	lemiszewski@gmx.com	JL Testing	0.1.0	2	Study version updated	2017-10-29 19:09:02.857163	2017-10-29 19:09:02.857163
135	2017-10-29 19:09:59.400523	lemiszewski@gmx.com	JL Testing	0.1.0	2	Study version updated	2017-10-29 19:09:59.407172	2017-10-29 19:09:59.407172
136	2017-10-29 19:13:43.321002	lemiszewski@gmx.com	JL Testing	0.1.0	2	Study version updated	2017-10-29 19:13:43.321905	2017-10-29 19:13:43.321905
137	2017-10-29 19:18:58.091006	lemiszewski@gmx.com	JL Testing	0.1.0	2	Study version updated	2017-10-29 19:18:58.097474	2017-10-29 19:18:58.097474
138	2017-10-29 19:20:57.483345	lemiszewski@gmx.com	JL Testing	0.1.0	2	Study version updated	2017-10-29 19:20:57.489436	2017-10-29 19:20:57.489436
139	2017-10-29 19:25:11.510588	lemiszewski@gmx.com	JL Testing	0.1.0	2	Study version updated	2017-10-29 19:25:11.511689	2017-10-29 19:25:11.511689
140	2017-10-29 19:25:25.671366	lemiszewski@gmx.com	JL Testing	0.1.0	2	Study version updated	2017-10-29 19:25:25.672456	2017-10-29 19:25:25.672456
141	2017-10-29 19:25:51.811618	lemiszewski@gmx.com	JL Testing	0.1.0	2	Study version updated	2017-10-29 19:25:51.818774	2017-10-29 19:25:51.818774
142	2017-10-29 19:28:06.653715	lemiszewski@gmx.com	JL Testing	0.1.0	2	Study version updated	2017-10-29 19:28:06.65437	2017-10-29 19:28:06.65437
143	2017-10-29 21:11:26.129171	lemiszewski@gmx.com			4	User logged in.	2017-10-29 21:11:26.136497	2017-10-29 21:11:26.136497
144	2017-10-29 21:11:26.200694	lemiszewski@gmx.com	JL Testing	0.1.0	2	Study version updated	2017-10-29 21:11:26.201319	2017-10-29 21:11:26.201319
\.


--
-- Name: audit_trails_id_seq; Type: SEQUENCE SET; Schema: public; Owner: tourmalet
--

SELECT pg_catalog.setval('audit_trails_id_seq', 144, true);


--
-- Data for Name: elements; Type: TABLE DATA; Schema: public; Owner: tourmalet
--

COPY elements (id, study_version_id, form_id, visit_id, created_at, updated_at) FROM stdin;
18	2	2	6	2017-10-26 09:38:46.563381	2017-10-26 09:38:46.563381
21	2	2	103	2017-10-26 18:21:01.680122	2017-10-26 18:21:01.680122
26	2	2	102	2017-10-29 18:51:55.069309	2017-10-29 18:51:55.069309
27	2	2	104	2017-10-29 18:51:55.738485	2017-10-29 18:51:55.738485
28	2	2	105	2017-10-29 18:51:56.330012	2017-10-29 18:51:56.330012
29	2	2	106	2017-10-29 18:51:56.849981	2017-10-29 18:51:56.849981
30	2	2	107	2017-10-29 18:51:57.334385	2017-10-29 18:51:57.334385
31	2	2	108	2017-10-29 18:51:57.831182	2017-10-29 18:51:57.831182
32	2	2	109	2017-10-29 18:51:58.448448	2017-10-29 18:51:58.448448
\.


--
-- Name: elements_id_seq; Type: SEQUENCE SET; Schema: public; Owner: tourmalet
--

SELECT pg_catalog.setval('elements_id_seq', 32, true);


--
-- Data for Name: forms; Type: TABLE DATA; Schema: public; Owner: tourmalet
--

COPY forms (id, study_version_id, form_id, form_namespace, identifier, ordinal, created_at, updated_at, label) FROM stdin;
2	2	F-ACME_FB000010	http://www.assero.co.uk/MDRForms/ACME/V3	FB000010	1	2017-10-23 21:34:48.580672	2017-10-23 21:34:48.580672	Demographics (Pilot)
\.


--
-- Name: forms_id_seq; Type: SEQUENCE SET; Schema: public; Owner: tourmalet
--

SELECT pg_catalog.setval('forms_id_seq', 6, true);


--
-- Data for Name: old_passwords; Type: TABLE DATA; Schema: public; Owner: tourmalet
--

COPY old_passwords (id, encrypted_password, password_salt, password_archivable_type, password_archivable_id, created_at) FROM stdin;
\.


--
-- Name: old_passwords_id_seq; Type: SEQUENCE SET; Schema: public; Owner: tourmalet
--

SELECT pg_catalog.setval('old_passwords_id_seq', 1, false);


--
-- Data for Name: schema_migrations; Type: TABLE DATA; Schema: public; Owner: tourmalet
--

COPY schema_migrations (version) FROM stdin;
20160115062817
20160115075700
20160115075736
20160116062817
20160806112954
20160806113443
20160825123908
20160829115312
20170215074254
20170215081944
20170215082029
20170216082750
20170217075132
20170217075524
20170217075636
20170217080118
20170217080245
20170217080255
20170217080303
20170217081452
20170217085239
20170218113324
20170226113151
20170226113452
\.


--
-- Data for Name: studies; Type: TABLE DATA; Schema: public; Owner: tourmalet
--

COPY studies (id, created_at, updated_at, label, identifier) FROM stdin;
2	2017-10-23 21:31:35.467674	2017-10-23 21:31:35.467674	Testing	JL Testing
\.


--
-- Name: studies_id_seq; Type: SEQUENCE SET; Schema: public; Owner: tourmalet
--

SELECT pg_catalog.setval('studies_id_seq', 2, true);


--
-- Data for Name: study_versions; Type: TABLE DATA; Schema: public; Owner: tourmalet
--

COPY study_versions (id, study_id, name, description, protocol_name, state, version, semantic_version, created_at, updated_at) FROM stdin;
2	2	Testing	To be set	To be set	1	1	0.1.0	2017-10-23 21:32:31.441155	2017-10-23 21:32:31.441155
\.


--
-- Name: study_versions_id_seq; Type: SEQUENCE SET; Schema: public; Owner: tourmalet
--

SELECT pg_catalog.setval('study_versions_id_seq', 2, true);


--
-- Data for Name: tokens; Type: TABLE DATA; Schema: public; Owner: tourmalet
--

COPY tokens (id, locked_at, refresh_count, study_version_id, item, item_info, user_id, created_at, updated_at) FROM stdin;
242	2017-10-29 21:11:26.19763	0	\N	JL Testing	0.1.0	2	2017-10-29 21:11:26.198436	2017-10-29 21:11:26.198436
\.


--
-- Name: tokens_id_seq; Type: SEQUENCE SET; Schema: public; Owner: tourmalet
--

SELECT pg_catalog.setval('tokens_id_seq', 244, true);


--
-- Data for Name: user_settings; Type: TABLE DATA; Schema: public; Owner: tourmalet
--

COPY user_settings (id, name, value, user_id, created_at, updated_at) FROM stdin;
\.


--
-- Name: user_settings_id_seq; Type: SEQUENCE SET; Schema: public; Owner: tourmalet
--

SELECT pg_catalog.setval('user_settings_id_seq', 1, false);


--
-- Data for Name: users; Type: TABLE DATA; Schema: public; Owner: tourmalet
--

COPY users (id, email, encrypted_password, reset_password_token, reset_password_sent_at, remember_created_at, sign_in_count, current_sign_in_at, last_sign_in_at, current_sign_in_ip, last_sign_in_ip, created_at, updated_at, access_level, name, password_changed_at) FROM stdin;
1	daveih1664@gmail.com	$2a$11$RG0QHs9gokoy.eMdOnl2PO.LbLVrvV8GZl/PZxAHo4vlT5gwmjfFq	\N	\N	\N	0	\N	\N	\N	\N	2017-10-23 09:33:10.047744	2017-10-23 09:33:10.047744	2	Dave IH	2017-10-23 09:33:10.046937
2	lemiszewski@gmx.com	$2a$11$gSNkGa4PQZg/GJGwXXuRreTQ5.4TXCZdM77U8DaNG4QQmB9rFxtpS	\N	\N	\N	12	2017-10-29 21:11:26.091243	2017-10-29 17:34:04.55579	127.0.0.1	127.0.0.1	2017-10-23 09:33:10.222877	2017-10-29 21:11:26.092309	2	Jakub LE	2017-10-23 09:33:10.222701
\.


--
-- Name: users_id_seq; Type: SEQUENCE SET; Schema: public; Owner: tourmalet
--

SELECT pg_catalog.setval('users_id_seq', 2, true);


--
-- Data for Name: visits; Type: TABLE DATA; Schema: public; Owner: tourmalet
--

COPY visits (id, study_version_id, short_name, long_name, timing, ordinal, created_at, updated_at) FROM stdin;
102	2	V2	Visit 2	Set Timing ...	2	2017-10-26 18:18:27.244486	2017-10-26 18:18:27.244486
103	2	V3	Visit 3	Set Timing ...	3	2017-10-26 18:18:28.384297	2017-10-26 18:18:28.384297
104	2	V4	Visit 4	Set Timing ...	4	2017-10-26 18:18:28.864897	2017-10-26 18:18:28.864897
105	2	V5	Visit 5	Set Timing ...	5	2017-10-26 18:18:29.410327	2017-10-26 18:18:29.410327
106	2	V6	Visit 6	Set Timing ...	6	2017-10-26 18:18:29.843004	2017-10-26 18:18:29.843004
107	2	V7	Visit 7	Set Timing ...	7	2017-10-27 07:58:01.672005	2017-10-27 07:58:01.672005
108	2	V8	Visit 8	Set Timing ...	8	2017-10-27 07:58:01.831018	2017-10-27 07:58:01.831018
6	2	V1	Visit 1	Set Timing ...	1	2017-10-23 21:41:53.366886	2017-10-23 21:41:53.366886
109	2	V9	Visit 9	Set Timing ...	9	2017-10-27 07:58:02.019388	2017-10-27 07:58:02.019388
\.


--
-- Name: visits_id_seq; Type: SEQUENCE SET; Schema: public; Owner: tourmalet
--

SELECT pg_catalog.setval('visits_id_seq', 111, true);


--
-- Name: amendments amendments_pkey; Type: CONSTRAINT; Schema: public; Owner: tourmalet
--

ALTER TABLE ONLY amendments
    ADD CONSTRAINT amendments_pkey PRIMARY KEY (id);


--
-- Name: audit_trails audit_trails_pkey; Type: CONSTRAINT; Schema: public; Owner: tourmalet
--

ALTER TABLE ONLY audit_trails
    ADD CONSTRAINT audit_trails_pkey PRIMARY KEY (id);


--
-- Name: elements elements_pkey; Type: CONSTRAINT; Schema: public; Owner: tourmalet
--

ALTER TABLE ONLY elements
    ADD CONSTRAINT elements_pkey PRIMARY KEY (id);


--
-- Name: forms forms_pkey; Type: CONSTRAINT; Schema: public; Owner: tourmalet
--

ALTER TABLE ONLY forms
    ADD CONSTRAINT forms_pkey PRIMARY KEY (id);


--
-- Name: old_passwords old_passwords_pkey; Type: CONSTRAINT; Schema: public; Owner: tourmalet
--

ALTER TABLE ONLY old_passwords
    ADD CONSTRAINT old_passwords_pkey PRIMARY KEY (id);


--
-- Name: studies studies_pkey; Type: CONSTRAINT; Schema: public; Owner: tourmalet
--

ALTER TABLE ONLY studies
    ADD CONSTRAINT studies_pkey PRIMARY KEY (id);


--
-- Name: study_versions study_versions_pkey; Type: CONSTRAINT; Schema: public; Owner: tourmalet
--

ALTER TABLE ONLY study_versions
    ADD CONSTRAINT study_versions_pkey PRIMARY KEY (id);


--
-- Name: tokens tokens_pkey; Type: CONSTRAINT; Schema: public; Owner: tourmalet
--

ALTER TABLE ONLY tokens
    ADD CONSTRAINT tokens_pkey PRIMARY KEY (id);


--
-- Name: user_settings user_settings_pkey; Type: CONSTRAINT; Schema: public; Owner: tourmalet
--

ALTER TABLE ONLY user_settings
    ADD CONSTRAINT user_settings_pkey PRIMARY KEY (id);


--
-- Name: users users_pkey; Type: CONSTRAINT; Schema: public; Owner: tourmalet
--

ALTER TABLE ONLY users
    ADD CONSTRAINT users_pkey PRIMARY KEY (id);


--
-- Name: visits visits_pkey; Type: CONSTRAINT; Schema: public; Owner: tourmalet
--

ALTER TABLE ONLY visits
    ADD CONSTRAINT visits_pkey PRIMARY KEY (id);


--
-- Name: index_amendments_on_study_version_id; Type: INDEX; Schema: public; Owner: tourmalet
--

CREATE INDEX index_amendments_on_study_version_id ON amendments USING btree (study_version_id);


--
-- Name: index_forms_on_study_version_id; Type: INDEX; Schema: public; Owner: tourmalet
--

CREATE INDEX index_forms_on_study_version_id ON forms USING btree (study_version_id);


--
-- Name: index_password_archivable; Type: INDEX; Schema: public; Owner: tourmalet
--

CREATE INDEX index_password_archivable ON old_passwords USING btree (password_archivable_type, password_archivable_id);


--
-- Name: index_studies_on_identifier; Type: INDEX; Schema: public; Owner: tourmalet
--

CREATE UNIQUE INDEX index_studies_on_identifier ON studies USING btree (identifier);


--
-- Name: index_user_settings_on_user_id; Type: INDEX; Schema: public; Owner: tourmalet
--

CREATE INDEX index_user_settings_on_user_id ON user_settings USING btree (user_id);


--
-- Name: index_users_on_email; Type: INDEX; Schema: public; Owner: tourmalet
--

CREATE UNIQUE INDEX index_users_on_email ON users USING btree (email);


--
-- Name: index_users_on_password_changed_at; Type: INDEX; Schema: public; Owner: tourmalet
--

CREATE INDEX index_users_on_password_changed_at ON users USING btree (password_changed_at);


--
-- Name: index_users_on_reset_password_token; Type: INDEX; Schema: public; Owner: tourmalet
--

CREATE UNIQUE INDEX index_users_on_reset_password_token ON users USING btree (reset_password_token);


--
-- Name: index_visits_on_study_version_id; Type: INDEX; Schema: public; Owner: tourmalet
--

CREATE INDEX index_visits_on_study_version_id ON visits USING btree (study_version_id);


--
-- Name: unique_schema_migrations; Type: INDEX; Schema: public; Owner: tourmalet
--

CREATE UNIQUE INDEX unique_schema_migrations ON schema_migrations USING btree (version);


--
-- Name: user_settings fk_rails_d1371c6356; Type: FK CONSTRAINT; Schema: public; Owner: tourmalet
--

ALTER TABLE ONLY user_settings
    ADD CONSTRAINT fk_rails_d1371c6356 FOREIGN KEY (user_id) REFERENCES users(id);


--
-- PostgreSQL database dump complete
--

