# Be sure to restart your server when you modify this file.

# Version of your assets, change this if you want to expire all your assets.
Rails.application.config.assets.version = '1.0'

# Add additional assets to the asset load path
# Rails.application.config.assets.paths << Emoji.images_path

# Precompile additional assets.
# application.js, application.css, and all non-JS/CSS in app/assets folder are already added.
Rails.application.config.assets.precompile += %w( 
	d3_tree.js d3_editor.js 
	standard_datatable.js export.js
	sve_main.js sve_study.js sve_soa_table.js sve_soa_form.js sve_soa_visit.js
	sve_study_detail.js sve_progress.js
	sve_crf.js sve_acrf.js token_timer.js
	general/iso_mi_select_panel.js
)
Rails.application.config.assets.precompile += %w( d3_tree.css print.css spinner.css )
