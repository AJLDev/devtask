Rails.application.routes.draw do

  # Root
  root to: 'studies#index'

  #devise_for :users
  devise_for :users, controllers: {sessions: "users/sessions"}
  resources :users, except: [:create] do
    member do
      put :update_name
      put :set_read
      put :set_edit
      put :set_system_admin
    end
  end
  post 'create_user' => 'users#create', as: :create_user

  # API
  namespace :api do
    namespace :v1 do
      resources :studies, only: [:index] do
        member do
          get :history
        end
        resources :study_versions, only: [:show] do
        end
      end
      resources :study_versions, only: [:show] do
        resources :forms, only: [:show]
      end
    end
  end

  # User Settings
  resources :user_settings

  resources :audit_trail, only: [:index] do
    collection do
      post :search
      get :export_csv
    end
  end

  # Tokens
  resources :tokens, only: [:index] do
    member do
      post :release
      get :status
      get :extend_token
    end
  end

  # Standard resources
  resources :amendments, only: [:update, :create, :destroy] do
    collection do
      get :find
    end
  end
  resources :studies, only: [:index, :new, :create, :destroy] do
    member do
      get :history
    end
  end
  match 'study_versions/:id/export' => 'study_versions#export', :via => [:get, :post]
  resources :study_versions, only: [:new, :create, :edit, :destroy, :show, :update] do
    member do
      get :status
      put :update_status
      get :soa
      put :clear
      get :export
      get :export_json
      get :export_odm
      get :export_als
      get :export_crf
      get :export_acrf
    end
    collection do
      post :export_crf
      post :export_acrf
    end
  end
  resources :forms do
    member do
      put 'move'
    end
  end
  resources :visits  do
    member do
      put 'move'
    end
  end
  resources :elements do
    collection do
      post 'toggle'
      get 'for_study'
      delete 'clear'
    end
  end
  resources :mdrs do
    collection do
      get 'forms_list'
      get 'forms_all'
      get 'forms'
      get 'form'
      get 'thesaurus_concept'
      get 'bc_property'
      get 'form_show'
      get 'form_annotations'
    end

  end
  namespace :mdrs do
    resources :forms, only: [] do
      member do
        get 'history'
        get 'latest'
      end
    end
  end
  resources :markdown_engines, only: [:create, :index]
end
