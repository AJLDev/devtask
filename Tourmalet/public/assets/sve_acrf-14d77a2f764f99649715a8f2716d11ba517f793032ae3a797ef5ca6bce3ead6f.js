// Study Version Editor: Annotated CRF
//

// Initialise
//
// @param [Integer] studyVersionId the id for the study version
// @return [void]
function SveAcrf(studyVersionId) {
    this.progress = new SveProgress('#aCrfPb');
    this.studyVersionId = studyVersionId;
    this.html = []; // Array for resulting HTML

    var _this = this;

    // Button handlers
    $('#study_acrf').click(function() {
        $('#soa_table thead tr th, #soa_table tbody tr td').removeClass('success'); //success
        _this.start();
    });

    $('#export_acrf').click(function() {
        var table_data = $("<div />").append($("#soa_table").clone()).html()
        // window.open('/study_versions/' + studyVersionId + '/export?study_version[export_type]=acrf&table_data='+table_data);
        $("#soa_table_data").val(table_data)
        $('#export_type').val("acrf")
        $("#report_form").submit()
    });
}

// Start the CRF build
//
// @return [void]
SveAcrf.prototype.start = function() {
    var _this = this;
    this.clear();
    $.ajax({
        url: "/study_versions/" + this.studyVersionId,
        type: 'GET',
        dataType: 'json',
        success: function(result){
            var forms = result.data.children;
            if (forms.length > 0) {
                _this.progress.clear(forms.length); // Set the progress total
                var table = $("<div />").append($("#soa_table").clone()).html()
                _this.html.push(table);

                for (var i=0; i<forms.length; i++) {
                    _this.html.push(_this.placeholder(i, forms[i].label)); // Create the slot for the result.
                    _this.getForm(forms[i], i);
                }
                $("#aCrfHtml").append(result);
            } else {
                displayWarning("Study does not include any forms at present, nothing to display.");
            }
        },
        error: function(xhr,status,error){
            handleAjaxError(xhr, status, error);
        }
    });
}

// Display form
//
// @param [JS Object] form structure containing the form id and namespace
// @param [Integer] index the index of the form
// @return [void]
SveAcrf.prototype.getForm = function(form, index) {
    var _this = this;
    $.ajax({
        url: "/mdrs/form_annotations",
        data: { "id": form.form_id, "namespace": form.form_namespace },
        type: 'GET',
        dataType: 'html',
        success: function(result){
            _this.displayForm(result, index);
            setTimeout(function(){ $(".spinner_and_label_"+index).hide(); }, 500);
        },
        error: function(xhr,status,error){
            handleAjaxError(xhr, status, error);
        }
    });
}

// Form display
//
// @param [result] the form html
// @param [Integer] index the index of the form
// @return [void]
SveAcrf.prototype.displayForm = function(result, index) {
    this.html[index] = result; // Save the result in the correct slot
    $("#aCrfHtml").append(this.html.join('')); // Joint the array to form the whole page.
    this.progress.increment();
}

// Clear the CRF
//
// @return [void]
SveAcrf.prototype.clear = function() {
    this.progress.clear(0);
    this.html = [];
    $("#aCrfHtml").html("");
}

// Placeholder html for building the CRF
//
// @param [String] label the form label
// @return [String] the html placeholder
SveAcrf.prototype.placeholder = function(index, label) { return '<div class="row spinner_and_label_'+index+'"><div class="col-md-3 col-sm-4"><p><i class="fa fa-spinner fa-spin fa-lg fa-fw margin-bottom"></i></p></div>' +
    '<div class="col-md-9 col-sm-8"><p>Form: ' + label + ' will appear here ...</p></div></div>';
}

// Submit POST request using hidden form
//
// @param [String] url URL of POST request
// @param [Dictionary] data response data
SveAcrf.prototype.openWindowWithPost = function (url, data) {
    var form = document.createElement("form");
    form.target = "_blank";
    form.method = "POST";
    form.action = url;
    form.style.display = "none";

    for (var key in data) {
        var input = document.createElement("input");
        input.type = "hidden";
        input.name = key;
        input.value = data[key];
        form.appendChild(input);
    }

    document.body.appendChild(form);
    form.submit();
    document.body.removeChild(form);
}
;
