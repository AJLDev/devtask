/*
* Study Version Editor: SOA Forms
* 
* Requires:
* 
* soa_form_list_table_div [Div] the released form table Div
* soa_form_list_table [Table] the released form table
* soa_form_all_table_div [Div] the unreleased form table Div
* soa_form_all_table [Table] the unreleased form table
* soa_add_form [Button] the add form button
* soa_released_form_toggle [Button] toggle between all/released forms
*/


function SveSoaForms(studyVersionId, _callBackRef) {
  this.studyVersionId = studyVersionId;
  this._callBackRef = _callBackRef;
  this.formListTable = null;
  this.formListTableReload = false;
  this.formAllTable = null;
  this.formAllTableReload = false;
  this.buttonDisable();
  this.getFormAll();
  this.getFormList();
  this.useReleasedForms = true;
  this.toggleForm(false);

  var _this = this;
  
  $('#soa_form_list_table tbody').on('click', 'tr', function () {
    _this.formListClick(this);
  });

  $('#soa_form_all_table tbody').on('click', 'tr', function () {
    _this.formAllClick(this);
  });

  $('#soa_released_form_toggle').click(function() {
    _this.useReleasedForms = !_this.useReleasedForms;
    _this.toggleForm(true);
  });

  $('#soa_add_form').click(function() {
    _this.addForm();
  });

}

SveSoaForms.prototype.getFormList = function () {
  if (!this.formListTableReload) {
    this.formListTable = $('#soa_form_list_table').DataTable( {
      "ajax": {
        "type": 'GET',
        "url": "/mdrs/forms_list",
        "dataSrc": ""
      },
      "dataType": 'json',
      "bProcessing": true,
      "language": {
        "processing": "<img src='/assets/processing-9034d5d34015e4b05d2c1d1a8dc9f6ec9d59bd96d305eb9e24e24e65c591a645.gif'>"
      },
      error: function(xhr,status,error){
        handleAjaxError(xhr, status, error);
      },
      "columns": [
        {"data" : "scoped_identifier.identifier", "width" : "25%"},
        {"data" : "label", "width" : "50%"},
        {"data" : "scoped_identifier.version", "width" : "25%"}
      ]
    });
    this.formListTableReload = true;
  } else {
    formListTable.ajax.reload();
  }
} 

SveSoaForms.prototype.getFormAll = function () {
  if (!this.formAllTableReload) {
    this.formAllTable = $('#soa_form_all_table').DataTable( {
      "ajax": {
        "type": 'GET',
        "url": "/mdrs/forms_all",
        "dataSrc": ""
      },
      "dataType": 'json',
      "bProcessing": true,
      "language": {
        "processing": "<img src='/assets/processing-9034d5d34015e4b05d2c1d1a8dc9f6ec9d59bd96d305eb9e24e24e65c591a645.gif'>"
      },
      error: function(xhr,status,error){
        handleAjaxError(xhr, status, error);
      },
      "columns": [
        {"data" : "scoped_identifier.identifier", "width" : "25%"},
        {"data" : "label", "width" : "50%"},
        {"data" : "scoped_identifier.semantic_version", "width" : "25%"}
      ]
    });
    this.formAllTableReload = true;
  } else {
    formListTable.ajax.reload();
  }
} 

SveSoaForms.prototype.buttonEnable = function () {
  this.buttonState(false);
}

SveSoaForms.prototype.buttonDisable = function () {
  this.buttonState(true);
}

SveSoaForms.prototype.buttonState = function (state) {
  $("#soa_add_form").prop("disabled", state);
  $("#soa_released_form_toggle").prop("disabled", state);
}

SveSoaForms.prototype.toggleForm = function (alert) {
  var html;
  if (this.useReleasedForms) {
    $("#soa_form_list_table_div").show();
    $("#soa_form_all_table_div").hide();
    $("#soa_released_form_toggle").removeClass("btn-warning").addClass("btn-success");
    html = alertSuccess ("You are now using released forms.");
  } else {
    $("#soa_form_all_table_div").show();
    $("#soa_form_list_table_div").hide();
    $("#soa_released_form_toggle").removeClass("btn-success").addClass("btn-warning");
    html = alertWarning ("You are now using unreleased forms.");
  }
  if (this.formListCurrent != null) {
    $(this.formListCurrent).toggleClass('success');
  }
  if (this.formAllCurrent != null) {
    $(this.formAllCurrent).toggleClass('success');
  }
  this.formListCurrent = null;
  this.formAllCurrent = null;
  if (alert) {
    displayAlerts(html);
  }
}

SveSoaForms.prototype.addForm = function () {
  var row;
  var data;
  if (this.formListCurrent !== null) {
    row = this.formListTable.row(this.formListCurrent).index();
    data = this.formListTable.row(row).data();
  } else if (this.formAllCurrent !== null) {
    row = this.formAllTable.row(this.formAllCurrent).index();
    data = this.formAllTable.row(row).data();
  } else {
    displayWarning ("You need to select a form.");
    return;
  }
  this._callBackRef.addForm(data.id, data.namespace, data.scoped_identifier.identifier, data.label);
}

SveSoaForms.prototype.formListClick = function (_this) {
  if (this.formListCurrent != null) {
    $(this.formListCurrent).toggleClass('success');
  }
  $(_this).toggleClass('success');
  this.formListCurrent = _this;
}

SveSoaForms.prototype.formAllClick = function (_this) {
  if (this.formAllCurrent != null) {
    $(this.formAllCurrent).toggleClass('success');
  }
  $(_this).toggleClass('success');
  this.formAllCurrent = _this;
}
;
