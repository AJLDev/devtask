function SveStudyDetail(studyVersionId) {
  this.d3Editor = new D3Editor("d3", this.empty.bind(this), this.postClick.bind(this), this.empty.bind(this), this.validate.bind(this));
  this.studyRoot = null;
  this.progress = new SveProgress('#detailPb');
  this.studyVersionId = studyVersionId;
  this.selectNone();  

  var _this = this;

  // Button handlers
  $('#study_detail').click(function() {
    _this.start();
  });

  $('#updateQuestion').click(function() {
    _this.saveQuestion(d3eGetCurrent());
  });

  $('#export_json').click(function() {
    window.open('/study_versions/' + _this.studyVersionId + '/export?study_version[export_type]=json', '_blank');
  });

  $('#export_odm').click(function() {
    window.open('/study_versions/' + _this.studyVersionId + '/export?study_version[export_type]=odm', '_blank');
  });

  $('#export_als').click(function() {
    window.open('/study_versions/' + _this.studyVersionId + '/export?study_version[export_type]=als', '_blank');
  });
}

SveStudyDetail.prototype.start = function() {
  var _this = this;
  if (_this.studyRoot !== null) {
    _this.d3Editor.deleteChildren(this.studyRoot);
  } 
  _this.progress.clear(0);
  this.selectNone();  
  $.ajax({
    url: "/study_versions/" + _this.studyVersionId,
    type: 'GET',
    dataType: 'json',
    success: function(result){
      _this.study(result);
    },
    error: function(xhr,status,error){
      handleAjaxError(xhr, status, error);
    }
  });
}

SveStudyDetail.prototype.study = function(result) {
  var _this = this
  var study = result.data;
  _this.studyRoot = this.d3Editor.root(study.name, "", study);
  var forms = study.children;
  _this.progress.clear(forms.length);
  for (var i=0; i<forms.length; i++) {
    var id = forms[i].form_id;
    var namespace = forms[i].form_namespace;
    $.ajax({
      url: "/mdrs/form",
      data: { "id": id, "namespace": namespace },
      type: 'GET',
      dataType: 'json',
      success: function(result){
        _this.form(result);
      },
      error: function(xhr,status,error){
        handleAjaxError(xhr, status, error);
      }
    });   
  }
}

SveStudyDetail.prototype.form = function(result) {
  this.progress.increment();
  var form = $.parseJSON(JSON.stringify(result));
  var formNode = this.parseNode(form, this.studyRoot);
  this.getReferences(formNode);
  this.d3Editor.displayTree(this.studyRoot.key);
}

SveStudyDetail.prototype.parseNode = function(sourceNode, d3ParentNode) {
  var newNode;
  var i;
  var child;
  var enabled;
  // Update based on node type
  if (sourceNode.type == C_FORM) {
  } else if (sourceNode.type == C_GROUP) {
  } else if (sourceNode.type == C_BC_ITEM) {
  } else if (sourceNode.type == C_QUESTION) {
    // Save the default text, set enabled.
    sourceNode.default_question_text = sourceNode.question_text;
    sourceNode.enabled = true;
  } else if (sourceNode.type == C_PLACEHOLDER) {
  } else if (sourceNode.type == C_TC) {
  } else if (sourceNode.type == C_COMMON_GROUP) {
  }
  // Sort the enabled flag out. If no enabled option set one.
  enabled = true;
  if (sourceNode.hasOwnProperty('enabled')) {
    enabled = sourceNode.enabled
  }
  newNode = this.d3Editor.addNode(d3ParentNode, sourceNode.label, sourceNode.type, enabled, sourceNode, true);
  if (sourceNode.hasOwnProperty('children')) {
    for (i=0; i<sourceNode.children.length; i++) {
      child = sourceNode.children[i];
      this.parseNode(child, newNode);
    }
  }
  return newNode;
}

SveStudyDetail.prototype.getReferences = function(node) {
  var i;
  var child;
  var nodeData;
  nodeData = node.data;
  if (nodeData.type == C_TC) {
    this.getThesaurusConcept(node);
  } else if (nodeData.type == C_QUESTION) {
    this.getAmendment(node, this.questionAmendment.bind(this));
  } else if (node.type == C_BC_ITEM) {
    this.getBcProperty(node);
  }
  if (node.hasOwnProperty('children')) {
    for (i=0; i<node.children.length; i++) {
      child = node.children[i];
      this.getReferences(child);
    }
  } 
}

 // Get TC
SveStudyDetail.prototype.getThesaurusConcept = function(node) {
  _this = this;
  var nodeData = node.data;
  $.ajax({
    url: "/mdrs/thesaurus_concept/",
    type: "GET",
    data: { "id": nodeData.subject_ref.id, "namespace": nodeData.subject_ref.namespace },
    dataType: 'json',
    error: function (xhr, status, error) {
      displayError("An error has occurred loading a Terminology reference.");
    },
    success: function(result){
      nodeData.subject_data = result;
      node.name = result.label;
      _this.d3Editor.displayTree(1);
    }
  });
}

SveStudyDetail.prototype.getBcProperty = function(node) {
  _this = this;
  var nodeData = node.data
  $.ajax({
    url: "/mdrs/bc_property/",
    type: "GET",
    data: { "id": nodeData.property_ref.subject_ref.id, "namespace": nodeData.property_ref.subject_ref.namespace },
    dataType: 'json',
    error: function (xhr, status, error) {
      displayError("An error has occurred loading a Biomedical Concept reference.");
    },
    success: function(result){
      nodeData.subject_data = result;
      _this.d3Editor.displayTree(1);
    }
  });
}

SveStudyDetail.prototype.getAmendment = function(node, resultFunction) {
  var nodeData = node.data
  $.ajax({
    url: "/amendments/find",
    type: "GET",
    data: {"amendment": { "cid": nodeData.id, "namespace": nodeData.namespace }},
    dataType: 'json',
    error: function (xhr, status, error) {
      displayError("An error has occurred requestiung an amendment.");
    },
    success: function(result){
      resultFunction(node, $.parseJSON(JSON.stringify(result)));
    }
  });
}

SveStudyDetail.prototype.empty = function() {
}

SveStudyDetail.prototype.validate = function() {
  return true;
}

SveStudyDetail.prototype.postClick = function(node) {
  if (node.type == C_FORM) {
    this.selectForm();
    this.displayForm(node);
  } else if (node.type == C_GROUP) {
    this.selectGroup();
    this.displayGroup(node);
  } else if (node.type == C_BC_ITEM) {
    this.selectBcItem();
    this.displayBcItem(node);
  } else if (node.type == C_QUESTION) {
    this.selectQuestion();
    this.displayQuestion(node);
  } else if (node.type == C_PLACEHOLDER) {
    this.selectPlaceholder();
    this.displayPlaceholder(node);
  } else if (node.type == C_TC) {
    this.selectCl();
    this.displayCl(node);
  } else if (node.type == C_COMMON_GROUP) {
    this.selectCommon();
    this.displayCommon(node);
  }
}

SveStudyDetail.prototype.selectNone = function() {
  $(".sve-panel").addClass('hidden');
}

SveStudyDetail.prototype.selectForm = function() {
  $(".sve-panel").addClass('hidden');
  $("#sdeFormTable").removeClass('hidden');
}

SveStudyDetail.prototype.selectGroup = function() {
  $(".sve-panel").addClass('hidden');
  $("#sdeGroupTable").removeClass('hidden');
}

SveStudyDetail.prototype.selectBcItem = function() {
  $(".sve-panel").addClass('hidden');
  $("#sdeBcItemTable").removeClass('hidden');
}

SveStudyDetail.prototype.selectQuestion = function() {
  $(".sve-panel").addClass('hidden');
  $("#sdeQuestionTable").removeClass('hidden');
}

SveStudyDetail.prototype.selectPlaceholder = function() {
  $(".sve-panel").addClass('hidden');
  $("#sdePlaceholderTable").removeClass('hidden');
}

SveStudyDetail.prototype.selectCl = function() {
  $(".sve-panel").addClass('hidden');
  $("#sdeClTable").removeClass('hidden');
}

SveStudyDetail.prototype.selectCommon = function() {
  $(".sve-panel").addClass('hidden');
  $("#sdeCommonTable").removeClass('hidden');
}

SveStudyDetail.prototype.displayForm = function(node) {
  $("#sdeFormIdentifier").html(node.data.scoped_identifier.identifier);
  $("#sdeFormLabel").html(node.data.label);
  getMarkdown(document.getElementById("sdeFormCompletion"), node.data.completion);
  getMarkdown(document.getElementById("sdeFormNote"), node.data.note);
}

SveStudyDetail.prototype.displayGroup = function(node) {
  $("#sdeGroupLabel").html(node.data.label);
  $("#sdeGroupRepeating").html(this.yesNo(node.data.repeating));
  $("#sdeGroupOptional").html(this.yesNo(node.data.optional));
  getMarkdown(document.getElementById("sdeGroupCompletion"), node.data.completion);
  getMarkdown(document.getElementById("sdeGroupNote"), node.data.note);
}

SveStudyDetail.prototype.displayBcItem = function(node) {
  $("#sdeBcItemLabel").html(node.data.label);
  $("#sdeBcItemEnabled").html(this.yesNo(node.data.enabled));
  $("#sdeBcItemOptional").html(this.yesNo(node.data.optional));
  $("#sdeBcItemQText").html(node.data.subject_data.question_text);
  $("#sdeBcItemDatatype").html(node.data.subject_data.datatype);
  $("#sdeBcItemFormat").html(node.data.subject_data.format);
  getMarkdown(document.getElementById("sdeBcItemCompletion"), node.data.completion);
  getMarkdown(document.getElementById("sdeBcItemNote"), node.data.note);
}

SveStudyDetail.prototype.displayQuestion = function(node) {
  $("#sdeQuestionText").val(node.data.question_text);
  if (node.data.optional) {
    $("#sdeQuestionEnabled").prop('checked', node.data.enabled);
    $("#sdeQuestionEnabled").prop("disabled", false);
  } else {
    $("#sdeQuestionEnabled").prop('checked', true);
    $("#sdeQuestionEnabled").prop("disabled", true);
  }
  $("#sdeQuestionLabel").html(node.data.label);
  $("#sdeQuestionOptional").html(this.yesNo(node.data.optional));
  $("#sdeQuestionMapping").html(node.data.mapping);
  $("#sdeQuestionDatatype").html(node.data.datatype);
  $("#sdeQuestionFormat").html(node.data.format);
  getMarkdown(document.getElementById("sdeQuestionCompletion"), node.data.completion);
  getMarkdown(document.getElementById("sdeQuestionNote"), node.data.note);
}

SveStudyDetail.prototype.saveQuestion = function(node) {
  node.data.question_text = this.updateProperty(node, $("#sdeQuestionText").val(), node.data.question_text, node.data.default_question_text, node.data.id, node.data.namespace, "QuestionText", "string")
  node.data.enabled = this.updateProperty(node, $("#sdeQuestionEnabled").prop('checked'), node.data.enabled, true, node.data.id, node.data.namespace, "Enabled", "string")
}

SveStudyDetail.prototype.questionAmendment = function(node, result) {
  //
  var data;
  var amendment;
  data = result.data;
  for (i=0; i<data.length; i++) {
    amendment = data[i];
    if (amendment.property == "QuestionText") {
      node.data.question_text = amendment.study_value;
    } else if (amendment.property == "Enabled") {
      node.data.enabled == amendment.study_value
    }
  }
}

SveStudyDetail.prototype.displayPlaceholder = function(node) {
  $("#sdePlaceholderLabel").html(node.data.label);
  $("#sdePlaceholderOptional").html(node.data.optional);
  getMarkdown(document.getElementById("sdePlaceholderFreeText"), node.data.free_text)
  getMarkdown(document.getElementById("sdePlaceholderCompletion"), node.data.completion);
  getMarkdown(document.getElementById("sdePlaceholderNote"), node.data.note);
}

SveStudyDetail.prototype.displayCl = function(node) {
  $("#sdeClIdentifier").html(node.data.subject_data.identifier);
  $("#sdeClLabel").html(node.data.subject_data.label);
  $("#sdeClDefaultLabel").html(node.data.local_label);
  $("#sdeClSubmission").html(node.data.subject_data.notation);
  $("#sdeClEnabled").html(this.yesNo(node.data.enabled));;
  $("#sdeClOptional").html(this.yesNo(node.data.optional));;
}

SveStudyDetail.prototype.displayCommon = function(node) {
  $("#sdeCommonLabel").html(node.data.label);
}

SveStudyDetail.prototype.updateProperty = function(node, newValue, currentValue, defaultValue, id, namespace, property, datatype) {
  if (newValue != currentValue) {
    saveAmendment(node, id, namespace, property, newValue, defaultValue, datatype)
  }
  return newValue;
}

SveStudyDetail.prototype.saveAmendment = function(node, id, namespace, property, value, default_value, datatype) {
  if (node.hasOwnProperty('id')) {
    $.ajax({
      url: "/amendments/" + node.id,
      data: { "amendment": { "study_id": currentStudyId, "cid": id, "namespace": namespace, "property": property, "study_value": value, "default_value": default_value, "datatype": datatype }},
      type: 'PUT',
      dataType: 'json',
      success: function(result){
      },
      error: function(xhr,status,error) {
        handleAjaxError(xhr, status, error);
      }
    });  
  } else {
    $.ajax({
      url: "/amendments",
      data: { "amendment": { "study_id": currentStudyId, "cid": id, "namespace": namespace, "property": property, "study_value": value, "default_value": default_value, "datatype": datatype }},
      type: 'POST',
      dataType: 'json',
      success: function(result){
        var data = $.parseJSON(JSON.stringify(result));
        node.id = data["id"];
      },
      error: function(xhr,status,error) {
        handleAjaxError(xhr, status, error);
      }
    });
  }  
}

SveStudyDetail.prototype.yesNo = function(value) {
  if (value) {
    return "<span class=\"glyphicon glyphicon-ok text-success\"></span>";
  } else {
    return "<span class=\"glyphicon glyphicon-remove text-danger\"></span>";
  }
}
;
