$(document).ready( function() {
    $('#waiting').addClass('glyphicon-spin');
    $('#running_div').show();
    $('#finished_div').hide();
    $('#error_div').hide();
    //window.open(export_path, '_blank');
    if(export_lock_id !== "0") {
        statusTimeout(export_lock_id, 1000);
        //window.location.href = export_path;
        $('#preview_form').submit();
    } else {
        $('#waiting').removeClass('glyphicon-spin');
        $('#running_div').hide();
        $('#error_div').show();
    }
});

function statusTimeout(id, period) {
    setTimeout(function(){
        getStatus(id);
    }, period);
}

function getStatus(id) {
    $.ajax({
        url: "/tokens/" + id + "/status",
        type: "GET",
        dataType: 'json',
        error: function (xhr, status, error) {
            displayError("An error occured obtainig the download status.")
            $('#waiting').removeClass('glyphicon-spin');
            $('#running_div').hide();
            $('#error_div').show();
        },
        success: function(result){
            if (result.running) {
                statusTimeout(export_lock_id, 1000);
            } else {
                $('#waiting').removeClass('glyphicon-spin');
                $('#running_div').hide();
                $('#finished_div').show();
                $('.sk-fading-circle').hide();
            }
        }
    });
}
