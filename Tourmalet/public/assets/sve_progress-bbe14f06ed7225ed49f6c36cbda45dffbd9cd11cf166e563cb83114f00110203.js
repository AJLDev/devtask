// Study Version Editor: Progress Bar
//

// Initialise
//
// @param [String] name the identifier name of the progress div (needs to include '#')
// @param [Integer] max the maximum value
// @return [void]
function SveProgress(name, max) {
  this.name = name;
  this.max = max;
  this.count = 0
}

// Clear the progress bar
//
// @param [Integer] max the maximum value
// @return [void]
SveProgress.prototype.clear = function(max) {
  this.max = max;
  this.count = 0;
  this.show(0);
}

// Increment progress
//
// @return [void]
SveProgress.prototype.increment = function() {
	this.count += 1
	p = parseInt((this.count/this.max) * 100);
  this.show(p);
}

// Show progress
//
// @param [Integer] p the current percentage progress
// @return [void]
SveProgress.prototype.show = function(p) {
  $(this.name).css('width', p + '%').attr('aria-valuenow', p);
  $(this.name).html(p + '%');
}
;
