function SveProgress(name, max) {
  this.name = name;
  this.max = max;
  this.count = 0
}

SveProgress.prototype.clear = function(max) {
  this.max = max;
  this.count = 0;
  this.show(0);
}

SveProgress.prototype.increment = function() {
	this.count += 1
	p = parseInt((this.count/this.max) * 100);
  this.show(p);
}

SveProgress.prototype.show = function(p) {
  $(this.name).css('width', p + '%').attr('aria-valuenow', p);
  $(this.name).html(p + '%');
}
;
