$(document).ready(function() {
  
  //var formIdentifierElement = document.getElementById("formIdentifier");
  //var formLabelElement = document.getElementById("formLabel");
  //var groupIdentifierElement = document.getElementById("groupIdentifier");
  
  var formTable;
  var toaTable;
  var visitCount;
  var visitInfo;
  var visitIndex;
  var formCount;
  var formCurrent;
  var formCurrentData;
  var rowIndex;
  var colIndex;

  initData();

/*  $('#formTable').DataTable({
    columnDefs: [ ]
  } );*/

  //var auth = btoa('admin:admin');
  var formTable = $('#formTable').DataTable( {
    "ajax": {
      "type": 'GET',
      "url": "http://localhost:3000/api",
      "beforeSend": function(xhr){
        xhr.setRequestHeader("Authorization","Basic " + btoa("admin:admin"));
        xhr.setRequestHeader("accept", "application/json");
      }
    },
    dataType: 'json',
    success: function(result){
      //alert("data!")
    },
    "bProcessing": true,
    "columns": [
      {"data" : "identifier", "width" : "50%"},
      {"data" : "label", "width" : "50%"}
    ]
  });


  // Get the JSON structure. Set the namespace of the thesauri.
  /*$('#formSave').click(function() {
    
    // Copy the definition. Removes the circulart references and 
    // preserves the structure fo rfurther editing.
    formSave = {};
    copyNode(formDefinition, formSave);
    //alert("Data=" + JSON.stringify(formSave));

    // Send to the server
    $.ajax({
      url: "../forms",
      type: 'POST',
      data: { "form": formSave },
      success: function(result){
        var html = alertSuccess("Form has been saved.");
        displayAlerts(html);
      },
      error: function(xhr,status,error){
        var errors;
        var html;
        errors = $.parseJSON(xhr.responseText).errors;
        var html = ""
        for (var i=0; i<errors.length; i++) {
          html = html + alertError(errors[i]);
        }
        displayAlerts(html);
      }
    }); 

  });*/

  /*
   * Functions to handle click on the update button.
   */
  $('#visitAdd').click(function() {
    visitCount += 1;
    $('#toaTable tr').append($('<td  class="text-center">'));
    $('#toaTable thead tr>td:last').html("V" + visitCount);
    var visit = {num: visitCount, sn: "V" + visitCount,ln: "Visit " + visitCount,timing: "Day X to Y"}
    visitInfo.push(visit);
  });

  $('#visitDelete').click(function() {
    var html = alertWarning ("Delete Visit. Not implemented yet!");
    displayAlerts(html);
  });
  
  $('#visitUpdate').click(function() {
    saveVisit();
    var $td = $('#toaTable thead tr').find('td:nth-child(' + (colIndex+1) + ')');
    var visit = visitInfo[visitIndex];
    $td.html(visit["sn"]);
  });
  
  $('#formAdd').click(function() {
    if (formCurrentData != null) {
      var html = "";
      html = '<tr><td class="text-left col-md-1">' + formCurrentData.identifier + '</td>';
      for (c=1; c<=(visitCount); c++) {
        html = html + '<td class="text-center"></td>';
      }
      html = html + '</tr>';
      $('#toaTable tbody:last').append(html);
      formCount += 1;
    } else {
      var html = alertWarning ("Select a form.");
      displayAlerts(html);
    }
  });

  $('#formDelete').click(function() {
    var html = alertWarning ("Delete Form. Not implemented yet!");
    displayAlerts(html);
  });
  
  $('#toaTable tbody').on('click', 'td', function () {
    var cIndex = parseInt($(this).index());
    var $tr = $(this).closest('tr');
    var rIndex = $tr.index();
    if (cIndex > 0) {
      if (this.innerHTML == 'X') {
        this.innerHTML = ''; 
      } else {
        this.innerHTML = 'X'; 
      }
      updateCol(cIndex);
      updateRow(rIndex);
      displayVisit(cIndex);
    }
  });

  $('#toaTable thead').on('click', 'td', function () {
    var cIndex = parseInt($(this).index());
    var $tr = $(this).closest('tr');
    var rIndex = $tr.index();
    displayVisit(cIndex);
    updateCol(cIndex);
    updateRow(rIndex);
  });

  function updateCol(index) {
    var $td;
    $td = $('#toaTable thead tr').find('td:nth-child(' + (colIndex+1) + ')');
    $td.toggleClass('success');
    $td = $('#toaTable thead tr').find('td:nth-child(' + (index+1) + ')');
    $td.toggleClass('success');
    colIndex = index;
  }

  function updateRow(index) {
    var $tr;
    var $td;
    $tr = $('#toaTable tbody').find('tr:nth-child(' + (rowIndex+1) + ')');
    $td = $tr.find('td:first-child');
    $td.toggleClass('success');
    $tr = $('#toaTable tbody').find('tr:nth-child(' + (index+1) + ')');
    $td = $tr.find('td:first-child');
    $td.toggleClass('success');
    rowIndex = index;
  }

  /*
   * Function to handle click on the form table.
   */
  $('#formTable tbody').on('click', 'tr', function () {
    // Toggle the highlight for the row
    if (formCurrent != null) {
      $(formCurrent).toggleClass('success');
    }
    $(this).toggleClass('success');

    // Get the row
    var row = formTable.row(this).index();
    var data = formTable.row(row).data();

    // Save the selection
    formCurrent = this;
    formCurrentData = data;
  });

  /* ****************
  * Utility Functions
  */

  function initData () {
    visitCount = 0;
    visitInfo = [];
    visitIndex = -1;
    formCount = 0;
    formCurrent = null;
    formCurrentData = null;
    formTable = document.getElementById("formTable");
    toaTable = document.getElementById("toaTable");
    rowIndex = -1;
    colIndex = -1;
  }

  function displayVisit(index) {
    visitIndex = index - 1;    
    $('#visitSN').val(visitInfo[visitIndex]["sn"]);
    $('#visitLN').val(visitInfo[visitIndex]["ln"]);
    $('#visitTiming').val(visitInfo[visitIndex]["timing"]);
  }

  function saveVisit() {
    var visit = visitInfo[visitIndex]
    visit["sn"] = $('#visitSN').val();
    visit["ln"] = $('#visitLN').val();
    visit["timing"] = $('#visitTiming').val();
  }

});
