function SveStudy(studyVersionId, edit) {
  this.studyVersionId = studyVersionId;
  if ($("#token_1").length) { // If the field exists start timer
    ttAddToken("1");

    var _this = this;

    // Button handlers
    $('#save_button').click(function() {
      _this.save();
    });
  }

}

SveStudy.prototype.save = function() {
  var _this = this;
  data = { "study_version": {} };
  data.study_version.name = $("#study_version_name").val();
  data.study_version.description = $("#study_version_description").val();
  data.study_version.protocol_name = $("#study_version_protocol_name").val();
  $("#saving i").addClass("fa-spin");
  $.ajax({
    url: "/study_versions/" + this.studyVersionId,
    type: 'PUT',
    data: JSON.stringify(data),
    dataType: 'json',
    contentType: 'application/json',
    success: function(result){
		  $("#saving i").removeClass("fa-spin");
      ttExtendLock("1");
    },
    error: function(xhr,status,error){
		  $("#saving i").removeClass("fa-spin");
      handleAjaxError(xhr, status, error);
    }
  });
}
;
